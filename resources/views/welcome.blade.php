<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body class="bg-brand-100 font-sans font-normal">
    <div class="flex flex-col">
        @if(Route::has('login'))
            <div class="absolute top-0 right-0 mt-4 mr-4">
                @auth
                    <a href="{{ url('/home') }}" class=" hover:underline text-sm font-normal text-brand-600 uppercase">Home</a>
                @else
                    <a href="{{ route('login') }}" class=" hover:underline text-sm font-normal text-brand-600 uppercase pr-6">Login</a>
                    <a href="{{ route('register') }}" class=" hover:underline text-sm font-normal text-brand-600 uppercase">Register</a>
                @endauth
            </div>
        @endif

        <div class="min-h-screen flex items-center justify-center">
            <div class="flex flex-col justify-around h-full">
                <div>
                    <h1 class="text-grey-800 text-center font-hairline tracking-wider text-7xl mb-6">
                        {{ config('app.name', 'Laravel') }}
                    </h1>
                    <ul class="list-none p-0">
                        <li class="inline pr-8">
                            <a href="https://laravel.com/docs" class=" hover:underline text-sm font-normal text-brand-600 uppercase" title="Documentation">Documentation</a>
                        </li>
                        <li class="inline pr-8">
                            <a href="https://laracasts.com" class=" hover:underline text-sm font-normal text-brand-600 uppercase" title="Laracasts">Laracasts</a>
                        </li>
                        <li class="inline pr-8">
                            <a href="https://laravel-news.com" class=" hover:underline text-sm font-normal text-brand-600 uppercase" title="News">News</a>
                        </li>
                        <li class="inline pr-8">
                            <a href="https://forge.laravel.com" class=" hover:underline text-sm font-normal text-brand-600 uppercase" title="Forge">Forge</a>
                        </li>
                        <li class="inline pr-8">
                            <a href="https://github.com/laravel/laravel" class=" hover:underline text-sm font-normal text-brand-600 uppercase" title="GitHub">GitHub</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
