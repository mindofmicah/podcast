namespace Deployer;
@foreach($recipes as $recipe)
require 'recipe/{{$recipe}}.php';
@endforeach

@foreach($settings as $setting=>$value)
set('{{$setting}}', {{var_export($value)}});

@endforeach

@foreach($hosts as $host_name=> $host)
host('{{$host_name}}')
    ->user('{{$host['user']}}')
    ->identityFile('{{$host['identity_file']}}')
    ->stage('{{$host['stage']}}')
@foreach($host['settings'] as $key=>$value)
    ->set('{{$key}}', var_export($value))
;@endforeach

@endforeach

add('rsync', [
    'exclude'=>[
        '.git', 
        'deploy.php',
        'node_modules',
    ]
]);

task('npm:build', function (){
    runLocally("cd @{{release_path}} && @{{bin/npm}} run production");
});

set('rsync_src', function () {
    $local_src = get('build_path');
    if (is_callable($local_src)) {
        $local_src = $local_src();
    }
    return $local_src;
});

task('bbuild', function () {
    set('deploy_path', '/tmp/deployer');

    invoke('deploy:prepare');

    invoke('deploy:prepare');
    invoke('deploy:release');
    invoke('deploy:update_code');
    invoke('deploy:vendors');

    invoke('npm:install');
    invoke('npm:build');

    invoke('deploy:symlink');

    if (get('install')) {
        $output = preg_replace_callback('/^(DB_DATABASE=)'.preg_quote(getcwd(), '/').'\/(.+)$/m', function ($matches) {
            run('touch @{{release_path}}/' . $matches[2]);
            return $matches[1] . host('160.153.93.197')->get('deploy_path') . '/shared/' . $matches[2];
        }, file_get_contents('.env'));
        file_put_contents('.envv', $output);
        upload('.envv', '@{{release_path}}/.env');
        unlink('.envv');
    }

    $release_path = get('release_path');
    on(Deployer::get()->hosts, function () use ($release_path){
        set('build_path', "$release_path/");
    });

})->local();

task('cleeean', function () {
    invoke('cleanup');
})->local();

task('deploy:staging', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:writable',
    'deploy:shared',
    'artisan:view:clear',
    'artisan:cache:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'artisan:migrate',  // this isn't set up yet
    'artisan:queue:restart',
    'deploy:symlink',
    'deploy:cloudflare',
    'deploy:unlock',
    'cleanup',
])->desc('Deploy your project');

@foreach($before as $k=>$v)
before('{{$k}}', '{{$v}}')
@endforeach

@foreach($after as $k=>$v)
before('{{$k}}', '{{$v}}')
@endforeach
