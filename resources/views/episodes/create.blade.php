@extends('layouts.master')
@section('content')
<form action="/feeds/{{$feed->path}}/episodes" method="post">
    <div class="field">
        <label class="label">URL</label>
        <div class="control">
            <input class="input" name="url"/>
        </div>
    </div>
    <button class="button is-pulled-right">Add Episode</button>
</form>
@stop
