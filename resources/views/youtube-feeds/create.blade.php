@extends('layouts.master')
@section('content')
    <div class="container">
        <h1 class="heading is-size-1">Create a new YouTube feed</h1>
        <?php
        dump($errors);
        ?>
        <form action="/youtube-feeds" method="post" id="youtube-feed">
             <div class="field">
                <label class="label">YouTube source</label>
                <div class="control">
                    <input class="input" type="text" name="source" value="" @change="fetchMetaData($event)">
                </div>
            </div>
           
            <div class="field">
                <label class="label">Name</label>
                <div class="control">
                    <input class="input" type="text" name="title" value="" v-model="name">
                </div>
                
                <p v-show="path" class="help">https://podcasts.whamdonk.com/feeds/<strong>@{{path}}</strong><a @click.prevent="changePath" style="margin-left:40px;">Change</a></p>
                <input type="hidden" v-model="path" name="path">
            </div>
            <div class="field">
                <label class="label">Episode Limit</label>
                <div class="control">
                    <input class="input" type="text" name="episode_limit" value="">
                </div>
            </div>
            <div>
                <button class="button">Save Changes</button>
            </div>
        </form>
    </div>
@stop
