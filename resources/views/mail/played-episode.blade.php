@component('mail::message')
# Marked some episodes as played

@foreach($feeds as $feed=>$episodes)
    ## {{$feed}}
    @foreach($episodes as $episode)
        **{{$episode}}**
    @endforeach
@endforeach
@endcomponent