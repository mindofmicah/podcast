@component('mail::message')
# Hi there

Added a totally awesome overcast id for a **{{$episode->title}}**

It was **{{$episode->overcast_id->overcast_id}}**

Awesome, right?
@endcomponent