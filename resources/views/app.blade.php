<!DOCTYPE html>
<html class="h-full bg-gray-100">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="icon" type="image/svg+xml" href="/favicon.svg">
    <link href="/assets/legacy.css" rel="stylesheet">
    <link href="/assets/legacy2.css" rel="stylesheet">

    {{-- Inertia --}}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=smoothscroll,NodeList.prototype.forEach,Promise,Object.values,Object.assign"
            defer></script>

    {{-- Ping CRM --}}
    <script src="https://polyfill.io/v3/polyfill.min.js?features=String.prototype.startsWith" defer></script>

    @if(!empty($svelte) && $svelte)
        {{remix()}}
    @else
        <script src="/assets/legacy.js" type="module" defer></script>
    @endif
    @inertiaHead
</head>
<body class="bg-green-800 h-screen font-sans">
<div class="container mx-auto shadow bg-gray-100 p-2">
    <header class="mb-3">
        <h1 class="text-4xl text-center">Podcast Stuff</h1>
        <nav class="border-t border-green-800 border-solid border-b py-1">
            <ul class="list-none p-0 flex justify-center">
                @foreach(['/feeds'=>'Active Feeds', '/overcast/feeds'=>'Overcast Feeds'] as $href=>$label)
                    <li class="mx-2">
                        <a href="{{$href}}" class=" hover:underline text-green-800">{{$label}}</a>
                    </li>
                @endforeach
            </ul>
        </nav>
    </header>
    <div>
        @inertia
    </div>
</div>

<!-- Scripts -->

{{--<script src="/build/assets/app-055a21cf.js"></script>--}}
</body>
</html>
