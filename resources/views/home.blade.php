@extends('layouts.app')

@section('content')
<div class="flex items-center">
    <div class="md:w-1/2 md:mx-auto">
        <div class="rounded shadow">
            <div class="font-medium text-lg text-brand-800 bg-brand-200 p-3 rounded-t">
                Dashboard
            </div>
            <div class="bg-white p-3 rounded-b">
                @if (session('status'))
                    <div class="bg-green-100 border border-green-400 text-green-600 text-sm px-4 py-3 rounded mb-4">
                        {{ session('status') }}
                    </div>
                @endif

                <p class="text-grey-600 text-sm">
                    You are logged in!
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
