@component('mail::message')
# Don't forget about these fellows

You have a few feeds that are not set to automatically release episodes. Maybe you'd like to see what they're up to.

@foreach($feeds as $feed)
@component('mail::panel')
##{{$feed->title}}
@foreach($feed->episodes()->whereNull('released_at')->take(3)->get()->shuffle() as $episode)
- {{$episode->options['title']}}
@endforeach
@component('mail::button', ['url' => route('feeds.episodes.index', $feed->details->path)])
View all available episodes
@endcomponent
@endcomponent
@endforeach

Thanks,<br>
{{ config('app.name') }}
@endcomponent
