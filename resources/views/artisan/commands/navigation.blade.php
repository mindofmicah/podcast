<style>

nav > ul > li {
    position:relative;
}
li:hover > ul{
    display:block;
}
li > ul {
list-style:none;
padding:0;margin:0;
    position: absolute;
background-color:#0394fe;
border:1px solid brown;
    top:100%;
    display:none;
    left:0;
}
</style>

<nav>
<ul class="flex flex-wrap" style="list-style:none;">
@foreach($navigation as $namespace=> $links)
    <li style="width:{{100/8}}%; text-transform:capitalize;">{{strtr($namespace,'-', ' ')}}
        <ul class="list-disc pl-4">
            @foreach($links as $link)
            <li><a href="/artisan/commands/{{$link['command']}}">{{$link['label']}}</a></li>
            @endforeach
        </ul>
    </li>

@endforeach
</ul>
</nav>
