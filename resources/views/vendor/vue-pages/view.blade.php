@extends('layouts.master')

@section('app', 'component-app')

@section('content')
<div data-component="{{$name}}" data-props="{{json_encode($data)}}">
</div>
@endsection
