<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/main.css') }}" rel="stylesheet">
</head>
<body class="bg-green-800 h-screen font-sans">
<div class="container mx-auto shadow bg-gray-100 p-2">
    <header class="mb-3">
        <h1 class="text-4xl text-center">Podcast Stuff</h1>
        <nav class="border-t border-green-800 border-solid border-b py-1">
            <ul class="list-none p-0 flex justify-center">
                @foreach(['/feeds'=>'Active Feeds', '/overcast/feeds'=>'Overcast Feeds'] as $href=>$label)
                    <li class="mx-2">
                        <a href="{{$href}}" class=" hover:underline text-green-800">{{$label}}</a>
                    </li>
                @endforeach
            </ul>
        </nav>
    </header>
    <div id="@yield('app', 'fallback-app')">
        @yield('content')
    </div>
</div>

<!-- Scripts -->

<script type="module" src="{{remix()}}"></script>
</body>
</html>
