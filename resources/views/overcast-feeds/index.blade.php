@extends('layouts.master')

@section('content')
    <h1>Overcast Feeds</h1>
    @foreach($listings as $listing)
        <overcast-feed
                artwork="{{$listing['artwork']}}"
                title="{{$listing['title']}}"
                id="{{strtok($listing['id'], '/')}}"
                class="mb-2 pb-2 border-b border-gray-400"
                ></overcast-feed>
    @endforeach
@stop
