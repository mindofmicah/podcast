import "./bootstrap";
import { createInertiaApp } from "@inertiajs/svelte";

createInertiaApp({
    resolve: (name) => {
        const pages = import.meta.glob("./page-components/**/*.svelte", {
            eager: true
        });
        return pages[`./page-components/${name}.svelte`];
    },
    setup({el, App, props}) {
        new App({target: el, props});
    }
});
