import Button from './Button';

export default class ButtonFactory {
    static createLink(text, link, options = {}): Button {
        options.text = text;
        options.link = link;

        return new Button(options);
    }

    static createButton(text, button, options = {}): Button {
        options.text = text;
        options.handler = button;

        return new Button(options);
    }
}