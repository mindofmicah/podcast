export default class Button {
    constructor({text, link, handler, class_name}) {
        this.text = text;
        this.link = link;
        this.handler = handler;
        this.class_name = class_name;
    }
}