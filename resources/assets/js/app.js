import './bootstrap';

import Vue from 'vue';
import { createInertiaApp } from '@inertiajs/vue2';

window.Vue = Vue;

createInertiaApp({
    resolve: name => {
        const pages = import.meta.glob('./page-components/**/*.vue', {eager: true});
        return pages[`./page-components/${name}.vue`];
    },
    setup({el, App, props, plugin}) {
        Vue.use(plugin);
        new Vue({
            render: h => h(App, props),
        }).$mount(el)
    }
});