<?php
namespace Deployer;

desc('Run custom artisan stuff');
task('artisan:custom', function () {
    run('{{bin/php}} {{release_path}}/artisan discover:releasers');
});