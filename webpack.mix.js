let mix = require('laravel-mix');

require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.options({
    })
    .js('resources/assets/js/app.js', 'public/js' )
    .postCss(
        'resources/assets/css/main.css', 
        'public/css', 
        require('./postcss.config.js').plugins
    )
    .purgeCss();

if (mix.inProduction()) {
    mix.version();
} else {
    mix.browserSync('localhost:8000')
}


//mix.sourceMaps();
