<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Audiobook
 *
 * @property int $id
 * @property int $feed_id
 * @property string $title
 * @property string $author
 * @property string|null $goodreads_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Audiobook newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Audiobook newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Audiobook query()
 * @mixin \Eloquent
 */
class Audiobook extends Model
{
    protected $guarded = [];
}
