<?php
use Illuminate\Support\HtmlString;

if (!function_exists('dur2sec')) {
    function dur2sec(string $duration): int
    {
        $duration = strtr($duration, '"', ':');

        $total = $i = 0;
        /** @var int[] $chunks */
        $chunks = explode(':', $duration);

        try {
            while ($segment = array_pop($chunks)) {
                $total += $segment * pow(60, $i++);
            }
        } catch (ErrorException $e) {
            throw new InvalidArgumentException($duration . ' is not a valid duration');
        }
        return $total;
    }
}

if (!function_exists('remix')) {

    function remix()
    {
        $is_dev = false;
        $asset_domain = null;

        if (app()->environment('local')) {
            $asset_domain = (file_get_contents(public_path('hot')));
            if ($asset_domain != 'prod') {
                $is_dev = true;
            }
        }

        if ($is_dev && $asset_domain) {
            // TODO Somehow store that entrypoint somewhere?
            return new HtmlString(<<<HTML
<script type="module" src="$asset_domain/@vite/client"></script>
<script type="module" src="$asset_domain/resources/assets/js/svelte.js"></script>
HTML
            );
        }

        $built_files = array_map(function ($file) {
            if (pathinfo($file['file'], PATHINFO_EXTENSION) === 'css') {
                return '<link rel="stylesheet" href="/build/' . $file['file'] . '"/>';
            }
            return '<script type="module" src="/build/' . $file['file'] . '"></script>';
        }, json_decode(file_get_contents(public_path('build/manifest.json')), 1));

        return new HtmlString(implode(PHP_EOL, $built_files));
    }
}
