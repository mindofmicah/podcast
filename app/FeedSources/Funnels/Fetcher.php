<?php
namespace App\FeedSources\Funnels;

use App\Domain\EpisodeDiscovery\DiscoverNewEpisodesRequest;
use App\Domain\EpisodeDiscovery\UseCases\DiscoverNewEpisodes;
use App\Feed;
use App\FeedSources\FetchedInformation;
use App\FeedSources\Contracts\InformationFetchable;

class Fetcher implements InformationFetchable
{
    public function fetchInformationForFeed(Feed $feed): FetchedInformation
    {
        /** @var DiscoverNewEpisodes $a */
        $a = resolve(DiscoverNewEpisodes::class);

        /** @var Feed[] $nested_feeds */
        $nested_feeds = $feed->details->feeds;

        foreach ($nested_feeds as $f) {
            dump($a->handle(new DiscoverNewEpisodesRequest($f)));
        }

        return new FetchedInformation(collect(), '');
    }
}