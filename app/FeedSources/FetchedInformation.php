<?php
namespace App\FeedSources;

use Illuminate\Support\Collection;

class FetchedInformation
{
    private $episodes;
    private $coverart;

    public function __construct(Collection $episodes, string $coverart = '')
    {
        $this->episodes = $episodes;
        $this->coverart = $coverart;
    }

    public function filterEpisodes(Collection $filters)
    {
        if ($this->episodes->count() && $filters->count()) {
            $this->episodes = $this->episodes->filter(function (array $episode) use ($filters) {
                foreach ($filters as $filter) {
                    if (!$filter->matches($episode)) {
                        return false;
                    }
                }
                return true;
            })->values();
        }

        return $this;
    }
    public function newEpisodes(): Collection
    {
        return $this->episodes;
    }

    public function coverart(): string
    {
        return $this->coverart;
    }
}

