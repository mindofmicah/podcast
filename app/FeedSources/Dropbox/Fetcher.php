<?php
namespace App\FeedSources\Dropbox;

use App\Domain\EpisodeDiscovery\Contracts\FeedInformationFetcher;
use App\Feed;
use Exception;
use Illuminate\Filesystem\FilesystemAdapter;
use League\Flysystem\MountManager;
use App\Jobs\MoveDropboxEpisode;
use Illuminate\Support\Collection;

class Fetcher extends FeedInformationFetcher
{
    /** @var string */
    private $path;

    /** @var FilesystemAdapter */
    private $dropbox;
    /** @var MountManager */
    private $mount_manager;
    /** @var Collection */
    private $existing_files;

    public function __construct(string $path, FilesystemAdapter $filesystem, MountManager $mount_manager)
    {
        $this->path = $path;
        $this->dropbox = $filesystem;
        $this->mount_manager = $mount_manager;
        $this->existing_files = collect();
    }

    protected function before()
    {
        $this->existing_files = collect($this->dropbox->allFiles($this->path))
            ->groupBy(function (string $filename) {
                $extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
                return in_array($extension, ['jpg', 'png'])
                    ? 'image'
                    : (in_array($extension, ['mp3', 'm4a',]) ? 'mp3' : $extension);
            });
    }

    protected function getNewEpisodes(Feed $feed): Collection
    {
        $mp3_files = $this->existing_files->get('mp3', collect());
        $files_to_add = $mp3_files->diff($feed->episodes->map(function ($episode) {
            return "$this->path/$episode->filename";
        }));

        $previous_max_order = $feed->episodes()->max('order');

        return $mp3_files->reduce(function (Collection $carry, string $data) use (&$previous_max_order, $feed, $files_to_add) {

            $path_info = pathinfo($data);
            $file_metadata = $this->dropbox->getMetadata($data);

            $this->jobs_to_dispatch[] = new MoveDropboxEpisode($data);

            if ($files_to_add->contains($data)) {
                $slug = substr(md5($data), 0, 16);

                $carry->push([
                    'filename' => $path_info['basename'],
                    'order'    => ++$previous_max_order,
                    'url'      => route('audio-files.show', [$feed->path, $slug]),
                    'size'     => $file_metadata['bytes'],
                    'title'    => $path_info['filename'],
                    'options'  => [
                        'title'       => $path_info['filename'],
                        'type'        => 'podcast',
                        'description' => '',
                    ],
                ]);
            }
            return $carry;
        }, collect());
    }

    protected function getCoverart(Feed $feed): string
    {
        $coverart = '';

        if ($this->existing_files->has('image')) {
            $image_path = $this->existing_files['image']->first();
            try {
                $this->mount_manager->copy('dropbox://' . $image_path, 'local://coverart/' . $image_path);
            } catch (Exception $e) {
            }
            $coverart = 'coverart/' . $image_path;
        }
        return $coverart;
    }
}
