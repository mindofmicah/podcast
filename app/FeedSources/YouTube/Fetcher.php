<?php
namespace App\FeedSources\YouTube;

use Alaouy\Youtube\Youtube;
use App\Feed;
use App\Jobs\DownloadYoutubeAsAudio;
use App\FeedSources\FetchedInformation;
use App\FeedSources\Contracts\InformationFetchable;
use Illuminate\Support\Collection;

class Fetcher implements InformationFetchable
{
    const STOP_AT_ONE = 1;

    /** @var Youtube */
    private $youtube_api;
    /** @var string */
    private $playlist_url;
    /** @var string|null */
    private $next_page;

    private $stop_at_one = false;

    public function __construct(Youtube $youtube_api, string $playlist_url, string $current_page = null)
    {
        $this->youtube_api = $youtube_api;
        $this->playlist_url = $playlist_url;
        $this->next_page = $current_page;
    }

    public function handleFlags(int $flags)
    {
        if ($flags & self::STOP_AT_ONE) {
            $this->stop_at_one = true;
        }
    }

    /**
     * @throws \Exception
     */
    public function fetchInformationForFeed(Feed $feed): FetchedInformation
    {
        $existing_youtube_ids = $feed->episodes()->pluck('filename')->map(function ($f) {
            return explode('.', $f, 2)[0];
        });

        $path = $this->playlist_url;
        $next_page = $this->next_page;

        $youtube = $this->youtube_api;

        $playlist_id = explode('=', parse_url($path, PHP_URL_QUERY))[1];

        $results = [];
        $previous_page = null;
        $first_loop = true;

        do {
            $latest_page = $next_page;
            $response = $youtube->getPlaylistItemsByPlaylistId($playlist_id, $next_page, 25, ['snippet']);

            $next_page = $response['info']['nextPageToken'];
            if ($first_loop) {
                $previous_page = $response['info']['prevPageToken'];
            }
            $first_loop = false;
            array_push($results, ...array_column($response['results'], 'snippet'));
        } while ($next_page);

        while (!$this->stop_at_one && $previous_page) {
            $response = $youtube->getPlaylistItemsByPlaylistId($playlist_id, $previous_page, 25, ['snippet']);

            $previous_page = $response['info']['prevPageToken'];
            array_push($results, ...array_column($response['results'], 'snippet'));
        }

        $coverart = '';
        if (!$feed->coverart && $results) {
            $channel_id = $results[0]->channelId;
            $thumbnails = $youtube->getChannelById($channel_id, [], ['snippet'])->snippet->thumbnails;

            if ($thumbnails->high) {
                $coverart = $thumbnails->high->url;
            } elseif ($thumbnails->default) {
                $coverart = $thumbnails->default->url;
            }
        }

        $episodes = collect($results)
            ->keyBy('position')
            ->sortKeysDesc()
            ->pipe(function (Collection $collection) use ($existing_youtube_ids) {
                return $collection->take($collection->pluck('resourceId.videoId')->intersect($existing_youtube_ids)->keys()->min());
            })
	    ->filter(function ($video) {
	    	return $video->title !== 'Private video';
	    })
	    ->values()
            ->when($this->stop_at_one, function (Collection $collection) {
                return $collection->take(1);
            })
            ->map(function ($video) use ($feed) {
                // TODO STOP WHEN YOU FIND AN EXISTING ONE
                return [
                    'filename' => $video->resourceId->videoId . '.mp3',
                    'order'    => $video->position+5,
                    'url'      => route('audio-files.show', [$feed->path, $video->resourceId->videoId . '.mp3']),
                    'title'    => $video->title,
                    'options'  => [
                        'title'       => $video->title,
                        'type'        => 'podcast',
                        'description' => $video->description,
                    ],
                ];
            })
            ->reverse()
            ->values();

        $episodes->pluck('filename')->each(function ($video_id) use ($feed) {
            $video_id = explode('.', $video_id, 2)[0];
            dispatch((new DownloadYoutubeAsAudio($video_id, $feed->path)));
        });

        $feed->details->current_page = $latest_page;
        $feed->details->save();

        return new FetchedInformation($episodes, $coverart);
    }
}
