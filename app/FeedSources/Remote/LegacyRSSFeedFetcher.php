<?php
namespace App\FeedSources\Remote;

use App\Feed;
use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\FetchedInformation;
use Exception;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class LegacyRSSFeedFetcher extends RSSFeedFetcher implements InformationFetchable
{
    public function fetchInformationForFeed(Feed $feed): FetchedInformation
    {
        $existing_urls = $feed->episodes->pluck('url');
        $order = $feed->episodes()->max('order');
        $keep_going = true;

        $c = new Client;
        try {
            $rss_feed = $c->get($this->path)->getBody()->getContents();
        } catch (Exception $e) {
            \Log::info('Remote feed url returned 404', [
                'feed' => $feed->toArray(),
            ]);
        }

        $crawler = new Crawler($rss_feed ?? null);

        $cover_art = '';
        if (!$feed->coverart) {
            $img = $crawler->filter('image url');
            if (count($img)) {
                $cover_art = $img->text();
            } else {
                $cover_art = $crawler->filterXPath('//itunes:image')->attr('href');
            }
        }

        $aaaa = collect($crawler->filter('item')->each(function (Crawler $item, $index) use (&$keep_going, $existing_urls) {
            if ($this->skip_existing && !$keep_going) {
                return [];
            }

            $enclosure = $item->filterXPath('//enclosure');
            // If there isn't an enclosure (a downloadable file), skip it
            if (count($enclosure) === 0) {
                return [];
            }
            $full_url = $enclosure->attr('url');
            $filesize = $enclosure->attr('length');

            $duration = null;
            try {
                $duration_tag = $item->filterXPath('//itunes:duration');
                if ($duration_tag->count()) {
                    $duration = dur2sec($duration_tag->text());
                }
            } catch (Exception $e) {
            }

            if ($this->skip_existing && (!$keep_going || $existing_urls->search($full_url) !== false)) {
                $keep_going = false;

                return [];
            }
            $title = $item->filter('title')->first()->text();
            $options = [
                'title'       => $title,
                'description' => $item->filter('description')->first()->html(),
            ];

            $season_tag = $item->filterXPath('//itunes:season');
            if ($season_tag->count()) {
                $options['season'] = $season_tag->html();
            }

            return $this->mergeWithDefaultOptions([
                'url'      => $full_url,
                'filename' => pathinfo($full_url, PATHINFO_FILENAME),
                'size'     => $filesize,
                'duration' => $duration,
                'title'    => $title,
                'options'  => $options,
            ]);
        }))->filter()->when($this->direction == self::OLDEST_FIRST, function ($collection) {
            return $collection->reverse();
        })->values()->transform(function ($item, $index) use ($order) {
            $item['order'] = $index + $order + 1;

            return $item;
        });

        return new FetchedInformation($aaaa, $cover_art);
    }
}