<?php
namespace App\FeedSources\Remote;

use App\Feed;
use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\FetchedInformation;
use Exception;

class RSSFeedFetcher implements InformationFetchable
{
    use DefaultRemoteOptions;

    const OLDEST_FIRST = 'oldest_first';
    const NEWEST_FIRST = 'newest_first';

    protected $path;
    protected $direction;
    protected $skip_existing = false;

    public function __construct(string $path, $direction = self::OLDEST_FIRST)
    {
        $this->path = $path;
        $this->direction = $direction;
    }

    public function stopOnExistingEpisodes()
    {
        $this->skip_existing = true;
    }

    public function fetchInformationForFeed(Feed $feed): FetchedInformation
    {
        $existing_urls = $feed->episodes->pluck('url');
        $order = $feed->episodes()->max('order');
        $keep_going = true;

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->path);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $xmlresponse = curl_exec($ch);
            $xml = simplexml_load_string($xmlresponse);
            $xml->channel;
        } catch (Exception $e) {
            return new FetchedInformation(collect([]));
        }

        $cover_art = '';
        if (!$feed->coverart) {
            $cover_art = (string)$xml->channel->image->url;
            if ($cover_art === '') {
                try {
                    $cover_art = (string)$xml->channel->children('itunes', true)->image->attributes()->href;
                } catch (Exception $e) {
                    // do nothing, $cover_art stays an empty string
                }
            }
        }

        $episodes = [];
        foreach ($xml->channel->item as $index => $item) {
            $fields = array_merge(
                (array)$item->children('itunes', true),
                (array)$item
            );

            $enclosure = [];
            foreach ($fields['enclosure']->attributes() as $k => $v) {
                $enclosure[$k] = (string)$v;
            }

            if ($existing_urls->search($enclosure['url']) !== false) {
                break;
            }
            $options = array_filter([
                'title'       => $fields['title'],
                'description' => $fields['description'] ?? null,
                'season'      => $fields['season_tag'] ?? null,
            ]);

            $duration = null;
            if (!empty($fields['duration'])) {
                $duration = dur2sec($fields['duration']);
            }

            $episodes[] = $this->mergeWithDefaultOptions([
                'url'      => $enclosure['url'],
                'filename' => pathinfo($enclosure['url'], PATHINFO_FILENAME),
                'size'     => $enclosure['length'] ?? 0,
                'title'    => $fields['title'],
                'duration' => $duration,
                'options'  => $options,
            ]);
        }
        if ($this->direction === self::OLDEST_FIRST) {
            $episodes = array_reverse($episodes);
        }
        foreach ($episodes as $index => &$episode) {
            $episode['order'] = $order + $index + 1;
        }
        $aaaa = collect($episodes);
        return new FetchedInformation($aaaa, $cover_art);
    }
}

