<?php
namespace App\FeedSources\Remote;

use App\Feed;
use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\FetchedInformation;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Symfony\Component\DomCrawler\Crawler;

class LoyalBooksFetcher extends LegacyRSSFeedFetcher
{
    public function __construct(string $path)
    {
        $this->path = $this->getRSSFeedFromPath($path);
        $this->direction = RSSFeedFetcher::NEWEST_FIRST;
    }

    /**
     * Look at the HTML of the Loyal Books website and find the actual RSS feed
     *
     * @return string
     */
    private function getRSSFeedFromPath(string $path): string
    {
        return (new Crawler(file_get_contents($path)))
            // Grab the first <a class="download"> where the href includes "feed" to grab the path to the RSS feed
            ->filter('a.download[href*="feed"]')
            ->first()
            ->extract('href')[0];
    }
}
