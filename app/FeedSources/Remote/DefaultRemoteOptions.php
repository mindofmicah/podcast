<?php
namespace App\FeedSources\Remote;

trait DefaultRemoteOptions
{
    protected $default_options = [
        // Since we're pulling from a remote source, the episodes are available
        'is_available' => true,
        'size'         => 0,
        'options'      => [
            'type'        => 'podcast',
            'description' => '',
        ],
    ];

    /**
     * Set some sensible defaults for all remote feeds
     *
     * @param array $options
     *
     * @return array
     */
    public function mergeWithDefaultOptions(array $options)
    {
        return array_merge($this->default_options, $options);
    }
}