<?php
namespace App\FeedSources\Remote;

use App\Feed;
use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\FetchedInformation;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Symfony\Component\DomCrawler\Crawler;

class ArchiveOrgFetcher implements InformationFetchable
{
    use DefaultRemoteOptions;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function fetchInformationForFeed(Feed $feed): FetchedInformation
    {
        $download_path = str_replace('/details/', '/download/', $this->path);

        $crawler = new Crawler(file_get_contents($download_path));

        $coverart = '';
        if(is_null($feed->coverart)) {
            $image_file = $crawler->filter('table.directory-listing-table a[href$=".jpg"]')->first()->extract(['href'])[0];
            $coverart = $download_path . '/'. $image_file;
        }

        // Grab the MP3s on the page
        $mp3s = $crawler->filter('table.directory-listing-table a[href$=".mp3"]');
        if (!$mp3s->count()) {
            // If there aren't any mp3s, find a path to the next directory

            $next_path = $crawler->filter('table.directory-listing-table a[href$=\/]')->first()->extract(['href'])[0];
            $download_path = $download_path . '/' . $next_path;

            $next_page = (new Crawler(file_get_contents($download_path)));

            $mp3s = $next_page->filter('table.directory-listing-table a[href$=".mp3"]');

            if (!$mp3s->count()) {
                throw new Exception('No sign of MP3s at ' . $download_path);
            }

            // Create a string of ":not"s to make sure we only pull in NEW files
//            $existing_paths = $feed->episodes->pluck('options.url')->map(function ($url) {
  //              return ':not([href="' . pathinfo($url)['basename'] . '"])';
    //        })->implode('');

      //      $mp3s = $mp3s->filter($existing_paths);
        }

        $today = Carbon::now()->subHours($mp3s->count());

        $needed_episodes = ($feed->episode_limit -$feed->episodes()->whereNotNull('released_at')->count());

        $existing_urls = $feed->episodes()
            ->pluck('filename')
            ->map(function ($filename) {
                return urldecode($filename);
            });

        $download_path = rtrim($download_path, '/') . '/';
        return new FetchedInformation(collect($mp3s->each(function (Crawler $crawler, int $index) use ($existing_urls, $today, $download_path) {
            $fake_date = $today->copy()->addHour($index);
            $href = $crawler->extract(['href'])[0];

            if (($existing_urls->search($href) !== false) || ($existing_urls->search(urldecode($href)) !== false)) {
                return [];
            }

            return $this->mergeWithDefaultOptions([
                'filename'   => $href,
                'order'      => $index + 1,
                'url'        => $download_path . $href,
                'title'      => urldecode($href),
                'options'    => [
                    'title'       => urldecode($href),
                ],
            ]);
        }))->filter()->pipe(function ($items) use ($needed_episodes){
            $how_many = min($items->count(), $needed_episodes);
            if ($how_many <= 0) {
                return $items;
            }

            foreach (range(0, $how_many-1) as $index) {
                $a = $items[$index];
                $a['released_at'] = \Carbon\Carbon::now();
                $items->put($index, $a);
            }
            return $items;
        }), $coverart);
    }
}
