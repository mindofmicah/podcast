<?php
namespace App\FeedSources\Contracts;

interface Fetchable
{
    public function fetcher(): InformationFetchable;
}
