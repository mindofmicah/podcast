<?php
namespace App\FeedSources\Contracts;

use App\Feed;
use App\FeedSources\FetchedInformation;

interface InformationFetchable
{
    public function fetchInformationForFeed(Feed $feed): FetchedInformation;
}
