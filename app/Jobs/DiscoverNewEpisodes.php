<?php
namespace App\Jobs;

use App\Domain\EpisodeDiscovery\DiscoverNewEpisodesRequest;
use App\Domain\EpisodeDiscovery\UseCases\DiscoverNewEpisodes as UseCase;
use App\Feed;
use App\FeedSources\FetchedInformation;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DiscoverNewEpisodes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $feed;
    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Feed $feed)
    {
        $this->feed = $feed;
    }

    /**
     * Execute the job.
     *
     * @return FetchedInformation
     */
    public function handle(UseCase $use_case)
    {
        return $use_case->handle(new DiscoverNewEpisodesRequest($this->feed));
    }
}
