<?php
namespace App\Jobs;

use App\Domain\Common\Models\Episode;
use App\Events\EpisodeAssignedOvercastId;
use App\OvercastId;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use MindOfMicah\OvercastSDK\OvercastSDK;

class AssignOvercastEpisodeId extends Job
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $episode;

    public $tries = 2;

    public function __construct(Episode $episode)
    {
        $this->episode = $episode;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OvercastSDK $overcast)
    {
        $this->episode->load('feed.overcast_id', 'overcast_id');
        if ($this->episode->overcast_id) {
            return;
        }

        if (!$this->episode->feed->overcast_id) {
            return $this->tryAgainLater();
        }

        $feed_episodes = collect($overcast->getFeedEpisodes($this->episode->feed->overcast_id->overcast_id))->keyBy('id');

        $feed_episodes = $feed_episodes->only($feed_episodes->keys()->diff(OvercastId::pluck('overcast_id')))->toArray();

        foreach ($feed_episodes as $feed_episode) {
            if ($this->episode->matchesOvercastTitle($feed_episode['title'])) {
                $this->episode->setOvercastId($feed_episode['id']);

                event(new EpisodeAssignedOvercastId($this->episode->fresh()));
                return true;
            }
        }

        return $this->tryAgainLater();
    }

    private function tryAgainLater()
    {
        return $this->release(300);
    }
}
