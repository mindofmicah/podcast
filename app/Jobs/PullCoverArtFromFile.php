<?php

namespace App\Jobs;

use Storage;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PullCoverArtFromFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $feed;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\Feed $feed)
    {
        $this->feed = $feed;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $first_episode = $this->feed->episodes->first();

        
        $driver = Storage::disk('dropbox')->getDriver();
        $fullname = $this->feed->path . '/'. $first_episode->filename;
        $stream = $driver->readStream($fullname);
        file_put_contents('/tmp/stuff', $stream);
        dd(get_defined_vars());
        $source = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix('test');
        $this;
        dd(get_defined_vars());
    }
}
