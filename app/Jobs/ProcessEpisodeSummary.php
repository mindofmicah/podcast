<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessEpisodeSummary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $filename;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $contents = file_get_contents($this->filename);
        $crawler = new \Symfony\Component\DomCrawler\Crawler($contents);

//    $eps = \App\Episode::with('overcast_id')->whereHas('overcast_id')->where('is_played', '=', 0)
  //      ->get()
    //    ->pipe(function ($e) {
      //      return($e->pluck('overcast_id.overcast_id'));
        //})

        $existing_records = \DB::table('played_episodes')->pluck('overcast_id')->all();
        $feeds = $crawler->filter('[type="rss"]')
            ->each(function ($crawler) {
                return $crawler->filter('[type="podcast-episode"][played="1"]')
                    ->each(function ($c) use ($crawler){
                        $url = $c->attr('enclosureUrl');
                        $overcast_id = trim(parse_url($c->attr('overcastUrl'), PHP_URL_PATH), '/'); 
                        $played_at=    \Carbon\Carbon::parse($c->attr('userUpdatedDate'));
                        $released_at=    \Carbon\Carbon::parse($c->attr('pubDate'));
                        $title = $c->attr('title');
                        $podcast = $crawler->attr('title');
                        $created_at = now();
                        $updated_at = now();
                        return compact('podcast','title','url','overcast_id','released_at','played_at', 'created_at', 'updated_at');
                    });
            });
        $feeds = array_filter(array_collapse($feeds), function ($feed) use ($existing_records){
            return !(in_array($feed['overcast_id'], $existing_records));
        });
        foreach(array_chunk($feeds, 50) as $chunk) {
            \DB::table('played_episodes')->insert($chunk);
        }
    }
}
