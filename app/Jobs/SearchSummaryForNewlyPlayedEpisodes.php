<?php
namespace App\Jobs;

use App\Domain\EpisodeReleasing\Actions\MarkEpisodeAsPlayedAction;
use App\Domain\OvercastSummaryReader;
use App\Episode;
use App\Events\EpisodePlayed;
use App\Feed;
use App\Mail\PlayedEpisodeMailable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;
use Symfony\Component\DomCrawler\Crawler;

class SearchSummaryForNewlyPlayedEpisodes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var string */
    private $filename;

    private $updatedEpisodes = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $overcast_path)
    {
        $this->filename = $overcast_path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OvercastSummaryReader $reader, MarkEpisodeAsPlayedAction $mark_as_played, Dispatcher $events)
    {
        $events->listen(EpisodePlayed::class, function (EpisodePlayed $event) {
            $episode = $event->episode;
            $this->updatedEpisodes[$episode->feed->title][] = $episode->title;
        });
        $feeds = Feed::with(['episodes' => function ($query) {
            return $query->unplayed()->released()->with('overcast_id');
        }])->get()
            ->filter(function (Feed $feed) {
                return $feed->episodes->isNotEmpty();
            })->mapWithKeys(function (Feed $feed) {
                return [
                    $feed->decorated_title => $feed->episodes->mapWithKeys(function (Episode $episode) {
                        return [$episode->overcast_id->overcast_id => $episode];
                    }),
                ];
            })->toBase();

        $contents = file_get_contents($this->filename);
        $reader->onlyIncludePlayedEpisodes()
            ->onlyIncludeFeeds($feeds->keys()->all())
            ->forEachPodcastFeed(new Crawler($contents), function (string $feed_name, array $played_episodes) use ($mark_as_played, $feeds) {
                $feeds[$feed_name]->each(function (Episode $episode, $id) use ($mark_as_played, $played_episodes) {
                    if (array_key_exists($id, $played_episodes)) {
                        $mark_as_played->execute($episode);
                    }
                });
            });

        if (count($this->updatedEpisodes)) {
            Mail::to('michael.eschbacher+podcasts@gmail.com')->send(new PlayedEpisodeMailable($this->updatedEpisodes));
        }
    }
}