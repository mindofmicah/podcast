<?php
namespace App\Jobs;

use getID3;
use Storage;
use App\Episode;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ExtractMP3Data implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $filename;

    public $tries = 2;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(getID3 $parser)
    {
        $episode = Episode::with('feed')
            ->where('filename', pathinfo($this->filename)['basename'])
            ->firstOrFail();
        
        $full_path = Storage::disk('local')
            ->path("podcasts/{$this->filename}");

        $parsed_info = $parser->analyze($full_path);

        if (is_null($episode->feed->coverart) && array_key_exists('comments', $parsed_info)) {
            $episode->feed->setCoverArt($parsed_info['comments']['picture'][0]['data']);
        }

        $episode->duration = floor($parsed_info['playtime_seconds']);
        while($tags = array_pop($parsed_info['tags'])) {
            $episode->title = current($tags['title']);
            if (current($tags['encoded_by']??[]) === 'OverDrive, Inc.' || in_array('Audiobook', $tags['genre']??[])) {
                if (is_null($episode->feed->audiobook)) {
                    event(new \App\Events\AudioBookDiscovered($episode->feed, current($tags['album']??$tags['title']), current($tags['artist'])));
                }
            }
        }
        
        $episode->save();
    }
}
