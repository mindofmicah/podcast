<?php

namespace App\Jobs;

use League\Flysystem\MountManager;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Exception;

class MoveDropboxEpisode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    private $filename;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(60 * 5);
        ini_set('memory_limit', '450M');

        $dropbox = \Storage::disk('dropbox');
        $local = \Storage::disk('local');

        if ($local->exists('podcasts/' . $this->filename)) {
            $dropbox_size = $dropbox->getMetadata($this->filename)['size'];
            $local_size   = $local->size('podcasts/' . $this->filename);

            if ($dropbox_size < $local_size) {
                throw new Exception('Something crazy happened');
            }

            if ($dropbox_size == $local_size) {
                return true;
            }

            $local->delete('podcasts/' . $this->filename);
        }

        $manager = new MountManager([
            'dropbox' => $dropbox->getDriver(),
            'local'   => $local->getDriver(),
        ]);

        try {
            if (!$manager->move('dropbox://' . $this->filename, 'local://podcasts/' . $this->filename)) { 
                throw new Exception('the move didn\'t work');
            };
        } catch (\League\Flysystem\FileNotFoundException $exception) {
            // Tried adding a file that doesn't actually exist
            \Log::info($this->filename . ' cannot be found');
        } catch (\League\Flysystem\FileExistsException $exception) {
            // The file is already there? move on
            \Log::info($this->filename . ' is already on the remote server');
        }

        ExtractMP3Data::dispatch($this->filename);
        UploadEpisodeToAWS::dispatch($this->filename);
    }

    public function failed(Exception $e)
    {
        \Log::info($e->getMessage());
    }
}
