<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class UploadEpisodeToAWS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $filename;

    public $tries = 5;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        list($folder, $filename) = explode('/', $this->filename, 2);

        try {
            $file_to_copy = new \Illuminate\Http\File(storage_path('app/podcasts/' . $this->filename));
        }catch(FileNotFoundException $e) {
            \Log::info('File scheduled for AWS upload could not be found', [
                'filename' => $this->filename
            ]);
            return;
        }

        \Storage::disk('s3')->putFileAs($folder, $file_to_copy, $filename);
    }
}
