<?php
namespace App\Jobs;

use App\Domain\Services\YouTube\YouTubeDownloader;
use App\Exceptions\VideoHasNotPremieredException;
use App\Exceptions\VideoIsPrivateException;
use getID3;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Carbon;
use Log;
use Storage;
use YoutubeDl\Entity\Video;

class DownloadYoutubeAsAudio implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 720;
    public $tries = 3;
    /**
     * @var string
     */
    private $video_id;
    /**
     * @var string
     */
    private $feed_path;
    /** @var string */
    private $url;
    /** @var bool */
    private $debug;
    /** @var boolean */
    private $should_upload_to_aws = true;

    /**
     * Create a new job instance.
     *
     * @param string $video_id
     * @param string $feed_path
     * @param bool $debug
     */
    public function __construct(string $video_id, string $feed_path, bool $debug = false)
    {
        $video_id = str_replace('.mp3', '', $video_id);
        $this->video_id = $video_id;
        $this->url = 'https://www.youtube.com/watch?v=' . $this->video_id;
        $this->feed_path = $feed_path;
        $this->debug = $debug;
    }

    public function onlyStoreLocally()
    {
        $this->should_upload_to_aws = false;
    }

    /**
     * Execute the job.
     *
     * @param YouTubeDownloader $downloader
     * @param getID3 $ID3
     *
     * @return bool
     */
    public function handle(YouTubeDownloader $downloader, getID3 $ID3)
    {
        $downloaded_file = null;
        $output_directory = $this->outputDirectory();
        $downloader->setDownloadPath($output_directory);

        $filename = "$this->video_id.mp3";
        $absolute_filename = "$output_directory/$filename";

        try {
            $expected_duration = $downloader->getExpectedDuration($this->url);
            if (file_exists($absolute_filename)) {
                $log_message = "$filename already exists: ";
                $current_duration = (int)floor($ID3->analyze($absolute_filename)['playtime_seconds']);

                // Allow some wiggle room for rounding
                if (abs($expected_duration - $current_duration) < 5) {
                    $log_message .= ' file looks good, calling it a day :)';
                    Log::info($log_message);
                    return true;
                }
                Log::info($log_message . ' file has an unexpected duration (' . $current_duration . ' vs ' . $expected_duration . '). Deleting current file and trying again');
                unlink($absolute_filename);
                $this->rerun();
                return true;
            }

            if ($this->debug) {
                $downloader->enableDebugging();
            }

            $downloaded_file = $downloader->downloadAudioVersion($this->url, ['output' => pathinfo($filename)['filename'] . '.%(ext)s']);

            if (abs($expected_duration - $downloaded_file->getDuration()) > 5) {
                Log::info('Downloaded file, but the duration didn\'t match (' . $downloaded_file->getDuration() . ' vs ' . $expected_duration . '). Deleting current file and trying again');
                unlink($absolute_filename);
                $this->rerun();
                return true;
            }
        } catch (VideoHasNotPremieredException $e) {
            Log::info($this->video_id . '(' . $this->feed_path . ') postponed: ' . $e->getMessage());

            $this->rerun($e->getPremiere());
            return true;
        } catch (VideoIsPrivateException $e) {
            Log::info($this->video_id . '(' . $this->feed_path . ') marked as private: ignored');
            return true;
        }
        $filename = "$this->feed_path/{$filename}";

        if ($this->should_upload_to_aws) {
            UploadEpisodeToAWS::dispatch($filename);
        }

        return $downloaded_file;
    }

    private function outputDirectory()
    {
        $storage = Storage::disk('local');

        if (!$storage->exists('podcasts/' . $this->feed_path)) {
            $storage->makeDirectory('podcasts/' . $this->feed_path);
        }

        return $storage->getAdapter()->applyPathPrefix('podcasts/' . $this->feed_path);
    }

    /**
     * @param Carbon|null $delay
     *
     * @return \Illuminate\Foundation\Bus\PendingDispatch
     */
    private function rerun(Carbon $delay = null)
    {
        $job = new self($this->video_id, $this->feed_path, (bool)$this->debug);
        $job->tries--;
        return dispatch($job)->delay($delay);
    }
}
