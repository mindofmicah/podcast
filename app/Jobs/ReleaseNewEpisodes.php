<?php
namespace App\Jobs;

use App\Domain\EpisodeReleasing\Actions\ReleaseNextEpisodesForFeedAction;
use App\Feed;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ReleaseNewEpisodes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $feed;
    /** @var int */
    private $d;

    /**
     * Create a new job instance.
     *
     * @param Feed $feed
     * @param int $delay
     */
    public function __construct(Feed $feed, int $delay = 5)
    {
        $this->feed = $feed;
        $this->d = $delay;
    }

    /**
     * Execute the job.
     */
    public function handle(ReleaseNextEpisodesForFeedAction $action)
    {
        return $action->execute($this->feed, $this->d);
    }
}
