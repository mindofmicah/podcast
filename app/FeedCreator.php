<?php
namespace App;

use App\Http\Requests\FeedCreationRequest;
use Illuminate\Database\Connection;
use Throwable;

class FeedCreator
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param FeedCreationRequest $request
     *
     * @return Feed
     * @throws Throwable
     */
    public function createFromRequest(FeedCreationRequest $request): Feed
    {
        return $this->connection->transaction(function () use ($request) {
            $feed = new Feed([
                'title'         => $request->title(),
                'path'          => $request->slug(),
                'episode_limit' => $request->episode_limit ?? 0,
            ]);

            $fetchable = $request->feedDetails();
            $fetchable->save();

            $feed->feedable_type = get_class($fetchable);
            $feed->feedable_id = $fetchable->getKey();

            $feed->save();

            if ($releaser = $request->defaultReleaser()) {
                $feed->releasables()->attach($releaser->getKey(), ['options' => $request->defaultReleaserOptions()]);
            }

            if (!empty($filters = $request->filters())) {
                $feed->filters()->createMany($filters);
            }

            $request->onCreate($feed);
            return $feed;
        });
    }
}