<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\OvercastId
 *
 * @property int $id
 * @property int $overcastable_id
 * @property string $overcastable_type
 * @property string $overcast_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Model|\Eloquent $overcastable
 * @method static \Illuminate\Database\Eloquent\Builder|OvercastId newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OvercastId newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OvercastId query()
 * @mixin \Eloquent
 */
class OvercastId extends Model
{
    protected $fillable = [
        'overcast_id',
    ];

    public function overcastable()
    {
        return $this->morphTo();
    }

    /**
     * @param array $ids
     * @return bool
     */
    public function matchesAny(array $ids): bool
    {
        return in_array('/' . $this->overcast_id, $ids);
    }
}
