<?php
namespace App;

use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\Contracts\Fetchable;
use App\FeedSources\Funnels\Fetcher;
use Illuminate\Database\Eloquent\Model;

/**
 * App\FunnelledFeed
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Feed|null $feed
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Feed[] $feeds
 * @property-read int|null $feeds_count
 * @method static \Illuminate\Database\Eloquent\Builder|FunnelledFeed newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FunnelledFeed newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FunnelledFeed query()
 * @mixin \Eloquent
 */
class FunnelledFeed extends Model implements Fetchable
{
    protected $touches = ['feed'];

    public function feed()
    {
        return $this->morphOne(Feed::class, 'feedable', 'feedable_type', 'feedable_id');
    }

    public function fetcher(): InformationFetchable
    {
        return new Fetcher;
    }

    public function feeds()
    {
        return $this->belongsToMany(Feed::class,'funnelled_feed_feeds', 'funnelled_feed_id', 'feed_id');
    }
}
