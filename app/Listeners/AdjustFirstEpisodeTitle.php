<?php
namespace App\Listeners;

use App\Events\FeedWasCreated;
use App\Episode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdjustFirstEpisodeTitle
{
    /**
     * Handle the event.
     *
     * @param  FeedWasCreated  $event
     * @return void
     */
    public function handle(FeedWasCreated $event)
    {
        $first_episode = $event->getFeed()->episodes()->latest()->first();

        if (is_null($first_episode)) {
            return;
        }

        if (!$this->titleStartsWithEmoji($first_episode)) {
            $first_episode->update([
                'title'=> "{$this->fetchRandomEmoji()} $first_episode->title"
            ]);
        }

        return;
    }

    private function titleStartsWithEmoji(Episode $episode)
    {
        return strlen($episode->title) != mb_strlen($episode->title);
    }

    private function fetchRandomEmoji()
    {
        return html_entity_decode(
            json_decode(
                file_get_contents('https://ranmoji.herokuapp.com/emojis/api/v.1.0/'),
                true
            )['emoji']
        );
       
    }
}
