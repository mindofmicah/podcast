<?php
namespace App\Listeners;

use App\Events\EpisodeDiscovered;
use Exception;
use getID3;

/**
 * Class RegisterNewEpisode
 * @package App\Listeners
 */
class RegisterNewEpisode
{
    /**
     * Handle the event.
     *
     * @param EpisodeDiscovered $event
     * @return void
     * @throws Exception
     */
    public function handle(EpisodeDiscovered $event)
    {
        $options = $this->parsePodcastOptionsFromFile($event->full_filename);

        if (empty($options['title'])) {
            throw new Exception("A title cannot be found");
        }

        if (empty($options['description'])) {
            $options['description'] = '';
        }

        if (preg_match('/^\w{4}$/', pathinfo($event->full_filename)['filename'])) {
            $generated_filename = $this->generateNewFilename($options['title']);

            $full_generated_name = pathinfo($event->full_filename, PATHINFO_DIRNAME) . '/' . $generated_filename;
            if ($event->full_filename != $full_generated_name && !file_exists($full_generated_name)) {
                rename($event->full_filename, $full_generated_name);
            }
        } else {
            $generated_filename = $event->short_name;
            $full_generated_name = $event->full_filename;
        }

        $options['size'] = filesize($full_generated_name);
        $options['url'] = 'https://podcasts.whamdonk.com/podcasts/' . $event->feed->path . '/' . $generated_filename;

        $event->feed->episodes()->create([
            'filename'   => $generated_filename,
            'options'    => $options,
            'created_at' => filemtime($full_generated_name),
            'updated_at' => filemtime($full_generated_name),
        ]);
    }

    /**
     * @param string $full_filename
     * @return array
     */
    private function parsePodcastOptionsFromFile(string $full_filename): array
    {
        $results = (new getID3)->analyze($full_filename);

        $map = [
            'title'       => 'title',
            'description' => 'subtitle',
            'artist'      => 'artist',
        ];
        $options = [
            'duration' => ceil($results['playtime_seconds']),
            'type'     => 'podcast',
        ];

        /** @var array $tag_collection */
        // Loop through the different types of tags (v2, v3) and find the first instance of each podcast tag
        foreach (array_reverse($results['tags']) as $tag_type => $tag_collection) {
            // Only look at mapped elements that haven't been found in an earlier tag
            foreach (array_diff_key($map, $options) as $podcast_tag => $id3_tag) {
                // If the tag has an actual value, it will be stored as an array
                // add those values to the $options array with the proper key
                if (is_array($tag_collection[$id3_tag] ?? null)) {
                    $options[$podcast_tag] = implode(', ', $tag_collection[$id3_tag]);
                }
            }
        }

        return $options;
    }

    /**
     * @param string $title
     * @return string
     */
    private function generateNewFilename(string $title): string
    {
        return strtolower(preg_replace(['/\s+/', '/[^\w-]/'], ['-', ''], $title)) . '.mp3';
    }

    private function isIPODName($full_filename)
    {
        dd(get_defined_vars(), __FILE__, __LINE__);
    }
}
