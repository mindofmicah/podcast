<?php
namespace App\Listeners;

use App\Events\EpisodeAssignedOvercastId;
use App\Mail\OvercastEpisodeMailable;
use Mail;

class SendNotificationForAssignedOvercastEpisode
{
    /**
     * Handle the event.
     *
     * @param  EpisodeAssignedOvercastId  $event
     * @return void
     */
    public function handle(EpisodeAssignedOvercastId $event)
    {
        Mail::to('michael.eschbacher+podcasts@gmail.com')->send(new OvercastEpisodeMailable($event->episode));

    }
}
