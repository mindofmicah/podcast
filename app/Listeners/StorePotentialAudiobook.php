<?php

namespace App\Listeners;

use App\Events\AudioBookDiscovered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class StorePotentialAudiobook
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AudioBookDiscovered  $event
     * @return void
     */
    public function handle(AudioBookDiscovered $event)
    {
        return $event->feed->audiobook()->create([
            'title'  => $event->title,
            'author' => $event->author,
        ]);
    }
}
