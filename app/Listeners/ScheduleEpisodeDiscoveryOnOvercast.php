<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ScheduleEpisodeDiscoveryOnOvercast
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(\App\Events\EpisodeReleased $event)
    {
        \Log::info('episode was discovered');

        \App\Jobs\AssignOvercastEpisodeId::dispatch($event->episode)
            ->delay(now()->addMinutes(10));
    }
}
