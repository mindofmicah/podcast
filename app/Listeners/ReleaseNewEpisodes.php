<?php
namespace App\Listeners;

use App\Domain\EpisodeReleasing\Actions\ReleaseNextEpisodesForFeedAction;
use App\Events\Contracts\RelatesToFeed;
use App\Exceptions\FeedCannotReleaseEpisodesException;

class ReleaseNewEpisodes
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ReleaseNextEpisodesForFeedAction $action)
    {
        $this->action = $action;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(RelatesToFeed $event)
    {
        $feed = $event->getFeed();

        try {
            $this->action->execute($feed);
        } catch(FeedCannotReleaseEpisodesException $e){
            dd($e);
        }
    }
}
