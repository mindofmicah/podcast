<?php
namespace App;
trait OvercastIds
{
    public function setOvercastId($overcast_id)
    {
        $overcast_id = ltrim($overcast_id, '/');
        $this->overcast_id()->create([
            'overcast_id' => $overcast_id,
        ]);
    }

    public function overcast_id()
    {
        return $this->morphOne(OvercastId::class, 'overcastable');
    }

}
