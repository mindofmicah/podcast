<?php
namespace App;

use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\Contracts\Fetchable;
use App\FeedSources\Dropbox\Fetcher;
use Illuminate\Database\Eloquent\Model;

/**
 * App\DropboxFeed
 *
 * @property int $id
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Feed|null $feed
 * @method static \Illuminate\Database\Eloquent\Builder|DropboxFeed newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DropboxFeed newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DropboxFeed query()
 * @mixin \Eloquent
 */
class DropboxFeed extends Model implements Fetchable
{
    public function feed()
    {
        return $this->morphOne(Feed::class, 'feedable', 'feedable_type', 'feedable_id');
    }

    public function fetcher(): InformationFetchable
    {
        return new Fetcher;
    }
}
