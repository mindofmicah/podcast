<?php
namespace App;

class EpisodeScanner
{
    private $podcast_path;

    /**
     * EpisodeScanner constructor.
     * @param string $podcast_path
     */
    public function __construct(string $podcast_path)
    {
        $this->podcast_path = rtrim($podcast_path, '/') . '/';
    }

    /**
     * @param Feed $feed
     * @return array
     */
    public function scanForNewEpisodes(Feed $feed): array
    {
        $episode_names = $feed->episodes()->pluck('filename');

        $local_path = $this->podcast_path . $feed->path . '/';
        $filenames = array_sort(glob($local_path . '*.{mp3,mp4a}', GLOB_BRACE), function ($filename) {
            return filemtime($filename) . pathinfo($filename, PATHINFO_BASENAME);
        });

        $ret = [];

        foreach ($filenames as $full_filename) {
            $short_name = preg_replace('/^' . preg_quote($local_path, '/') . '/', '', $full_filename);
            if (!$episode_names->contains($short_name)) {
                $ret[$short_name] = $full_filename;
            }
        }

        return $ret;
    }
}
