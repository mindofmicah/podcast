<?php
namespace App;

use App\Domain\Common\Models\PodcastFeed;
use App\Domain\EpisodeDiscovery\DiscoverNewEpisodesRequest;
use App\Domain\EpisodeDiscovery\EloquentEpisodeRepository;
use App\Domain\EpisodeDiscovery\UseCases\DiscoverNewEpisodes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Image;

/**
 * Class Feed
 *
 * @package App
 * @property int $id
 * @property string $title
 * @property string $path
 * @property int|null $episode_limit
 * @property string|null $remote_path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $feed_source
 * @property string|null $feedable_type
 * @property int|null $feedable_id
 * @property string|null $coverart
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property bool $is_active
 * @property int $scan_schedule
 * @property-read \App\Audiobook|null $audiobook
 * @property-read Model|\Eloquent $details
 * @property-read \Illuminate\Database\Eloquent\Collection|Episode[] $episodes
 * @property-read int|null $episodes_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Filter[] $filters
 * @property-read int|null $filters_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\FunnelledFeed[] $funnelConfigurations
 * @property-read int|null $funnel_configurations_count
 * @property-read mixed $base_url
 * @property-read mixed $decorated_title
 * @property-read string $image_path
 * @property-read \App\OvercastId|null $overcast_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Common\Models\ReleaseScheduleDecorator[] $releasables
 * @property-read int|null $releasables_count
 * @method static Builder|Feed active()
 * @method static Builder|Feed newModelQuery()
 * @method static Builder|Feed newQuery()
 * @method static \Illuminate\Database\Query\Builder|Feed onlyTrashed()
* // * @method static Builder|Feed query()
 * @method static Builder|Feed shouldScanForDay(\Carbon\Carbon $day)
 * @method static Builder|Feed ignoreFunnelledFeeds()
 * @method static Builder|Feed ofType($feed_type)
 * @method static Builder|Feed withInactive()
 * @method static \Illuminate\Database\Query\Builder|Feed withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Feed withoutTrashed()
 * @mixin \Eloquent
 */
class Feed extends PodcastFeed
{
    public function filters()
    {
        return $this->hasMany(Filter::class, 'feed_id');
    }

    public function getBaseUrlAttribute()
    {
        return '/feeds/' . $this->path;
    }

    public function getDecoratedTitleAttribute()
    {
        return 'Magic Playlist - ' . $this->attributes['title'];
    }

    public function fetchInformation($flags = 0): FeedSources\FetchedInformation
    {
        return (new DiscoverNewEpisodes(new EloquentEpisodeRepository()))->handle(new DiscoverNewEpisodesRequest($this, $flags));
    }

    public function renameEpisodesByPattern(string $pattern_str)
    {
        $pattern = new NamingPattern($pattern_str);

        foreach ($this->episodes as $episode) {
            $episode->renameByPattern($pattern);
        }
    }

    /**
     * @return mixed
     */
    public function unplayedEpisodes()
    {
        return $this->episodes()->whereUnplayed();
    }

    public function audiobook()
    {
        return $this->hasOne(Audiobook::class, 'feed_id');
    }

    public function clearCoverart(bool $delete_file = false): bool
    {
        if ($delete_file && (parse_url($this->attributes['coverart'], PHP_URL_SCHEME) === null)) {
            $storage = \Storage::disk('local');
            $storage->delete($this->attributes['coverart']);
        }
        return $this->update([
            'coverart' => null,
        ]);
    }

    /**
     * @param string|Image $image_data
     *
     * @return bool
     */
    public function setCoverart($image_data)
    {
        $path = "coverart/{$this->attributes['path']}/coverart.jpg";
        $storage = \Storage::disk('local');

        if ($image_data instanceof Image) {
            $image_data = (string)$image_data->encode('jpg');
        }

        $storage->put($path, $image_data);

        return $this->update([
            'coverart' => $path,
        ]);
    }

    /**
     * Create a new model instance that is existing.
     *
     * @param array $attributes
     * @param string|null $connection
     *
     * @return static
     */
    public function newFromBuilder($attributes = [], $connection = null)
    {
        $model = (property_exists($attributes, 'feedable_type') && $attributes->feedable_type === FunnelledFeed::class)
            ? (new FunnelFeed())->newInstance($attributes, true)
            : $this->newInstance($attributes, true);

        $model->setRawAttributes((array)$attributes, true);

        $model->setConnection($connection ? : $this->getConnectionName());

        $model->fireModelEvent('retrieved', false);

        return $model;
    }

    public function funnelConfigurations()
    {
        return $this->belongsToMany(FunnelledFeed::class, 'funnelled_feed_feeds','feed_id', 'funnelled_feed_id');
    }

}
