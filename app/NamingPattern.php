<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class NamingPattern
{
    public function __construct($pattern)
    {
        if (strpos($pattern, '{') === false) {
            throw new \Exception;
        }
        $this->pattern = $pattern;
    }

    public function rename(Model $model)
    {
        $model_attributes = $model->getAttributes();

        return preg_replace_callback('/\{('.implode('|', array_map('strtoupper', array_keys($model_attributes))).')\}/', function ($match) use ($model_attributes) {
            return $model_attributes[strtolower($match[1])]??$match[0];

        }, $this->pattern);
    }
}
