<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource as Resource;

class ActiveFeedResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attributes = $this->only(['id', 'title', 'path', 'episode_limit', 'image_path']);
        $attributes['updated'] = [
            'raw'      => $this->updated_at->timestamp,
            'readable' => $this->updated_at->diffForHumans(),
        ];

//        $attributes['episode_count'] = $this->episodes()->count();
        $attributes['links'] = [
            'edit' => route('feeds.edit', $this->path),
            'show' => route('feeds.show', $this->path),
        ];
        return $attributes;
    }
}
