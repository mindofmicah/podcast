<?php
namespace App\Http\Controllers;

use App\Feed;

use Illuminate\Http\Request;

class EpisodeNamingPatternsController extends Controller
{
    public function store(Request $request, Feed $feed)
    {
        $feed->renameEpisodesByPattern($request->pattern);
        return response()->json([], 201);
    }
}
