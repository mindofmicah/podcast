<?php
namespace App\Http\Controllers\Overcast;

use Illuminate\Http\Request;
use MindOfMicah\OvercastSDK\OvercastSDK;

class ListOvercastFeedPlayedEpisodesController
{
    const DEFAULT_LIMIT = 25;

    /** @var OvercastSDK */
    private $overcastSDK;

    public function __construct(OvercastSDK $overcastSDK)
    {
        $this->overcastSDK = $overcastSDK;
    }

    public function __invoke($overcast_feed, OvercastSDK $overcast, Request $request)
    {
        $limit = $request->get('limit', self::DEFAULT_LIMIT);
        $episodes = $this->overcastSDK->getFeedEpisodes($overcast_feed);

        $played = [];

        while (($episode = array_shift($episodes)) && (count($played) < $limit)) {
            if ($episode['is_played']) {
                $played[] = array_merge($episode, $overcast->getEpisodeDetails($episode['id']));
            }
        }
        return response()->json($played);
    }
}