<?php
namespace App\Http\Controllers\Overcast;

class ShowOvercastFeedRSSUrlController
{
    public function __invoke($overcast_feed)
    {
        if (!preg_match('/itunes([\d]+)/', $overcast_feed, $matches)) {
            abort(500);
        }

        $itunes_id = $matches[1];
        $itunes_data = json_decode(trim(file_get_contents('https://itunes.apple.com/lookup?id=' . $itunes_id . '&entity=podcast')), true);

        // If there weren't any results for the feed, error
        if ($itunes_data['resultCount'] == 0) {
            abort(500);
        }

        return response()->json([
            'data' => $itunes_data['results'][0]['feedUrl'],
        ]);
    }
}