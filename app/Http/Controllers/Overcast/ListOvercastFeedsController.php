<?php
namespace App\Http\Controllers\Overcast;

use Inertia\Inertia;
use MindOfMicah\OvercastSDK\OvercastSDK;

class ListOvercastFeedsController
{
    /** @var OvercastSDK */
    private $overcastSDK;

    public function __construct(OvercastSDK $overcastSDK)
    {
        $this->overcastSDK = $overcastSDK;
    }

    public function __invoke()
    {
        $listings = collect($this->overcastSDK->getFeeds())
            ->reject(function (array $data) {
                return starts_with($data['title'], 'Magic Playlist');
            })
            ->map(function (array $data) {
                // Normalize itunes ids
                $data['id'] = explode('/', $data['id'])[0];
                return $data;
            })
            ->sortBy('title')
            ->values();

        return Inertia::render('ListOvercastFeeds', compact('listings'))
            ->withViewData('svelte', true);
    }
}
