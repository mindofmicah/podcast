<?php
namespace App\Http\Controllers;

use App\Feed;
use App\Http\Resources\ActiveFeedResource;

class ActiveFeedsController extends Controller
{
    public function index()
    {
        return ActiveFeedResource::collection(Feed::active()->get());
    }
    public function destroy(Feed $feed)
    {
        $feed->delete();
        return response()->json();
    }
}
