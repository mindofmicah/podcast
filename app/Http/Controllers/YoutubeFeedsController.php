<?php
namespace App\Http\Controllers;

use App\Domain\EpisodeDiscovery\DiscoverNewEpisodesRequest;
use App\Domain\EpisodeDiscovery\UseCases\DiscoverNewEpisodes;
use App\FeedCreator;
use App\FeedSources\YouTube\Fetcher;
use App\Http\Requests\YouTubeFeedCreationRequest;
use Illuminate\Http\Request;
use Inertia\Inertia;

class YoutubeFeedsController extends Controller
{
    public function create()
    {
        return Inertia::render('CreateYoutubeFeed');
    }

    /**
     * @param YouTubeFeedCreationRequest $request
     * @param FeedCreator $creator
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Throwable
     */
    public function store(YouTubeFeedCreationRequest $request, FeedCreator $creator, DiscoverNewEpisodes $use_case)
    {
        $feed = $creator->createFromRequest($request);

        $use_case->handle(new DiscoverNewEpisodesRequest($feed, $request->limit_to_one === 'on' ? Fetcher::STOP_AT_ONE : 0));

        event(new \App\Events\FeedWasCreated($feed));

        return redirect('/feeds/' . $feed->path . '/edit');
    }

    public function show(Request $request, $slug)
    {
        dd(get_defined_vars(), 'show');
        // code...
    }
}
