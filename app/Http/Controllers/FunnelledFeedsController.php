<?php
namespace App\Http\Controllers;

use App\Events\FeedWasCreated;
use App\Feed;
use App\FeedCreator;
use App\Http\Requests\FunnelledFeedCreationRequest;
use Inertia\Inertia;

class FunnelledFeedsController extends Controller
{
    public function create()
    {
        return Inertia::render('CreateFunnelledFeed', [
            'feeds' => Feed::orderBy('title')
                ->doesntHave('funnelConfigurations')
                ->get(['title', 'id', 'path']),
        ]);
    }

    public function store(FunnelledFeedCreationRequest $request, FeedCreator $creator)
    {
        $feed = $creator->createFromRequest($request);
        $feed->load('details.feeds');

        event(new FeedWasCreated($feed));

        return redirect('/feeds/' . $feed->path . '/edit');
    }
}
