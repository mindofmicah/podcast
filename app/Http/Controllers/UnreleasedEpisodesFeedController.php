<?php
namespace App\Http\Controllers;

use App\Feed;
use App\PodcastFeed;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UnreleasedEpisodesFeedController extends Controller
{
    public function show(Feed $feed, PodcastFeed $feeder, Request $request)
    {
        $latest_update = $feed->updated_at;

        $headers = [
            'Last-Modified' => $latest_update->setTimezone('GMT')->format('D, d M Y H:i:s \G\M\T'),
        ];

        if ($request->hasHeader('if-modified-since')) {
            $last_check = new Carbon($request->header('if-modified-since'));
            if ($latest_update->lessThan($last_check)) {
                return response('', 304, $headers);
            }
        }

        $xml = (string)$feeder->buildPodcastFeed($feed, false);

        return response($xml, 200, $headers)
            ->header('Content-Type', 'text/xml');
    }
}
