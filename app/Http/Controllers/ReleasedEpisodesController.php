<?php
namespace App\Http\Controllers;

use App\Domain\EpisodeReleasing\Actions\ReleaseEpisodeAction;
use App\Episode;
use Illuminate\Http\Request;

class ReleasedEpisodesController extends Controller
{
    public function store(Request $request, ReleaseEpisodeAction $action)
    {
        $input = $request->validate([
            'episode_id' => 'required'
        ]);

        $action->execute(Episode::find($input['episode_id']));

        return back();
    }
}
