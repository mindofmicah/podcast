<?php

namespace App\Http\Controllers\Artisan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommandsController extends Controller
{
    public function __construct()
    {
        \View::composer('artisan.commands.navigation', function ($view) {
        $command_names = array_keys(\Artisan::all());


        $excluded_commands = [
            'up', 'down', 'help', 'list', 'env', 'clear-compiled', 'optimize', 'vendor:publish' 
        ];
        $excluded_namespaces = [
            'auth', 'app', 'db', 'event', 'schedule', 'storage',
            'key', 'route', 'session', 'notifications',
        ];


        list($namespaced, $misc) = collect($command_names)->diffKeys($excluded_commands)->partition(function ($a) {
            return strpos($a, ':');
        });

        $navigation = $namespaced->sort()->map(function ($command_name) {
           $chunks = explode(':', $command_name);
           return [
               'namespace'=>$chunks[0],
               'label'=>$chunks[1],
               'command'=>$command_name,
           ];
        })->groupBy('namespace')
        ->diffKeys(array_flip($excluded_namespaces))
        ->put('miscellaneous', $misc->map(function ($command_name) {
            return array_fill_keys(['namespace', 'label', 'command'], $command_name);
        }));

            $view->with('navigation', $navigation);
        });
    }
    public function index()
    {

        return view('artisan.commands.index', get_defined_vars());
        dd(get_Defined_Vars());
    }

    public function show($command_name)
    {
        $command = \Artisan::all()[$command_name];

        dd($command, get_class_methods($command));
        return view('artisan.commands.show', get_defined_vars());
    }
}
