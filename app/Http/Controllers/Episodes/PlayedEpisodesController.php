<?php
namespace App\Http\Controllers\Episodes;

use App\Domain\Common\Models\Episode;
use App\Domain\EpisodeReleasing\Actions\MarkEpisodeAsPlayedAction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlayedEpisodesController extends Controller
{
    public function store(Request $request, MarkEpisodeAsPlayedAction $action)
    {
        $inputs = $request->validate(['episode_id' => 'required']);
        $action->execute(Episode::findOrFail($inputs['episode_id']));
        return back();
    }
}