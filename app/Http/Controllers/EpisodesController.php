<?php
namespace App\Http\Controllers;

use App\Domain\EpisodeReleasing\Actions\MarkEpisodeAsPlayedAction;
use App\Feed;
use Illuminate\Http\Request;

class EpisodesController extends Controller
{
    public function update(Feed $feed, Request $request, \App\Episode $episode, MarkEpisodeAsPlayedAction $action)
    {
        $inputs = $request->only(['is_played']);

        if ($inputs['is_played']) {
            $action->execute($episode);
        } else {
            $episode->update($inputs);
        }

        $feed->touch();

        return redirect()->to(route('feeds.episodes.index', ['feed_name' => $feed->path]));
    }

    public function create(Feed $feed)
    {
        return view('episodes.create', compact('feed'));
    }

    public function store(Feed $feed, Request $request)
    {
        $input = $request->validate(['url' => 'required']);
        $path_info = pathinfo($input['url']);
        $filename = $path_info['basename'];
        $no_extension = $path_info['filename'];

        $feed->episodes()->create([
            'filename' => $filename,
            'options'  => [
                'title'       => strtr(urldecode($no_extension), '_', ' '),
                'url'         => $input['url'],
                'type'        => 'podcast',
                'description' => '',
            ],
        ]);

        return redirect()->to(route('feeds.episodes.index', ['feed_name' => $feed->path]));
    }
}
