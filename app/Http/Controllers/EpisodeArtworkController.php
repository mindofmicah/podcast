<?php

namespace App\Http\Controllers;

use App\Episode;
use Illuminate\Http\Request;

use Intervention\Image\ImageManager;

class EpisodeArtworkController extends Controller
{
    public function show($episode_id)
    {
        $episode = Episode::with(['feed' => function ($query) {
            $query->withTrashed();
        }])->findOrFail($episode_id);

        $options = $episode->options;

        $imager = new ImageManager;

        $feed_image_path = $episode->feed->image_path;
        if (empty($options['cover_art_is_sequenced'])) {
            return response(
                file_get_contents($feed_image_path), 200, [
                    'Content-Type' => 'image/png',
                ]
            );
        }

        $imager = new ImageManager;
        $cover_art = $imager->make($feed_image_path);

        $color = '#' . substr(md5($episode->options['title']), 0, 6);

        $angle = ($episode->order % 5 + 2);
        $angle_shadow = ($episode->order % 5 + 2) + 2;
        if ($episode->order % 2) {
            $angle = 0 - $angle;
            $angle_shadow = 0 - $angle_shadow;
        }

        // Number shadow
        $cover_art->text($episode->order, $cover_art->width() / 2 + 2, $cover_art->width() / 2 + 2, function ($font) use ($angle_shadow) {
            $font->file(public_path('/fonts/Oswald-Bold.ttf'));
            $font->color([0, 0, 0, .8]);
            $font->size(80);
            $font->align('center');
            $font->valign('middle');
            $font->angle($angle_shadow);
        });

        // Actual Number
        $cover_art->text($episode->order, $cover_art->width() / 2, $cover_art->width() / 2, function ($font) use ($angle, $color) {
            $font->file(public_path('/fonts/Oswald-Bold.ttf'));
            $font->color($color);
            $font->size(80);
            $font->align('center');
            $font->valign('middle');
            $font->angle($angle);
        });

        return $cover_art->response();
    }
}
