<?php
namespace App\Http\Controllers\Dropbox;

use App\Domain\EpisodeDiscovery\DiscoverNewEpisodesRequest;
use App\Domain\EpisodeDiscovery\UseCases\DiscoverNewEpisodes;
use App\FeedCreator;
use App\Http\Requests\DropboxFeedCreationRequest;
use App\Http\Resources\ActiveFeedResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActiveFeedsController extends Controller
{
    /**
     * @param Request $request
     * @param FeedCreator $feed_creator
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function store(DropboxFeedCreationRequest $request, FeedCreator $feed_creator, DiscoverNewEpisodes $use_case)
    {
        $feed = $feed_creator->createFromRequest($request);

        $use_case->handle(new DiscoverNewEpisodesRequest($feed));

        event(new \App\Events\FeedWasCreated($feed));
//        $this->dispatch(new \App\Jobs\PullCoverArtFromFile($feed));

        return (new ActiveFeedResource($feed))->response()->setStatusCode(201);
    }
}
