<?php
namespace App\Http\Controllers\Dropbox;

use App\Feed;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class UnclaimedFeedsController extends Controller
{
    public function index()
    {
        $dropbox_directories = Storage::disk('dropbox')->allDirectories();

        $directories = collect($dropbox_directories)->mapWithKeys(function ($folder_name) {
            preg_match('/^(?:(\d+)-)?(.+)$/', $folder_name, $matches);
            $episode_limit = (int)$matches[1];
            $path = $matches[2];
            $name = ucwords(strtr($path, '-', ' '));

            if ($episode_limit) {
                Storage::disk('dropbox')->move($folder_name, $path);
            }

            return [$path => compact('episode_limit', 'path', 'name')];
        })->except(Feed::pluck('path')->toArray());

        return response(['data' => $directories->values()->toArray()]);
    }
}
