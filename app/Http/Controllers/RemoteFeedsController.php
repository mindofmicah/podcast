<?php
namespace App\Http\Controllers;

use App\Domain\EpisodeDiscovery\DiscoverNewEpisodesRequest;
use App\Domain\EpisodeDiscovery\UseCases\DiscoverNewEpisodes;
use App\Feed;
use App\FeedCreator;
use App\Http\Requests\RemoteFeedCreationRequest;
use Illuminate\Http\Request;
use Inertia\Inertia;

class RemoteFeedsController extends Controller
{
    public function create(Request $request)
    {
        $fields = ['title', 'path', 'episode_limit', 'start_after', 'filters'];

        $input = array_merge(array_fill_keys($fields, null), ['episode_limit' => 1], array_merge($request->only($fields)));

        if (is_null($input['title']) && $input['path']) {
            $input['title'] = (ucwords(strtr(pathinfo($input['path'], PATHINFO_BASENAME), '-', ' ')));
        }

        return Inertia::render('CreateRemoteFeed', $input);
    }

    /**
     * @param RemoteFeedCreationRequest $request
     * @param FeedCreator $feed_creator
     *
     * @throws \Throwable
     */
    public function store(RemoteFeedCreationRequest $request, FeedCreator $feed_creator, DiscoverNewEpisodes $use_case)
    {
        $feed = $feed_creator->createFromRequest($request);

        // Should we skip ahead to a certain episode?
        if ($request['start_after']) {
            $feed->episodes->add([
                'url' => $request['start_after'],
            ]);
        }

        $use_case->handle(new DiscoverNewEpisodesRequest($feed));
        event(new \App\Events\FeedWasCreated($feed));

        return redirect('/feeds/' . $feed->path . '/edit');
    }
}
