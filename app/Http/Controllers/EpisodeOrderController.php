<?php
namespace App\Http\Controllers;

use App\Episode;
use DB;
use Illuminate\Http\Request;

class EpisodeOrderController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            '*.order'      => 'required',
            '*.episode_id' => 'required',
        ]);

        DB::transaction(function () use ($request){
            $data = collect($request->all())->pluck('episode_id', 'order');

            Episode::whereIn('id', $data->values())
                ->update(['order' => null]);

            foreach ($data as $new_order => $episode_id) {
                Episode::whereKey($episode_id)->update(['order' => $new_order]);
            }
        });

        return back();
    }
}
