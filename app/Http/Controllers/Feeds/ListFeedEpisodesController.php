<?php
namespace App\Http\Controllers\Feeds;

use App\Domain\EpisodeReleasing\ViewModels\ListFeedEpisode;
use App\Feed;
use Illuminate\Contracts\Routing\UrlGenerator;
use Inertia\Inertia;

class ListFeedEpisodesController
{
    /** @var UrlGenerator */
    private $url_generator;

    /**
     * ListFeedEpisodesController constructor.
     */
    public function __construct(UrlGenerator $url_generator)
    {
        $this->url_generator = $url_generator;
    }

    /**
     * @param Feed $feed
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke(Feed $feed)
    {
        return Inertia::render(
            'ListEpisodes',
            new ListFeedEpisode($feed, $this->url_generator)
        )->withViewData(['svelte'=>true]);
    }
}



















