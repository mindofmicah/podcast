<?php
namespace App\Http\Controllers\Feeds;

use App\Domain\Common\Models\ReleaseScheduleDecorator;
use App\Feed;
use App\Releasers\PickemDecorator;
use Illuminate\Http\Request;

class UpdatePickemReleasableController
{
    public function __invoke(Feed $feed, Request $request)
    {
        $existing = $feed->releasables()->where('class_name', PickemDecorator::class)->first();
        $options = $request->validate([
            'strategy'       => 'required',
            'selected_items' => 'required|array',
        ]);
        $options['items'] = implode(',', $options['selected_items']);
        unset($options['selected_items']);

        if (!$existing) {
            $releaser = ReleaseScheduleDecorator::where('class_name', PickemDecorator::class)->first();
            return $feed->releasables()->attach([
                $releaser->getKey() => [
                    'options' => $options,
                ],
            ]);
        }
        $existing->pivot->update([
            'options' => $options,
        ]);
    }
}