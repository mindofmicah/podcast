<?php
namespace App\Http\Controllers\Feeds;

use App\Feed;
use App\Releasers\PickemDecorator;
use Inertia\Inertia;

class EditPickemReleasableController
{
    public function __invoke(Feed $feed)
    {
        $feed->setRelation('episodes', $feed->episodes()->whereNull('released_at')->get());
        foreach ($feed->episodes as &$episode) {
            $episode['options'] = ['title' => $episode->title];
        }
        $selected = [];
        $releasable = $feed->releasables()->where('class_name', PickemDecorator::class)->first();
        $original_strategy = 'oldest';

        if ($releasable) {
            $selected = array_filter(explode(',', $releasable->pivot->options['items']));
            $original_strategy = $releasable->pivot->options['strategy'];
        }
        return Inertia::render('EditPickem', compact('feed', 'selected', 'original_strategy'));
    }
}