<?php
namespace App\Http\Controllers\Feeds;

use App\Feed;
use App\FunnelFeed;
use Illuminate\Http\RedirectResponse;
use Tzsk\Collage\MakeCollage;

class UpdateFeedCoverart
{
    const MAX_IMAGES = 4;

    /** @var MakeCollage */
    private $collage;

    /**
     * UpdateFeedCoverart constructor.
     */
    public function __construct(MakeCollage $collage)
    {
        $this->collage = $collage;
    }

    /**
     * @param Feed $feed
     *
     * @return RedirectResponse
     */
    public function __invoke(Feed $feed): RedirectResponse
    {
        if ($feed instanceof FunnelFeed) {
            $images = $feed->details->feeds->pluck('image_path')->take(self::MAX_IMAGES)->all();

            $feed->setCoverart($this->collage->make(400)->from($images));
        }

        return back();
    }
}