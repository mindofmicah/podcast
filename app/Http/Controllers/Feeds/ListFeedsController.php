<?php
namespace App\Http\Controllers\Feeds;

use App\Feed;
use App\Http\Resources\ActiveFeedResource;
use Inertia\Inertia;

class ListFeedsController
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        return Inertia::render('FeedAdmin', [
                'feeds' => ActiveFeedResource::collection(Feed::active()->get()),
            ])
            ->withViewData('svelte', true);
    }
}