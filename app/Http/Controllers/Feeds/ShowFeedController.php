<?php
namespace App\Http\Controllers\Feeds;

use App\Domain\PodcastFeeds\XMLFeedFactory;
use App\Feed;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ShowFeedController
{
    /** @var XMLFeedFactory */
    private $feeder;

    public function __construct(XMLFeedFactory $feeder)
    {
        $this->feeder = $feeder;
    }

    public function __invoke(Feed $feed, Request $request)
    {
        $latest_update = $feed->updated_at;

        $headers = [
            'Last-Modified' => $latest_update->setTimezone('GMT')->format('D, d M Y H:i:s \G\M\T'),
        ];

        if ($request->hasHeader('if-modified-since')) {
            $last_check = new Carbon($request->header('if-modified-since'));
            if ($latest_update->lessThan($last_check)) {
                return response('', 304, $headers);
            }
        }

        $xml = (string)$this->feeder->create($feed);

        return response($xml, 200, $headers)
            ->header('Content-Type', 'text/xml');
    }
}