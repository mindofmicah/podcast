<?php
namespace App\Http\Controllers\Feeds;

use App\Feed;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\FilesystemManager;
use Intervention\Image\ImageManager;

class ShowFeedCoverart
{
    /** @var FilesystemManager */
    private $filesystem;
    /** @var ImageManager */
    private $image_manager;

    public function __construct(FilesystemManager $filesystem, ImageManager $image_manager)
    {
        $this->filesystem = $filesystem;
        $this->image_manager = $image_manager;
    }

    /**
     * @param Feed $feed
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function __invoke(Feed $feed)
    {
        try {
            $coverart_contents = $this->filesystem->disk('local')->get((string)$feed->coverart);
        } catch (FileNotFoundException $e) {
            // TODO Add a fallback image for coverart
            abort(404);
            return;
        }

        $image = $this->image_manager->make($coverart_contents);
        $width = $image->getWidth();
        $height = $image->getHeight();
        if ($width !== $height) {
            $new_size = max($width, $height);
            $image->resizeCanvas($new_size, $new_size, 'center', false, '#000000');
        }
        return $image->response();
    }
}