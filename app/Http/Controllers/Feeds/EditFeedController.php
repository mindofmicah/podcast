<?php
namespace App\Http\Controllers\Feeds;

use App\Feed;
use Inertia\Inertia;

class EditFeedController
{
    public function __invoke(Feed $feed)
    {
        return Inertia::render('EditFeed', compact('feed'));
    }
}