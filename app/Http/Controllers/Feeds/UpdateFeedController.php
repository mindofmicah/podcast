<?php
namespace App\Http\Controllers\Feeds;

use App\Feed;
use Illuminate\Http\Request;

class UpdateFeedController
{
    public function __invoke(Feed $feed, Request $request)
    {
        $inputs = $request->only('title');
        $feed->update($inputs);

        return back();
    }
}