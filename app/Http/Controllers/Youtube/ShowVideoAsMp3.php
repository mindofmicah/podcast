<?php
namespace App\Http\Controllers\Youtube;

use App\Jobs\DownloadYoutubeAsAudio;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use YoutubeDl\Entity\Video;

class ShowVideoAsMp3
{
    public function __invoke($youtube_id): BinaryFileResponse
    {
        /** @var Video $file */
        $file = dispatch_now(new DownloadYoutubeAsAudio($youtube_id, 'downloads'));

        return response()->download($file->getFile()->getPathname(), $file->getTitle());
    }
}