<?php
namespace App\Http\Controllers;

use App\Feed;
use Inertia\Inertia;

class UnplayedEpisodesController extends Controller
{
    /**
     * @param Feed $feed
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Feed $feed)
    {
        $episodes = $feed->episodes()->unplayed()->orderBy('order')->get();
        $paths = [];
        $paths['feed']['edit'] = route('feeds.edit', ['feed' => $feed->path]);
        $paths['feed']['show'] = route('feeds.show', ['feed' => $feed->path]);
        $title = 'Unplayed Episodes for ' . $feed->title;

        return Inertia::render('ListEpisodes', compact('feed', 'episodes', 'paths', 'title'));
    }
}
