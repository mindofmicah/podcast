<?php
namespace App\Http\Controllers\AudioFiles;

use App\Domain\AudioStorage\Actions\ServeAudioFileAction;
use App\Feed;
use App\Http\Requests\AudioStorage\GetAudioFileRequest;
use Symfony\Component\HttpFoundation\Response;

class ShowAudioFilesController
{
    // TODO $feed is required because otherwise the middleware won't convert the path to an object
    public function __invoke(Feed $feed, GetAudioFileRequest $request, ServeAudioFileAction $action): Response
    {
        return $action->execute($request->getEpisodePath());
    }
}