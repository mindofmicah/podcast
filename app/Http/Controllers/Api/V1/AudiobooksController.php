<?php
namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\DomCrawler\Crawler;

class AudiobooksController extends Controller
{
    public function index(Request $request)
    {
        $input = $request->validate([
            'title'  => 'required',
            'author' => 'required',
        ]);

        $input['title'] = urlencode($input['title']);
        $key = 'Jczsx4WfwWVZa52D6eEpQw';

        $goodreads_url ="https://www.goodreads.com/search/index.xml?key={$key}&q={$input['title']}";

        $crawler = new Crawler(file_get_contents($goodreads_url));
        $results = $crawler->filter('work > best_book')->each(function ($a) {
            return [
                'id'     => $a->filter('id')->text(),
                'title'  => $a->filter('title')->text(),
                'author' => $a->filter('author name')->text(),
                'image'  => $a->filter('small_image_url')->text(),
            ];
        });

        $results = collect($results)
            ->partition(function ($a) use ($input){
                return strtolower($a['author']) === strtolower($input['author']);
            })
            ->collapse();
    
        return response()->json([
            'data' => $results
        ]);
    }
}
