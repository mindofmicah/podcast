<?php
namespace App\Http\Controllers\Api\V1;

use App\Domain\Common\Models\ReleaseScheduleDecorator;
use App\Http\Controllers\Controller;

class AvailableReleasersController extends Controller
{
    public function index()
    {
        $decorators = ReleaseScheduleDecorator::with('feeds')
            ->get()
            ->sortBy('label')
            ->sortByDesc(function (ReleaseScheduleDecorator $decorator) {
                return $decorator->feeds->count();
            })
            ->map(function (ReleaseScheduleDecorator $decorator) {
                return [
                    'id'=>$decorator->id,
                    'label'=>$decorator->label,
                    'path'=>($decorator->class_name)::getRoute(),
                    'options'=>($decorator->class_name)::getAvailableFields(),
                ];
            })
            ->values();

        return response()->json([
            'data'=>$decorators
        ]);
    }
}
