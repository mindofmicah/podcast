<?php
namespace App\Http\Requests;

use App\Domain\Common\Models\ReleaseScheduleDecorator;
use App\Feed;
use App\FeedSources\Contracts\Fetchable;
use App\Releasers\EpisodeLimitDecorator;
use Illuminate\Foundation\Http\FormRequest;

abstract class FeedCreationRequest extends FormRequest
{
    protected $rules = [
        'title'              => 'required',
        'episode_limit'      => 'nullable|integer',
        'filters'            => 'nullable|array',
        'filters.*.field'    => 'nullable',
        'filters.*.operator' => 'nullable',
        'filters.*.value'    => 'nullable',
    ];
    /**
     * @var mixed
     */
    private $episode_decorator;
    /**
     * @var array|mixed
     */
    private $episode_decorator_options = [];

    abstract public function feedDetails(): Fetchable;

    abstract protected function additionalRules(): array;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge($this->rules, $this->additionalRules());
    }

    public function title(): string
    {
        return $this->get('title');
    }

    public function slug(): string
    {
        return str_slug($this->get('title'));
    }

    public function filters(): array
    {
        return array_map(function ($filter) {
            $filter['pattern'] = $filter['value'] ?? '.';
            return $filter;
        }, $this->get('filters', []));
    }

    /**
     * @return ReleaseScheduleDecorator|null
     */
    public function defaultReleaser(): ?ReleaseScheduleDecorator
    {
        if ($episode_limit = $this->get('episode_limit', 0)) {
            $this->episode_decorator_options = ['limit' => $episode_limit];
            return $this->episode_decorator = ReleaseScheduleDecorator::where('class_name', EpisodeLimitDecorator::class)->first();
        }
        return null;
    }

    /**
     * @return array
     */
    public function defaultReleaserOptions(): array
    {
        if (is_null($this->episode_decorator)) {
            $this->defaultReleaser();
        }
        return $this->episode_decorator_options;
    }

    public function onCreate(Feed $feed)
    {
        return;
    }
}
