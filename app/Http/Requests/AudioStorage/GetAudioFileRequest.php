<?php
namespace App\Http\Requests\AudioStorage;

use App\Domain\Common\Models\Episode;
use App\Domain\Common\Models\PodcastFeed;
use Illuminate\Foundation\Http\FormRequest;

class GetAudioFileRequest extends FormRequest
{
    public function getEpisodePath(): string
    {
        /** @var PodcastFeed $feed */
        $feed = $this->route('feed');

        return sprintf(
            "%s/%s",
            $feed->path,
            Episode::query()->findByFilename($this->route('filename'), $feed)->filename
        );
    }
    // TODO pull up to a super class?
    public function rules()
    {
        return [];
    }
}
