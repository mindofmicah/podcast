<?php
namespace App\Http\Requests;

use App\Domain\Common\Models\PodcastFeed;
use App\DropboxFeed;
use App\FeedSources\Contracts\Fetchable;
use Illuminate\Validation\Rule;

class DropboxFeedCreationRequest extends FeedCreationRequest
{
    public function feedDetails(): Fetchable
    {
        $dropbox_feed = new DropboxFeed;
        $dropbox_feed->path = $this->path;
        return $dropbox_feed;
    }

    protected function additionalRules(): array
    {
        return [
            'path' => ['required', Rule::unique(PodcastFeed::class)],
        ];
    }
}
