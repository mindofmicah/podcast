<?php
namespace App\Http\Requests;

use App\Domain\Common\Models\YouTubeFeed;
use App\FeedSources\Contracts\Fetchable;

class YouTubeFeedCreationRequest extends FeedCreationRequest
{
    public function feedDetails(): Fetchable
    {
        $youtube_feed = new YouTubeFeed;
        $youtube_feed->path = $this->path;
        return $youtube_feed;
    }

    protected function additionalRules(): array
    {
        return [
            'path' => 'required',
        ];
    }
}
