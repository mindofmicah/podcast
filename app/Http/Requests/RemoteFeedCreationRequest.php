<?php
namespace App\Http\Requests;

use App\FeedSources\Contracts\Fetchable;
use App\RemoteFeed;

class RemoteFeedCreationRequest extends FeedCreationRequest
{
    public function feedDetails(): Fetchable
    {
        $remote_feed = new RemoteFeed;
        $remote_feed->path = $this->path;
        return $remote_feed;
    }

    protected function additionalRules(): array
    {
        return [
            'path'        => 'required|url',
            'start_after' => 'nullable|url',
        ];
    }
}
