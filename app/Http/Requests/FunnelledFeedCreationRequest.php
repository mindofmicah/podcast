<?php
namespace App\Http\Requests;

use App\Feed;
use App\FeedSources\Contracts\Fetchable;
use App\FunnelledFeed;

class FunnelledFeedCreationRequest extends FeedCreationRequest
{
    public function feedDetails(): Fetchable
    {
        $funnel_feed = new FunnelledFeed;

        return $funnel_feed;
    }

    public function onCreate(Feed $feed)
    {
        $feed->details->feeds()->attach($this->feeds);
    }

    protected function additionalRules(): array
    {
        return [
            'feeds' => 'required|array',
        ];
    }
}
