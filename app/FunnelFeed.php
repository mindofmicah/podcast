<?php
namespace App;

use Illuminate\Support\Collection;

/**
 * @property-read FunnelledFeed $details
 */
class FunnelFeed extends Feed
{
    protected function releasableEpisodes(): Collection
    {
        $all_eps = $this->details->feeds->Map(function (Feed $feed) {
            return $feed->getReleasableEpisodes();
        });

        $return = $all_eps->take(0);

        /** @var Collection $all_eps */
        while (count($all_eps = $all_eps->filter->count())) {
            $return->push($all_eps->random()->shift());
        }
        return $return;
    }
}
