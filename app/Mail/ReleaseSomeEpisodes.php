<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReleaseSomeEpisodes extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $feeds = \App\Feed::doesntHave('releasables')->where('is_active', true)->has('unplayedEpisodes')->get();

        return $this->markdown('emails.feeds.release-something', [
        'feeds'=>$feeds
        ]);
    }
}
