<?php

namespace App\Mail;

use App\Episode;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OvercastEpisodeMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $episode;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Episode $episode)
    {
        $this->episode = $episode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Episode discovered on Overcast')
            ->markdown('mail.overcast-episode');
    }
}
