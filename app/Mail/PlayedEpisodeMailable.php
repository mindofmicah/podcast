<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PlayedEpisodeMailable extends Mailable
{
    use Queueable, SerializesModels;

    /** @var array */
    public $feeds;

    /**
     * Create a new message instance.
     *
     * @param array $feeds
     */
    public function __construct(array $feeds)
    {
        $this->feeds = $feeds;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Episodes marked as played')
            ->markdown('mail.played-episode');
    }
}
