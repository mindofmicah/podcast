<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Exception;

/**
 * App\Filter
 *
 * @property int $id
 * @property int $feed_id
 * @property string $pattern
 * @property bool $case_sensitive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $value
 * @property string $field
 * @property string $operator
 * @method static \Illuminate\Database\Eloquent\Builder|Filter newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Filter newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Filter query()
 * @mixin \Eloquent
 */
class Filter extends Model
{
    protected $guarded = ['id'];

    protected $attributes = [
        'case_sensitive' => false,
    ];

    protected $casts = [
        'case_sensitive' => 'boolean',
    ];

    public function matches(array $episode)
    {
        $field_value = array_get($episode, $this->field);

        if ($this->operator === 'matches') {
            $pattern = "/{$this->attributes['value']}/";
            if (!$this->attributes['case_sensitive']) {
                $pattern.= 'i';
            }
            return preg_match($pattern, $field_value);
        } else {
            if ($this->operator === 'exists') {
                return !is_null($field_value);
            }
            throw new Exception('invalid filter matching');
        }
    }
}
