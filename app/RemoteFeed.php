<?php
namespace App;

use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\Contracts\Fetchable;
use App\FeedSources\Remote\ArchiveOrgFetcher;
use App\FeedSources\Remote\LegacyRSSFeedFetcher;
use App\FeedSources\Remote\RSSFeedFetcher;
use App\FeedSources\Remote\LoyalBooksFetcher;
use Illuminate\Database\Eloquent\Model;

/**
 * App\RemoteFeed
 *
 * @property int $id
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Feed|null $feed
 * @method static \Illuminate\Database\Eloquent\Builder|RemoteFeed newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RemoteFeed newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RemoteFeed query()
 * @mixin \Eloquent
 */
class RemoteFeed extends Model implements Fetchable
{
    public function feed()
    {
        return $this->morphOne(Feed::class, 'feedable', 'feedable_type', 'feedable_id');
    }

    public function fetcher(): InformationFetchable
    {
        $url_information = parse_url($this->attributes['path']);

        if (strtolower($url_information['host']) == 'archive.org') {
            return new ArchiveOrgFetcher($this->attributes['path']);
        }
        if (strtolower($url_information['host']) == 'www.loyalbooks.com') {
            return new LoyalBooksFetcher($this->attributes['path']);
        }

        if ($this->feed->id > 160 && $this->feed->id != 217) {
            return new RSSFeedFetcher($this->attributes['path']);
        }

        return new LegacyRSSFeedFetcher($this->attributes['path']);
    }

}
