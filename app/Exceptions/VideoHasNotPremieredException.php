<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Carbon;
use Throwable;

class VideoHasNotPremieredException extends Exception
{
    /** @var Carbon */
    private $premiere;

    public function __construct(Carbon $premiere_date, Throwable $previous = null)
    {
        $this->premiere = $premiere_date;
        parent::__construct("Will premiere {$premiere_date->toDateTimeLocalString()}",0, $previous);
    }

    public function getPremiere()
    {
        return $this->premiere;
    }
}