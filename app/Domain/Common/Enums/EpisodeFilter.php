<?php
namespace App\Domain\Common\Enums;

class EpisodeFilter
{
    const ALL = 0;
    const UNPLAYED = 2;
    const PLAYED = 1;
    const RELEASED = 4;
    const UNRELEASED = 8;
    const OUTSTANDING = self::RELEASED | self::UNPLAYED;
}