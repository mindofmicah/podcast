<?php
namespace App\Domain\Common\QueryBuilders;

use App\Domain\Common\Enums\EpisodeFilter;
use App\Domain\Common\EpisodeQueryBuilder;
use App\Domain\Common\Models\YouTubeFeed;
use App\FunnelMapper;
use App\YouTubeFeed as LegacyYoutubeFeed;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class FeedQueryBuilder extends Builder
{
    public function active(): self
    {
        return $this->where('is_active', true);
    }

    public function whereEpisodes(int $filter = EpisodeFilter::ALL, bool $eager_load = true): self
    {
        return $this->whereHas('episodes', function (EpisodeQueryBuilder $query) use ($filter) {
            return $query->filter($filter);
        })->when($eager_load, function () use ($filter) {
            return $this->withEpisodes($filter);
        });
    }

    public function withEpisodes(int $filter = EpisodeFilter::ALL): self
    {
        return $this->with(['episodes' => function (BelongsToMany $query) use ($filter) {
            /** @var EpisodeQueryBuilder $query */
            return $query->filter($filter);
        }]);
    }

    public function withInactive(): self
    {
        return $this->withoutGlobalScope('active');
    }

    public function whereInactive(): self
    {
        return $this->withInactive()->where('is_active', 0);
    }

    /**
     * Limit a query to only look for feeds that should be scanned for new episodes
     *
     * @param Carbon $day
     *
     * @return FeedQueryBuilder
     */
    public function shouldScanForDay(Carbon $day): self
    {
        return $this->where('scan_schedule', '&', pow(2, $day->dayOfWeek));
    }

    /**
     * Often we want to exclude feeds that are only around because they are used in funnel feeds
     *
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function ignoreFunnelledFeeds(): self
    {
        return $this->whereNotIn('id', array_keys((new FunnelMapper())->buildMapping()));
    }

    public function ofType(string $feed_type): self
    {
        if (in_array($feed_type, [YouTubeFeed::class, LegacyYoutubeFeed::class])) {
            $feed_type = [YouTubeFeed::class, LegacyYoutubeFeed::class];
        } else {
            $feed_type = [$feed_type];
        }

        return $this->whereIn('feedable_type', $feed_type);
    }
}