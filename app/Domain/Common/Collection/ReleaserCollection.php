<?php
namespace App\Domain\Common\Collection;

use App\Domain\Common\Models\PodcastFeed;
use App\Domain\Common\Models\ReleaseScheduleDecorator;
use App\Releasers\BasicReleaser;
use App\Releasers\EmptyReleaser;
use App\Releasers\Releasable;
use Illuminate\Database\Eloquent\Collection;

class ReleaserCollection extends Collection
{
    public function toReleaseSchedule(PodcastFeed $feed): Releasable
    {
        if ($this->isEmpty()) {
            return new EmptyReleaser;
        }

        return  $this->reduce(function (Releasable $carry, ReleaseScheduleDecorator $decorator) use ($feed) {
            return (new $decorator->class_name($carry, $decorator->pivot->options + ['feed' => $feed]));
        }, new BasicReleaser);
    }
}