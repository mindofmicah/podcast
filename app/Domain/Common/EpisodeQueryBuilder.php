<?php
namespace App\Domain\Common;

use App\Domain\Common\Enums\EpisodeFilter;
use App\Domain\Common\Models\Episode;
use App\Domain\Common\Models\PodcastFeed;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Carbon;
use RuntimeException;

/**
 * @uses self::wherePlayed()
 * @uses self::whereUnplayed()
 * @uses self::whereReleased()
 * @uses self::whereUnreleased()
 */
class EpisodeQueryBuilder extends Builder
{
    private $filter_handlers = [
        EpisodeFilter::UNPLAYED   => 'unplayed',
        EpisodeFilter::PLAYED     => 'played',
        EpisodeFilter::RELEASED   => 'released',
        EpisodeFilter::UNRELEASED => 'unreleased',
    ];

    public function filter(int $filters = EpisodeFilter::ALL)
    {
        if ($filters === EpisodeFilter::ALL) {
            return $this;
        }

        foreach ($this->filter_handlers as $filter => $handler) {
            if ($filters & $filter) {
                call_user_func([$this, 'where' . $handler]);
            }
        }
        return $this;
    }

    public function wherePlayed(): EpisodeQueryBuilder
    {
        return $this->where('is_played', 1);
    }

    public function whereUnplayed(): EpisodeQueryBuilder
    {
        return $this->where('is_played', 0);
    }

    public function whereReleased($adjustment = 0): EpisodeQueryBuilder
    {
        return $this->whereNotNull('released_at')->when($adjustment, function () use ($adjustment) {
            if (is_string($adjustment) && $adjustment[0] === '-') {
                $operator = '<=';
            } else {
                throw new RuntimeException('not yet implemented');
            }
            return $this->where('released_at', $operator, Carbon::parse($adjustment));
        })->where('released_at', '<=', now());
    }

    public function whereUnreleased(): EpisodeQueryBuilder
    {
        return $this->whereNull('released_at');
    }

    /**
     * @param string $filename
     * @param PodcastFeed|null $feed
     *
     * @return Episode
     * @throws ModelNotFoundException
     */
    public function findByFilename(string $filename, ?PodcastFeed $feed = null): Episode
    {
        return $this->where('url', 'LIKE', "%$filename")
            ->when(
                $feed,
                function (self $query, PodcastFeed $feed) {
                    // TODO check Laravel docs for cleaner approach
                    return $query->where('feed_id', $feed->id);
                }
            )
            ->firstOrFail();
    }

}