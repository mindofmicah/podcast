<?php
namespace App\Domain\Common\Models;

use App\Domain\Common\Collections\EpisodeCollection;
use App\Domain\Common\EpisodeQueryBuilder;
use App\Feed;
use App\NamingPattern;
use App\OvercastIds;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static EpisodeQueryBuilder|Builder query()
 */
class Episode extends BaseModel
{
    use OvercastIds;

    /** @var class-string */
    protected $queryBuilderClass = EpisodeQueryBuilder::class;
    /** @var class-string */
    protected $collectionClass = EpisodeCollection::class;

    /** @var string[] */
    protected $casts = [
        'is_played' => 'bool',
        'options'   => 'json',
    ];

    /** @var string[] */
    protected $touches = [
        'feed',
    ];
    /** @var string[] */
    protected $dates = [
        'created_at',
        'updated_at',
        'released_at',
    ];

    public function feed(): BelongsTo
    {
        return $this->belongsTo(Feed::class);
    }

    public function markAsPlayed(): self
    {
        return $this->setAttribute('is_played', true);
    }

    public function markAsReleased(?Carbon $released_at = null): self
    {
        return $this->setAttribute('released_at', $released_at ?? now());
    }

    public function getUrlAttribute()
    {
        if (strpos($this->attributes['url'], 'whamdonk') !== false) {
            return str_replace('http://', 'https://', $this->attributes['url']);
        }
        return $this->attributes['url'];
    }

    public function getImagePathAttribute()
    {
        return url('/episodes/' . $this->id . '/artwork');
    }

    public function getDescriptionAttribute()
    {
        $description_chunks = [];
        $description_chunks[] = $this->options['description'] ?? null;

        if (($this->feed->details instanceof \App\RemoteFeed) === false) {
            $description_chunks[] = "Episode {$this->attributes['order']} of {$this->feed->episodes->count()}";
        }
        $description_chunks[] = '<a href="' . url('/feeds/' . $this->feed->path . '/episodes') . '" rel="nofollow">Manage Episodes</a>';

        return implode('<br/><br/>', array_filter($description_chunks));
    }

    static protected function boot()
    {
        parent::boot();

        self::creating(function (self $episode) {
            if (is_null($episode->order)) {
                $episode->order = 1 + self::where('feed_id', $episode->feed_id)->max('order');
            }
        });
    }

    public function matchesOvercastTitle(string $overcast_title)
    {
        return ends_with(
            preg_replace('/\W/', '', $this->title),
            preg_replace('/\W/', '', $overcast_title)
        );
    }

    public function renameByPattern(NamingPattern $pattern)
    {
        $options = $this->options;
        $old_name = $options['title'];
        $options['title'] = $pattern->rename($this);
        $this->options = $options;
        $this->save();
    }

    /**
     * @deprecated
     */
    public function scopeUnplayed(EpisodeQueryBuilder $query)
    {
        return $query->whereUnplayed();
    }
    /**
     * @deprecated
     */
    public function scopeReleased(EpisodeQueryBuilder $query): EpisodeQueryBuilder
    {
        return $query->whereReleased();
    }

    public function parent_feeds()
    {
        return $this->belongsToMany(PodcastFeed::class, 'episode_feed', 'episode_id', 'feed_id');
    }

    public function getMorphClass()
    {
        return 'episode';
    }
}