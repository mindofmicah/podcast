<?php
namespace App\Domain\Common\Models;

use App\Domain\Common\Collection\ReleaserCollection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\ReleaseScheduleDecorator
 *
 * @property int $id
 * @property string $label
 * @property string $class_name
 * @property array $options
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Feed[] $feeds
 * @property-read int|null $feeds_count
 * @method static \Illuminate\Database\Eloquent\Builder|ReleaseScheduleDecorator newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReleaseScheduleDecorator newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ReleaseScheduleDecorator query()
 * @mixin \Eloquent
 */
class ReleaseScheduleDecorator extends BaseModel
{
    protected $collectionClass = ReleaserCollection::class;
    protected $casts = [
        'options'=>'array',
    ];

    public function feeds(): BelongsToMany
    {
        return $this->belongsToMany(PodcastFeed::class, 'feed_release_schedule_decorators', 'release_schedule_decorator_id', 'feed_id')
            ->using(FeedReleaserPivot::class)
            ->withPivot(['options'])
            ->withTimestamps();        
    }
}
