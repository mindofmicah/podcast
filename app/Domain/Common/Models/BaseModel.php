<?php
namespace App\Domain\Common\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    /** @var class-string */
    protected $queryBuilderClass = Builder::class;

    /** @var class-string */
    protected $collectionClass = Collection::class;

    /** @var string[] */
    protected $guarded = [];

    public function newEloquentBuilder($query): Builder
    {
        return new $this->queryBuilderClass($query);
    }

    /**
     * @param array $models
     *
     * @return Collection
     */
    public function newCollection(array $models = []): Collection
    {
        return new $this->collectionClass($models);
    }
}