<?php
namespace App\Domain\Common\Models;

use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\Contracts\Fetchable;
use App\FeedSources\YouTube\Fetcher;

/**
 * App\YouTubeFeed
 *
 * @property int $id
 * @property string $path
 * @property string|null $current_page
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Feed|null $feed
 * @method static \Illuminate\Database\Eloquent\Builder|YouTubeFeed newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|YouTubeFeed newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|YouTubeFeed query()
 * @mixin \Eloquent
 */
class YouTubeFeed extends BaseModel implements Fetchable
{
    protected $table = 'youtube_feeds';

    public function feed()
    {
        return $this->morphOne(PodcastFeed::class, 'feedable', 'feedable_type', 'feedable_id');
    }

    public function fetcher(): InformationFetchable
    {
        return new Fetcher;
    }
}
