<?php
namespace App\Domain\Common\Models;

use App\Domain\Common\Collection\ReleaserCollection;
use App\Domain\Common\Collections\EpisodeCollection;
use App\Domain\Common\QueryBuilders\FeedQueryBuilder;
use App\Feed;
use App\FeedSources\Contracts\Fetchable;
use App\OvercastIds;
use App\Releasers\Releasable;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\Self_;

/**
 * @method static FeedQueryBuilder|Builder query()
 * @property Fetchable $details
 * @property-read Releasable $release_schedule
 * @property-read ReleaserCollection $releasables
 * @property-read \App\Domain\Common\Models\Episode[] $episodes
 */
class PodcastFeed extends BaseModel
{
    use SoftDeletes;
    use OvercastIds;

    /** @var string */
    protected $queryBuilderClass = FeedQueryBuilder::class;

    /** @var string */
    protected $table = 'feeds';

    /** @var string[] */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'released_at',
    ];
    /** @var string[] */
    protected $appends = [
        'image_path',
    ];

    /** @var string[] */
    protected $touches = [
        'funnelConfigurations',
    ];

    /** @var string[] */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function getDecoratedTitleAttribute()
    {
        return 'Magic Playlist - ' . $this->attributes['title'];
    }

    public function getImagePathAttribute(): string
    {
        if (!$this->coverart) {
            return 'https://www.gravatar.com/avatar/' . md5($this->attributes['path']) . '?d=robohash&amp;f=y';
        }

        if ($this->coverart[0] == 'h') {
            return $this->coverart;
        }

        return url('/feeds/' . (optional($this->details)->path ? : $this->attributes['path']) . '/coverart');
    }

    public function episodes()
    {
        return $this->belongsToMany(Episode::class, 'episode_feed', 'feed_id');
    }

    public function direct_episodes()
    {
        return $this->hasMany(Episode::class, 'feed_id');
    }

    /**
     * @return EpisodeCollection|Episode[]
     */
    public function getReleasableEpisodes(): EpisodeCollection
    {
        return $this->episodes()->get()->take(3);
        return $this->releasables->toReleaseSchedule($this)
            ->filterReleasableEpisodes($this->episodes()->whereUnplayed()->whereUnreleased()->get());
    }

    public function resolveRouteBinding($value, $field = null)
    {
        if (in_array($value, \Cache::get('inactive-feeds', []))) {
            return abort(410);
        }

        $feed = self::where('path', $value)
            ->withTrashed()
            ->withInactive()
            ->first();

        if (!$feed) {
            abort(404);
        }
        if ($feed->deleted_at) {
            abort(410);
        }

        return $feed->load('details');
    }

    protected function releasableEpisodes()
    {
        return $this->episodes()->whereNull('released_at')->orderBy('order')->get();
    }

    public function releasables()
    {
        return $this->belongsToMany(
            ReleaseScheduleDecorator::class,
            'feed_release_schedule_decorators',
            'feed_id',
            'release_schedule_decorator_id'
        )
            ->using(FeedReleaserPivot::class)
            ->withPivot(['options'])
            ->withTimestamps();
    }

    public function markAsDeactivated(): self
    {
        return $this->setAttribute('is_active', false);
    }

    public function getMorphClass()
    {
        return 'feed';
    }

    /**
     * Provide access to the details of the feed
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function details()
    {
        return $this->morphTo('details', 'feedable_type', 'feedable_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function (FeedQueryBuilder $builder) {
            return $builder->active();
        });
    }
}