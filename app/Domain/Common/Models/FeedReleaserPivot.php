<?php
namespace App\Domain\Common\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * App\FeedReleaserPivot
 *
 * @method static \Illuminate\Database\Eloquent\Builder|FeedReleaserPivot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FeedReleaserPivot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FeedReleaserPivot query()
 * @mixin \Eloquent
 */
class FeedReleaserPivot extends Pivot
{
    protected $casts =[
        'options'=>'json',
    ];
}
