<?php

namespace App\Domain\Common\ViewModels;

use Illuminate\Contracts\Support\Arrayable;

class ViewModel implements Arrayable
{

    public function toArray()
    {
        $data = [];

        foreach (get_object_vars($this) as $property => $value) {
            $data[$property] = $value instanceof Arrayable ? $value->toArray() : $value;
        }

        foreach (array_diff(get_class_methods($this), ['__construct', 'toArray']) as $method) {
            $data[$method] = $this->$method();
        }

        return $data;
    }
}