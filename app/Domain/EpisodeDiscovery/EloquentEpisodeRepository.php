<?php
namespace App\Domain\EpisodeDiscovery;

use App\Domain\Common\Models\Episode;
use App\Feed;
use Illuminate\Database\Eloquent\Collection;

class EloquentEpisodeRepository implements EpisodeRepository
{
    /**
     * @inheritDoc
     */
    public function createMany(array $episodes, Feed $feed): Collection
    {
        return $feed->direct_episodes()
            ->createMany($this->addNonConflictingOrdersToEpisodes($feed, $episodes));
    }

    /**
     * @inheritDoc
     */
    public function getNextAvailableOrder(Feed $feed): int
    {
        return 1 + $feed->episodes()->max('order');
    }

    /**
     * Ignore whatever 'orders' are in the new data. Sometimes the feedsources get it twisted
     *
     * @param Feed $feed
     * @param array[] $episodes
     *
     * @return array
     */
    protected function addNonConflictingOrdersToEpisodes(Feed $feed, array $episodes): array
    {
        $starting_order = $this->getNextAvailableOrder($feed);
        foreach ($episodes as $index => &$episode) {
            $episode['order'] = $starting_order + $index;
        }
        return $episodes;
    }
}
