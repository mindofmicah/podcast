<?php
namespace App\Domain\EpisodeDiscovery\Contracts;

use App\Feed;
use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\FetchedInformation;
use Illuminate\Support\Collection;

abstract class FeedInformationFetcher implements InformationFetchable
{
    protected $jobs_to_dispatch = [];

    abstract protected function getNewEpisodes(Feed $feed): Collection;

    abstract protected function getCoverart(Feed $feed): string;

    public function fetchInformationForFeed(Feed $feed): FetchedInformation
    {
        $this->before();

        $episodes = $this->getNewEpisodes($feed);
        $coverart = !$feed->coverart ? $this->getCoverart($feed) : '';

        $this->after();

        return new FetchedInformation($episodes, $coverart);
    }

    protected function before()
    {
    }

    protected function after()
    {
        foreach ($this->jobs_to_dispatch as $job) {
            dispatch($job);
        }
    }
}
