<?php
namespace App\Domain\EpisodeDiscovery\UseCases;

use App\Domain\EpisodeDiscovery\DiscoverNewEpisodesRequest;
use App\Domain\EpisodeDiscovery\EpisodeRepository;
use App\Domain\EpisodeDiscovery\FeedSourceFactory;
use App\Feed;
use App\FeedSources\FetchedInformation;
use App\FeedSources\Remote\RSSFeedFetcher;
use Log;

class DiscoverNewEpisodes
{
    /** @var EpisodeRepository */
    private $episodes;
    /** @var FeedSourceFactory */
    private $feed_source;

    /**
     * DiscoverNewEpisodes constructor.
     *
     * @param EpisodeRepository $episodes
     * @param FeedSourceFactory $feed_source
     */
    public function __construct(EpisodeRepository $episodes, FeedSourceFactory $feed_source)
    {
        $this->episodes = $episodes;
        $this->feed_source = $feed_source;
    }

    public function handle(DiscoverNewEpisodesRequest $request): FetchedInformation
    {
        $feed = $request->getFeed();
        $flags = $request->getFlags();

        $fetched_information = $this->scanFeedSource($feed, function ($fetcher) use ($flags) {
            if (method_exists($fetcher, 'handleFlags')) {
                $fetcher->handleFlags($flags);
            }

            if ($fetcher instanceof RSSFeedFetcher) {
                $fetcher->stopOnExistingEpisodes();
            }
        });

        if ($new_episodes = $fetched_information->newEpisodes()->all()) {
            $created_episodes = $this->episodes->createMany($new_episodes, $feed);

            Log::info('Episodes created for feed', [
                'feed'     => $feed->title,
                'episodes' => $created_episodes->pluck('title')->all(),
            ]);
            $feed->touch();
        }

        if ($new_coverart = $fetched_information->coverart()) {
            $feed->update([
                'coverart' => $new_coverart,
            ]);
        }
        return $fetched_information;
    }

    private function scanFeedSource(Feed $feed, callable $modify_fetcher = null): FetchedInformation
    {
        $fetcher = $this->feed_source->createFromFeed($feed);


        if ($modify_fetcher && ($modify_fetcher($fetcher) === false)) {
            return new FetchedInformation(collect());
        }

        return $fetcher->fetchInformationForFeed($feed)
            ->filterEpisodes($feed->filters);
    }
}