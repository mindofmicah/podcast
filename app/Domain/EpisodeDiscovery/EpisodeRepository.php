<?php
namespace App\Domain\EpisodeDiscovery;

use App\Episode;
use App\Feed;
use Illuminate\Database\Eloquent\Collection;

interface EpisodeRepository
{
    /**
     * @param array[] $episodes
     * @param Feed $feed
     *
     * @return Episode[]|Collection
     */
    public function createMany(array $episodes, Feed $feed): Collection;

    /**
     * @param Feed $feed
     *
     * @return int
     */
    public function getNextAvailableOrder(Feed $feed): int;
}