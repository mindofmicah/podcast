<?php
namespace App\Domain\EpisodeDiscovery;

use Alaouy\Youtube\Youtube;
use App\Domain\Common\Models\PodcastFeed;
use App\Domain\Common\Models\YouTubeFeed;
use App\DropboxFeed;
use App\FeedSources\Contracts\InformationFetchable;
use App\FeedSources\Dropbox\Fetcher as DropboxFetcher;
use App\FeedSources\YouTube\Fetcher;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\MountManager;

class FeedSourceFactory
{
    /** @var Youtube */
    private $youtube_api;

    public function __construct(Youtube $youtube_api)
    {
        $this->youtube_api = $youtube_api;
    }

    public function createFromFeed(PodcastFeed $feed): InformationFetchable
    {
        $details = $feed->details;
        if ($details instanceof YouTubeFeed) {
            return new Fetcher($this->youtube_api, $details->path, $details->current_page);
        }

        if ($details instanceof DropboxFeed) {
            return new DropboxFetcher(
                $details->path,
                Storage::disk('dropbox'),
                new MountManager([
                    'dropbox' => Storage::disk('dropbox')->getDriver(),
                    'local'   => Storage::disk('local')->getDriver(),
                ])

            );
        }

        return $feed->details->fetcher();
    }
}