<?php
namespace App\Domain\EpisodeDiscovery;

use App\Feed;

class DiscoverNewEpisodesRequest
{
    /** @var Feed */
    private $feed;
    /** @var int */
    private $flags;

    /**
     * DiscoverNewEpisodesRequest constructor.
     *
     * @param Feed $feed
     * @param int $flags
     */
    public function __construct(Feed $feed, int $flags = 0)
    {
        $this->flags = $flags;
        $this->feed = $feed;
    }

    public function getFeed(): Feed
    {
        return $this->feed;
    }

    public function getFlags(): int
    {
        return $this->flags;
    }
}