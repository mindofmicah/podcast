<?php
namespace App\Domain\EpisodeReleasing\Actions;

use App\Domain\Common\Models\Episode;
use App\Events\EpisodePlayed;
use Illuminate\Events\Dispatcher;

class MarkEpisodeAsPlayedAction
{
    /** @var Dispatcher */
    private $dispatcher;

    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function execute(Episode $episode): void
    {
        $episode->markAsPlayed()->save();
        $this->dispatcher->dispatch(new EpisodePlayed($episode));
    }
}