<?php
namespace App\Domain\EpisodeReleasing\Actions;

use App\Domain\Common\Models\Episode;
use App\Events\EpisodeReleased;
use Carbon\Carbon;
use Illuminate\Contracts\Events\Dispatcher;

class ReleaseEpisodeAction
{
    /** @var Dispatcher */
    private $events;

    public function __construct(Dispatcher $events)
    {
        $this->events = $events;
    }

    public function execute(Episode $episode, int $delay = 0): void
    {
        // No sense releasing if it is already released
        if ($episode->released_at) {
            return;
        }

        $episode->markAsReleased(Carbon::now()->addMinutes($delay))->save();

        $this->events->dispatch(new EpisodeReleased($episode));
    }
}