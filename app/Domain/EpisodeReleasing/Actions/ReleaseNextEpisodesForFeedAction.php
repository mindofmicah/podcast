<?php
namespace App\Domain\EpisodeReleasing\Actions;

use App\Domain\Common\Collections\EpisodeCollection;
use App\Domain\Common\Models\Episode;
use App\Domain\Common\Models\PodcastFeed;

class ReleaseNextEpisodesForFeedAction
{
    /** @var ReleaseEpisodeAction */
    private $episode_action;

    public function __construct(ReleaseEpisodeAction $episode_action)
    {
        $this->episode_action = $episode_action;
    }

    public function execute(PodcastFeed $feed, int $minutes_to_delay = 0): EpisodeCollection
    {
        return $feed->getReleasableEpisodes()
            ->each(function (Episode $episode, int $index) use ($minutes_to_delay): void {
                $this->episode_action->execute($episode, $minutes_to_delay * ($index + 1));
            });
    }
}