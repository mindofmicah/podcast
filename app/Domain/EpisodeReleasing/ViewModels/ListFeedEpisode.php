<?php
namespace App\Domain\EpisodeReleasing\ViewModels;

use App\Domain\Common\Models\PodcastFeed;
use App\Domain\Common\ViewModels\ViewModel;
use App\FunnelFeed;
use Illuminate\Contracts\Routing\UrlGenerator;

class ListFeedEpisode extends ViewModel
{
    /** @var PodcastFeed */
    public $feed;
    /** @var UrlGenerator */
    private $url_generator;

    public function __construct(PodcastFeed $feed, UrlGenerator $url_generator)
    {
        $this->feed = $feed;
        $this->url_generator = $url_generator;
    }

    public function episodes()
    {
        return $this->feed->episodes()->orderBy('order')->get()->toArray();
    }

    public function paths()
    {
        $paths = [];
        $paths['feed']['edit'] = $this->url_generator->route('feeds.edit', ['feed' => $this->feed->path]);
        $paths['feed']['show'] = $this->url_generator->route('feeds.show', ['feed' => $this->feed->path]);
        return $paths;
    }

    public function funnel_feeds()
    {
        return $this->feed instanceof FunnelFeed ? $this->feed->details->feeds : [];
    }
}