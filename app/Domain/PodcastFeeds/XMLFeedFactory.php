<?php
namespace App\Domain\PodcastFeeds;

use App\Domain\Common\Models\PodcastFeed;
use App\Domain\Common\Models\Episode;
use App\FunnelFeed;
use Castanet_Feed;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class XMLFeedFactory
{
    /**
     * @param PodcastFeed $feed
     * @param bool $use_released_episodes
     *
     * @return Castanet_Feed
     */
    public function create(PodcastFeed $feed, bool $use_released_episodes = true): Castanet_Feed
    {
        $feed_obj = new XMLFeed(
            'Magic Playlist - ' . $feed->title,
            'https://podcasts.whamdonk.com/feeds/' . $feed->path . '/episodes',
            'Release Schedule: ' . $feed->releasables->toReleaseSchedule($feed)->description()
        );
        $feed_obj->setAtomLink('https://podcasts.whamdonk.com/feeds/' . $feed->path);
        $feed_obj->setImage(htmlspecialchars($feed->image_path), 1400, 1400);
        $feed_obj->setItunesExplicit(false);

        $feed_obj->setLastBuildDate($feed->updated_at);

        if ($feed instanceof FunnelFeed) {
            $description_prefixes = $feed->details->feeds->mapWithKeys(function (PodcastFeed $feed) {
                return [$feed->id => $feed->title . '<br>'];
            })->all();
        } else {
            $description_prefixes = [$feed->id => ''];
        }

        $feed->episodes()
            ->when($use_released_episodes, function (Builder $query) {
                return $query->orderBy('released_at', 'DESC')
                    ->whereNotNull('released_at');
            }, function (Builder $query) {
                return $query->orderBy('order')
                    ->whereNull('released_at');
            })
            ->get()
            ->tap(function (Collection $episodes) use ($feed_obj) {
                if ($episodes->isNotEmpty()) {
                    $feed_obj->setLastBuildDate($episodes->first()->released_at ?? $episodes->max('updated_at'));
                }
            })
            ->each(function (Episode $episode) use ($description_prefixes, $feed_obj, $feed) {
                $prefix = $description_prefixes[$episode->feed_id] ?? '';
                $episode->setRelation('feed', $feed);
                $item = new FeedItem();
                if ($episode->duration) {
                    $item->setMediaDuration($episode->duration);
                }

                $item->setMediaMimeType("audio/mpeg");
                $item->setDescription($prefix . $episode->description);
                $item->setTitle($episode->title);
                if ($artist = $episode->options['artist'] ?? false) {
                    $item->setItunesSubtitle($artist);
                }
                if ($episode->size) {
                    $item->setMediaSize($episode->size);
                }

                $item->setItunesImage($episode->image_path);
                $item->setMediaUrl($episode->url);
                $item->setPublishDate($episode->released_at);

                $feed_obj->addItem($item);
            });

        return $feed_obj;
    }
}
