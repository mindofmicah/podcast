<?php
namespace App\Domain\PodcastFeeds;

use Castanet_Item;
use DOMNode;

class FeedItem extends Castanet_Item
{
    /**
     * @param DOMNode $parent
     *
     * @return void
     * @todo see if this feature is still used
     */
    protected function buildItunesImage(DOMNode $parent)
    {
        if ($this->itunes_image_url == '') {
            return;
        }

        parent::buildItunesImage($parent);

        $document = $parent->ownerDocument;

        $image_node = $document->createElement('image');
        $parent->appendChild($image_node);

        $node = $document->createElement('url', $this->itunes_image_url);
        $image_node->appendChild($node);

        $title = $document->createTextNode($this->title);
        $node = $document->createElement('title');
        $node->appendChild($title);
        $image_node->appendChild($node);

        $width = $document->createTextNode(80);
        $node = $document->createElement('width');
        $node->appendChild($width);
        $image_node->appendChild($node);

        $height = $document->createTextNode(80);
        $node = $document->createElement('height');
        $node->appendChild($height);
        $image_node->appendChild($node);
    }
}
