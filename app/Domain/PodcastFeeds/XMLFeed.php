<?php
namespace App\Domain\PodcastFeeds;

use Castanet_Feed;
use Carbon\Carbon;
use DOMNode;

class XMLFeed extends Castanet_Feed
{
    public function build(DOMNode $parent)
    {
        parent::build($parent);

        $this->buildLastBuildDate($parent->firstChild);
    }

    public function setLastBuildDate(Carbon $carbon)
    {
        $this->last_build_date = $carbon->setTimezone('GMT');
    }

    protected function buildLastBuildDate(DOMNode $parent)
    {
        if ($this->last_build_date) {
            $document = $parent->ownerDocument;

            $text = $document->createTextNode($this->last_build_date->format('D, d M Y H:i:s \G\M\T'));
            $node = $document->createElement('lastBuildDate');

            $node->appendChild($text);
            $parent->appendChild($node);
        }
    }
}
