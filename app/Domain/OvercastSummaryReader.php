<?php
namespace App\Domain;

use Symfony\Component\DomCrawler\Crawler;

class OvercastSummaryReader
{
    /** @var array */
    private $included_feeds = [];
    /** @var bool */
    private $only_show_played_episodes;

    public function onlyIncludeFeeds(array $included_feeds): OvercastSummaryReader
    {
        $this->included_feeds = $included_feeds;
        return $this;
    }

    public function onlyIncludePlayedEpisodes(): OvercastSummaryReader
    {
        $this->only_show_played_episodes = true;
        return $this;
    }

    public function forEachPodcastFeed(Crawler $crawler, callable $callback)
    {
        $crawler->filter('[type="rss"]')
            ->each(function (Crawler $crawler) use ($callback) {
                $feed_title = $crawler->attr('title');
                if (!in_array($feed_title, $this->included_feeds)) {
                    return;
                }

                $episode_filter = '[type="podcast-episode"]';
                if ($this->only_show_played_episodes) {
                    $episode_filter .= '[played="1"]';
                }

                $episodes = [];
                $crawler->filter($episode_filter)->each(function (Crawler $c) use (&$episodes) {
                    $overcast_id = trim(parse_url($c->attr('overcastUrl'), PHP_URL_PATH), '/');
                    $episodes[$overcast_id] = $c;
                });

                $callback($feed_title, $episodes);
            });
    }
}