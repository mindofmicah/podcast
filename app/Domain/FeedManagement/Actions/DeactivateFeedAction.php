<?php
namespace App\Domain\FeedManagement\Actions;

use App\Domain\Common\Models\PodcastFeed;

class DeactivateFeedAction
{
    public function execute(PodcastFeed $feed)
    {
        $feed->markAsDeactivated()
            ->save();

        return $feed;
    }
}