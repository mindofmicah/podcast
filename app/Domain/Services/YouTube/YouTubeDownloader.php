<?php
namespace App\Domain\Services\YouTube;

use App\Exceptions\VideoHasNotPremieredException;
use App\Exceptions\VideoIsPrivateException;
use Exception;
use Symfony\Component\Process\Exception\ProcessFailedException;
use YoutubeDl\YoutubeDl;

class YouTubeDownloader extends YoutubeDl
{
    /**
     * @param string $url
     * @param array $options
     *
     * @return \YoutubeDl\Entity\Video
     */
    public function downloadAudioVersion(string $url, array $options = [])
    {
        $options = array_merge([
            'extract-audio' => true,
            'audio-format'  => 'mp3',
            'audio-quality' => 0,
        ], $options);

        return $this->downloadWithOptions($url, $options);
    }

    /**
     * @param string $url
     * @param array $options
     *
     * @return \YoutubeDl\Entity\Video
     * @throws Exception
     */
    public function downloadWithOptions(string $url, array $options)
    {
        $original_options = $this->options;
        $this->options = array_merge($this->options, $options);

        try {
            return $this->wrapInException(function () use ($url) {
                return $this->download($url);
            });
        } catch (Exception $e) {
            throw $e;
        } finally {
            $this->options = $original_options;
        }
    }

    /**
     * @param string $url
     *
     * @return int
     * @throws VideoHasNotPremieredException
     */
    public function getExpectedDuration(string $url): int
    {
        return $this->wrapInException(function () use ($url) {
            return (int)$this->getInfo($url)['duration'];
        });
    }

    /**
     * @param \Closure $callable
     *
     * @return mixed
     * @throws VideoHasNotPremieredException
     * @throws VideoIsPrivateException
     */
    private function wrapInException(\Closure $callable)
    {
        try {
            return $callable();
        } catch (ProcessFailedException $exception) {
            $error_message = $exception->getProcess()->getErrorOutput();

            if (preg_match('/^ERROR: Premieres in (\d+) (hours|days|minutes)$/', $error_message, $matches)) {
                $premiere_date = now()->addHours(3);
                if ($matches[2] === 'days') {
                    $premiere_date->addDays($matches[1]);
                } elseif ($matches[2] === 'hours') {
                    $premiere_date->addHours($matches[1]);
                } elseif ($matches[2] === 'minutes') {
                    $premiere_date->addMinutes($matches[1]);
                }
                throw new VideoHasNotPremieredException($premiere_date, $exception);
            }
            if (strpos($error_message, 'ERROR: Private video') === 0) {
                throw new VideoIsPrivateException();
            }
            throw $exception;
        }
    }

    public function enableDebugging()
    {
        $this->debug(function ($type, $message) {
            dump($message);
        });
    }

}