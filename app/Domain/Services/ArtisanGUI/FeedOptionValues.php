<?php
namespace App\Domain\Services\ArtisanGUI;

use App\Feed;
use MindOfMicah\ArtisanGUI\Contracts\CommandOptionable;

class FeedOptionValues implements CommandOptionable
{
    public function values(): array
    {
        return Feed::pluck('path')->sort()->all();
    }
}