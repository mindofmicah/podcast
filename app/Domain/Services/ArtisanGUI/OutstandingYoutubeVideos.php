<?php
namespace App\Domain\Services\ArtisanGUI;

use App\Domain\Common\Models\Episode;
use App\Domain\Common\Models\YouTubeFeed;
use App\Domain\Common\QueryBuilders\FeedQueryBuilder;
use MindOfMicah\ArtisanGUI\Contracts\CommandOptionable;

class OutstandingYoutubeVideos implements CommandOptionable
{
    public function values(): array
    {
        return Episode::query()
            ->with('feed')
            ->whereUnplayed()
            ->whereHas('feed', function (FeedQueryBuilder $query) {
                $query->ofType(YouTubeFeed::class);
            })
            ->get()
            ->mapWithKeys(function (Episode $episode) {
                return [$episode->id => "$episode->title ({$episode->feed->title})"];
            })->all();
    }
}