<?php
namespace App\Domain\AudioStorage\Actions;

use App\Domain\AudioStorage\ServeableFiles\ServeableFileFactory;
use Symfony\Component\HttpFoundation\Response;

class ServeAudioFileAction
{
    public function __construct(private ServeableFileFactory $serveable_factory) {}

    public function execute(string $filename): Response
    {
        return $this->serveable_factory->create($filename)->serve();
    }
}