<?php
namespace App\Domain\AudioStorage\ServeableFiles;

use Carbon\Carbon;
use Illuminate\Filesystem\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Response;

class ServeableAWSFile implements Serveable
{
    public function __construct(private FilesystemAdapter $aws, private string $path, private string $lifetime) {}

    public function serve(): Response
    {
        return redirect(
            $this->aws->temporaryUrl($this->path, Carbon::parse($this->lifetime))
        );
    }
}