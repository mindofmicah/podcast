<?php
namespace App\Domain\AudioStorage\ServeableFiles;

use Illuminate\Http\Response;

class ServeableMissingFile implements Serveable
{
    public function serve(): Response
    {
        return response()->make('', 404);
    }
}