<?php
namespace App\Domain\AudioStorage\ServeableFiles;

use Symfony\Component\HttpFoundation\Response;

interface Serveable
{
    public function serve(): Response;
}