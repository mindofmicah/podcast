<?php
namespace App\Domain\AudioStorage\ServeableFiles;

use Illuminate\Filesystem\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Response;

class ServeableLocalFile implements Serveable
{
    public function __construct(private FilesystemAdapter $filesystem, private string $path) {}

    public function serve(): Response
    {
        return $this->filesystem->response($this->path);
    }
}