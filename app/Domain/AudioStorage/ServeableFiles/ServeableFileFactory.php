<?php
namespace App\Domain\AudioStorage\ServeableFiles;

use App\Domain\AudioStorage\Config;
use Illuminate\Filesystem\FilesystemManager;

class ServeableFileFactory
{
    public function __construct(private FilesystemManager $filesystem, private Config $config) {}

    public function create(string $path): Serveable
    {
        $aws = $this->filesystem->disk('s3');
        if ($aws->exists($path)) {
            return new ServeableAWSFile($aws, $path, $this->config->aws_lifetime);
        }

        $local = $this->filesystem->disk('local');
        $local_path = 'podcasts/' . $path;
        if ($local->exists($local_path)) {
            return new ServeableLocalFile($local, $local_path);
        }

        return new ServeableMissingFile();
    }
}