<?php
namespace App\Domain\AudioStorage;

use MindOfMicah\Configgy\Config as BaseConfig;

/**
 * @property-read string $aws_lifetime
 */
class Config extends BaseConfig
{
    protected string $config_heading = 'audio-storage';
}