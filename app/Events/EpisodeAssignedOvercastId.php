<?php
namespace App\Events;

use App\Episode;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class EpisodeAssignedOvercastId
{
    use Dispatchable, SerializesModels;

    public $episode;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Episode $episode)
    {
        $this->episode = $episode;
    }
}
