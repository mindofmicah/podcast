<?php
namespace App\Events;

/**
 * Class EpisodeDiscovered
 * @package App\Events
 */
class EpisodeDiscovered extends Event
{
    public $feed;
    public $short_name;
    public $full_filename;

    /**
     * Create a new event instance.
     *
     * @param $feed
     * @param $short_name
     * @param $full_filename
     */
    public function __construct($feed, $short_name, $full_filename)
    {
        $this->feed = $feed;
        $this->short_name = $short_name;
        $this->full_filename = $full_filename;
    }
}
