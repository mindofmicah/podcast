<?php
namespace App\Events;

use App\Domain\Common\Models\PodcastFeed;
use App\Events\Contracts\RelatesToFeed;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class FeedWasCreated implements RelatesToFeed
{
    use Dispatchable, SerializesModels;

    public $feed;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PodcastFeed $feed)
    {
        $this->feed = $feed;
    }

    public function getFeed(): PodcastFeed
    {
        return $this->feed;
    }
}
