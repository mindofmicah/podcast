<?php
namespace App\Events;

use App\Domain\Common\Models\Episode;
use App\Domain\Common\Models\PodcastFeed;
use App\Events\Contracts\RelatesToFeed;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class EpisodePlayed implements RelatesToFeed
{
    use Dispatchable, SerializesModels;

    public $episode;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Episode $episode)
    {
        $this->episode = $episode;
    }

    public function getFeed(): PodcastFeed
    {
        return $this->episode->feed;
    }

}
