<?php
namespace App\Events\Contracts;

use App\Domain\Common\Models\PodcastFeed;

interface RelatesToFeed
{
    public function getFeed() : PodcastFeed;
}
