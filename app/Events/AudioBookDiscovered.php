<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AudioBookDiscovered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $feed;
    public $title;
    public $author;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Feed $feed, $title, $author)
    {
        $this->feed = $feed;
        $this->title = $title;
        $this->author = $author;
    }
}
