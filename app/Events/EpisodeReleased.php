<?php
namespace App\Events;

use App\Domain\Common\Models\Episode;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class EpisodeReleased
{
    use Dispatchable, SerializesModels;

    public $episode;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Episode $episode)
    {
        $this->episode = $episode;
    }
}
