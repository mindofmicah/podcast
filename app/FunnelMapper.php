<?php
namespace App;

use Illuminate\Support\Arr;

class FunnelMapper
{
    public function buildMapping($funnel_attribute = 'id', $funnelled_attribute = 'id'): array
    {
        $relationship = $this->feedRelationship($funnel_attribute);

        return FunnelledFeed::with($relationship, 'feeds')
            ->has($relationship)
            ->get()
            ->reduce(function (array $carry, FunnelledFeed $feed) use ($funnel_attribute, $funnelled_attribute) {
                /** @var Feed $real_feed */
                foreach ($feed->feeds as $real_feed) {
                    $carry[$real_feed->getAttribute($funnelled_attribute)] = Arr::get($feed, 'feed.' . $funnel_attribute);
                }
                return $carry;
            }, []);
    }

    private function feedRelationship($lookup_attribute): string
    {
        $chunks = explode('.', $lookup_attribute);
        array_pop($chunks);
        array_unshift($chunks, 'feed');

        return implode('.', $chunks);
    }
}