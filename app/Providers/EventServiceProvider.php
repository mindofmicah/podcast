<?php
namespace App\Providers;

use App\Events\EpisodeAssignedOvercastId;
use App\Feed;
use App\Listeners\SendNotificationForAssignedOvercastEpisode;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\EpisodeDiscovered' => [
            'App\Listeners\RegisterNewEpisode',
        ],
        \App\Events\EpisodePlayed::class => [
//            \App\Listeners\ReleaseNewEpisodes::class, 
        ],
        \App\Events\EpisodeReleased::class => [
            \App\Listeners\ScheduleEpisodeDiscoveryOnOvercast::class,
        ],
        EpisodeAssignedOvercastId::class=>[
            SendNotificationForAssignedOvercastEpisode::class
        ],
        \App\Events\AudioBookDiscovered::class => [
            \App\Listeners\StorePotentialAudiobook::class,
        ],
        \App\Events\FeedWasCreated::class=> [
            \App\Listeners\AdjustFirstEpisodeTitle::class,
            \App\Listeners\ReleaseNewEpisodes::class,
        ],
    ];

    public function boot()
    {
        parent::boot();

        \Event::listen(\App\Events\EpisodePlayed::class, function (\App\Events\EpisodePlayed $event) {
            static $feeds = [];

            $parent_feeds = $event->getFeed()->funnelConfigurations->map->feed->all();

            if (count($parent_feeds) === 0) {
                $parent_feeds[] = $event->getFeed();
            }

            /** @var Feed $feed */
            foreach ($parent_feeds as $feed) {
                $feed_key = $feed->getKey();
                if (!isset($feeds[$feed_key])) {
                    \App\Jobs\ReleaseNewEpisodes::dispatch($feed)
                        ->delay(now()->addMinute());
                }
                $feeds[$feed_key] = true;
            }
        });
    }
}
