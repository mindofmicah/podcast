<?php
namespace App\Providers;

use App\Domain\Common\Models\Episode;
use App\Domain\Common\Models\PodcastFeed;
use App\Domain\EpisodeDiscovery\EloquentEpisodeRepository;
use App\Domain\EpisodeDiscovery\EpisodeRepository;
use App\Domain\Services\YouTube\YouTubeDownloader;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;
use Tzsk\Collage\MakeCollage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->environment('production')) {
            \URL::forceScheme('https');
        }
        Relation::morphMap([
            'feed'    => PodcastFeed::class,
            'episode' => Episode::class,
        ]);
        Inertia::share([
            'errors' => function () {
                return Session::get('errors')
                    ? Session::get('errors')->getBag('default')->getMessages()
                    : (object)[];
            },
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EpisodeRepository::class, function () {
            return new EloquentEpisodeRepository();
        });

        $this->app->bind(YouTubeDownloader::class, function () {
            $downloader = new YouTubeDownloader([
                'continue' => true,
                //           'output'   => '%(title)s.%(ext)s',
            ]);
            $downloader->setBinPath(config('binaries.youtube-dl'));
            return $downloader;
        });

        $this->app->singleton(\App\FileMover::class, function () {

            $mover = new \App\FileMover($this->app['filesystem'], new \League\Flysystem\MountManager);
            $mover->loadDriver('local', 'podcasts', 'local-podcasts');
            $mover->loadDriver('dropbox');
            return $mover;
        });
        $this->app->bind(\MindOfMicah\OvercastSDK\OvercastSDK::class, function () {
            return new \MindOfMicah\OvercastSDK\OvercastSDK(config('services.overcast.username'), config('services.overcast.password'));
        });

        // Allow the Collage guy to be injected
        $this->app->bind(MakeCollage::class, function () {
            return $this->app['tzsk-collage'];
        });

    }
}
