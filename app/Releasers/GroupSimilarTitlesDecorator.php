<?php
namespace App\Releasers;

use Illuminate\Support\Collection;

class GroupSimilarTitlesDecorator extends AbstractDecorator
{
    CONST LIMIT = 'Limit to ONE Group';

    static protected $fields = [
        'santizier' => ['type' =>'text'],
        'options' => [
            'allow_multi' => true, 
            'values'      => [self::LIMIT,]
        ],
    ];


	/** @var string */
	protected $custom_sanitizer;

	/** @var bool */
    protected $limited = false;

    public function filterReleasableEpisodes(Collection $episodes)
    {
        $episodes = $this->releasable->filterReleasableEpisodes($episodes);

        if ($episodes->isEmpty()) {
            return $episodes;
        }

        $first_episode = $episodes->first();

        // If we only want one batch and there are still pending released episodes, bail out
        if ($this->limited && $first_episode->feed->episodes()->released()->unplayed()->exists()) {
            return $episodes->take(0);
        }
        
        $s1 = $this->sanitizeTitle($first_episode);

        for ($i = 1; $i < count($episodes); $i++) {
            if ($s1 != $this->sanitizeTitle($episodes[$i])) {
                break;
            }
        }

        return $episodes->take(
            $i < count($episodes) ? $i : 0
        );

    }    
    private function sanitizeTitle(\App\Episode $episode): string
    {
        return preg_replace(["/$this->custom_sanitizer/", '/[\d\W]/'], '', $episode->title ?? '') ?? '';
    }

    protected function parseOptions(array $options)
    {
        if (empty($options['santizier'])) {
            throw new \Exception('Please provide a sanitizer');
        }
        $this->custom_sanitizer = $options['santizier'];

        if (in_array(self::LIMIT, $options['options'])) {
            $this->limited = true;
        }
    }
}

