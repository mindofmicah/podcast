<?php
namespace App\Releasers;

use Illuminate\Support\Collection;

class SeasonDecorator extends AbstractDecorator
{
    const ONLY_USE_COMPLETED_SEASONS = 'Only use completed seasons';

    static protected $fields = [
        'options' => [
            'allow_multi' => true,
            'values'      => [self::ONLY_USE_COMPLETED_SEASONS,],
        ],
    ];

	/** @var bool */
    protected $only_uses_completed_seasons;

    protected function parseOptions(array $options)
    {
        $this->only_uses_completed_seasons = in_array(self::ONLY_USE_COMPLETED_SEASONS, $options['options'] ?? []);
    }

    public function filterReleasableEpisodes(Collection $episodes): Collection
    {
        $episodes = $this->releasable->filterReleasableEpisodes($episodes);
        if ($episodes->isEmpty()) {
            return $episodes;
        }

        $current_seasons = $episodes->first()->feed->episodes()
            ->whereNotNull('released_at')
            ->where('is_played', false)
            ->get()
            ->pluck('options.season')
            ->unique();

        if ($current_seasons->isEmpty()) {
            $current_seasons[] = $episodes->pluck('options.season')->unique()->pipe(function (Collection $unique_seasons) {
                if ($this->only_uses_completed_seasons && $unique_seasons->count() < 2) {
                    return $unique_seasons->take(0);
                }
                return $unique_seasons->min();

            });
        }

        return $episodes->whereIn('options.season', $current_seasons)->values();
    }

    public function description(): string
    {
        return ltrim($this->releasable->description() . ' released in seasons');
    }
}

