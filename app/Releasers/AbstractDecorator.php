<?php
namespace App\Releasers;

use App\Domain\Common\Models\PodcastFeed;
use App\Feed;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

abstract class AbstractDecorator implements Releasable
{
	/** @var Releasable */
    protected $releasable;

    /** @var Feed */
    protected $feed;

	/** @var bool Flag a releasable to has a special route */
    static protected $has_route = false;

	/** @var mixed[] */
    static protected $fields = [];

	/**
	 * @param Releasable $releasable
	 * @param mixed[] $options
	 */
    public function __construct(Releasable $releasable, array $options = [])
    {
        $this->releasable = $releasable;
        $this->parseOptions($options);
    }

    abstract public function filterReleasableEpisodes(Collection $episodes);

	/**
	 * @return mixed[]
	 */
    public static function getAvailableFields(): array
    {
        $defaults = [
            'type'        => 'string',
            'values'      => [],
            'allow_multi' => false,
        ];

        return array_map(function ($label) use ($defaults) {
            return array_merge(
                $defaults,
                static::$fields[$label] ?? [],
                compact('label')
            );
        }, Arr::isAssoc(static::$fields) ? array_keys(static::$fields) : static::$fields);
    }

    /**
	 * Generate a route to edit a releasable for a feed
	 *
	 * @return string|null
     */
    public static function getRoute()
    {
        if (static::$has_route) {
            return '/releasables/' . (strtolower(str_replace('Decorator', '', class_basename(get_called_class())))) . '/edit';
		}

		return null;
    }

	/**
	 * @param mixed[] $options
	 *
	 * @return void
	 */
    protected function parseOptions(array $options)
    {
        if (($options['feed'] ?? null) instanceof PodcastFeed) {
            $this->feed = $options['feed'];
        }
    }

    public function description()
    {
        return $this->releasable->description();
    }
}
