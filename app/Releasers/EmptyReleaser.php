<?php
namespace App\Releasers;

use Illuminate\Support\Collection;
class EmptyReleaser implements Releasable
{
    public function filterReleasableEpisodes(Collection $episodes)
    {
        return new Collection;
    }

    public function description()
    {
        return '';
    }
}
