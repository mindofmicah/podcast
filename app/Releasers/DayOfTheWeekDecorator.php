<?php
namespace App\Releasers;

use Illuminate\Support\Collection;
use Carbon\Carbon;

class DayOfTheWeekDecorator extends AbstractDecorator
{
	/** @var string[] */
    private $days;

    protected static $fields = [
        'days' =>[
            'allow_multi' => true,
            'values'      => [
                'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
            ],
        ]
    ];

    public function filterReleasableEpisodes(Collection $episodes)
    {
        if ($this->hasTodayInTheSchedule() === false) {
            $episodes = $episodes->take(0);
        }

        return $this->releasable->filterReleasableEpisodes($episodes);
    }    

    protected function parseOptions(array $options)
    {
        if (empty($options['days']) || !is_array($options['days'])) {
            throw new \Exception('Please provide a limit');
        }
        $this->days = $options['days'];
    }

    public function description()
    {
        return ltrim($this->releasable->description() . ('on: ' . implode(', ', $this->days)),', ');
    }

    private function hasTodayInTheSchedule(): bool
    {
        return in_array(Carbon::today()->format('l'), $this->days);
    }
}

