<?php
namespace App\Releasers;

use Illuminate\Support\Collection;
class BasicReleaser implements Releasable
{
    public function filterReleasableEpisodes(Collection $episodes)
    {
        return $episodes;
    }

    public function description()
    {
        return '';
    }
}
