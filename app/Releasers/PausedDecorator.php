<?php
namespace App\Releasers;

use Illuminate\Support\Collection;
use Carbon\Carbon;

class PausedDecorator extends AbstractDecorator
{
	/** @var Carbon */
    private $unpause_date;

    static protected $fields = [
        'unpause' => ['type' => 'date'], 
    ];

    public function filterReleasableEpisodes(Collection $episodes)
    {
        if (($this->unpause_date === null) || $this->unpause_date->isFuture()) {
            $episodes = $episodes->take(0);
        }
     
        return $this->releasable->filterReleasableEpisodes($episodes);
    }    

    protected function parseOptions(array $options)
    {
        if (!empty($options['unpause'])) {
            $this->unpause_date = Carbon::parse($options['unpause']);
        }
    }
}

