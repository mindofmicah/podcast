<?php
namespace App\Releasers;

use Illuminate\Support\Collection;

class IntervalDecorator extends AbstractDecorator
{
	/** @var int */
	private $limit;
	/** @var string */
    private $interval_type;

    protected static $fields = [
        'limit' =>[
            'type' => 'number',
        ], 
        'interval' => [
            'values' => [
                'Day', 'Week', 'Year'
            ]
        ]
    ];
    public function filterReleasableEpisodes(Collection $episodes)
    {
        $episodes = $this->releasable->filterReleasableEpisodes($episodes);
        if ($episodes->isEmpty()) {
            return $episodes;
        }

        $constraint = now();
        switch (strtolower($this->interval_type)) {
        case 'day':
            $constraint->startOfDay();
            break;
        case 'week':
            $constraint->startOfWeek();
            break;
        case 'year':
            $constraint->startOfYear();
            break;
        }

        $feed = $episodes->first()->feed;
        $num_episodes_this_interval = $feed->episodes()->where('released_at', '>=', $constraint)->count();

        return $episodes->take(max($this->limit - $num_episodes_this_interval, 0));
    }    
    protected function parseOptions(array $options)
    {
        if (empty($options['limit'])) {
            throw new \Exception('Please provide a limit');
        }
        if (empty($options['interval'])) {
            throw new \Exception('Please provide an interval');
        }


        $this->limit = $options['limit'];
        $this->interval_type = $options['interval'];
    }
    public function description()
    {
        return ltrim($this->releasable->description() .
        ($this->limit . ' per ' . $this->interval_type),', ');
    }

}

