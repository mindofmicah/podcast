<?php
namespace App\Releasers;

use App\Domain\Common\Models\Episode;
use Illuminate\Support\Collection;
use Exception;

class PriorityPatternDecorator extends AbstractDecorator
{
	/** @var string */
    private $pattern;

    protected static $fields = [
        'pattern' 
    ];

    public function filterReleasableEpisodes(Collection $episodes)
    {
        $episodes = $episodes->partition(function (Episode $episode) {
            return preg_match("/{$this->pattern}/i", $episode->title ?? '');
        })->collapse();

        return $this->releasable->filterReleasableEpisodes($episodes);
    }    

    protected function parseOptions(array $options)
    {
        if (empty($options['pattern'])) {
            throw new Exception('Please provide a pattern');
        }

        $this->pattern = $options['pattern'];
    }

    public function description()
    {
        return $this->releasable->description();
    }
}

