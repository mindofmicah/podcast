<?php
namespace App\Releasers;

use App\Domain\Common\Models\Episode;
use Illuminate\Support\Collection;

class PickemDecorator extends AbstractDecorator
{
	/** @var string */
	private $strategy;
	/** @var string[] */
    private $items = [];

    static protected $fields = ['strategy', 'items'];

    static protected $has_route = true;

    public function filterReleasableEpisodes(Collection $episodes)
    {
        $episodes = $episodes->filter(function (Episode $episode) {
            return in_array($episode->id, $this->items);
        });

        $episodes = $this->sortByStrategy($episodes);

        return $this->releasable->filterReleasableEpisodes($episodes);
    }    

    protected function parseOptions(array $options)
    {
        $this->strategy = $options['strategy'] ?? 'oldest';

        $this->items = array_filter(explode(',', $options['items'] ?? ''));
    }

	/**
	 * @param Collection|Episode[] $episodes
	 *
	 * @return Collection|Episode[]
	 */
    private function sortByStrategy(Collection $episodes): Collection
    {
        switch($this->strategy) {
        case 'oldest':
            return $episodes->sortBy('order');
        case 'newest':
            return $episodes->sortByDesc('order');
        case 'random':
            return $episodes->shuffle();
        }
        throw new \InvalidArgumentException('Invalid strategy provided'); 
    }
}

