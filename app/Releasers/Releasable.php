<?php
namespace App\Releasers;

use Illuminate\Support\Collection;

interface Releasable
{
	/**
	 * @param Collection|\App\Episode[] $episodes
	 *
	 * @return Collection|\App\Episode[]
	 */
	public function filterReleasableEpisodes(Collection $episodes);

	/**
	 * @return string
	 */
    public function description();
}
