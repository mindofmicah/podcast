<?php
namespace App\Releasers;

use Illuminate\Support\Collection;

class ChunkedDecorator extends AbstractDecorator
{
	/** @var int */
    private $min;
	/** @var int */
    private $max;

    static protected $fields = [
        'min' => ['type' => 'number'], 
        'max' => ['type' => 'number'],
    ];

    public function filterReleasableEpisodes(Collection $episodes)
    {
        $episodes = $this->releasable->filterReleasableEpisodes($episodes);
        if ($episodes->isEmpty()) {
            return $episodes;
        }

        if ($episodes->first()->feed->unplayedEpisodes()->released()->exists()) {
            return $episodes->take(0);
        };

        if ($this->max) {
            $episodes = $episodes->take($this->max);
        }

        if ($this->min) {
            if ($this->min > $episodes->count()) {
                return $episodes->take(0);
            }
        }

        return $episodes;
    }    

    protected function parseOptions(array $options)
    {
        if (empty($options['min']) && empty($options['max'])) {
            throw new \Exception('Please provide a min or max');
        }

        $this->min = $options['min'] ?? null;
        $this->max = $options['max'] ?? null;
    }
}

