<?php
namespace App\Releasers;

use App\Domain\Common\Enums\EpisodeFilter;
use Exception;
use Illuminate\Support\Collection;

class EpisodeLimitDecorator extends AbstractDecorator
{
	/** @var int */
    private $episode_limit;

    static protected $fields = [
        'limit' => ['type' => 'number'],
    ];

    public function filterReleasableEpisodes(Collection $episodes)
    {
        $episodes = $this->releasable->filterReleasableEpisodes($episodes);
        if ($episodes->isEmpty()) {
            return $episodes;
        }

        $num_released_episodes = $this->feed->episodes()->filter(EpisodeFilter::OUTSTANDING)->count();

        return $episodes->take(max(($this->episode_limit - $num_released_episodes), 0));
    }

    protected function parseOptions(array $options)
    {
        parent::parseOptions($options);

        if (empty($options['limit'])) {
            throw new Exception('Please provide a limit');
        }
        $this->episode_limit = $options['limit'];
    }

    public function description()
    {
        return ltrim($this->releasable->description() . ', ' . $this->episode_limit . ' at a time', ', ');
    }
}

