<?php
namespace App\Console;

use App\Console\Commands\PlayedEpisodeCleanupCommand;
use App\Console\Scheduling\Schedule;
use Illuminate\Console\Scheduling\Schedule as LaravelSchedule;
use MindOfMicah\Teletraan\Kernel as TeletraanKernel;

class Kernel extends TeletraanKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(LaravelSchedule $schedule)
    {
        $schedule->command('send:feed-reminders')->wednesdays()->at('19:00');

        $schedule->command('fetch:overcast-summary', ['--save-summary'])
            ->dailyAt('6:23');

//        $schedule->command('fetch:overcast-summary')
//            ->cron('25,50 8-20 * * *');

        $schedule->command('episodes:release')
            ->dailyAt('7:15');

        $schedule->command('cache:prime')
            ->dailyAt('19:15');

        $schedule->emailedCommand('played-episodes:discover', 'Episodes marked as played')
            ->cron('25,50 * * * *');

        $schedule->emailedCommand('inactive-feeds:discover', 'Feed marked as inactive')
            ->cron('5 22 * * *');

        $schedule->emailedCommand('overcast:assign-ids', 'Overcast Ids set for Episodes')
            ->cron('*/20 * * * *');

        $schedule->emailedCommand('telescope:prune', 'Clear out the telescope cache')->daily();

        $schedule->emailedCommand('discover:new-episodes', 'Look for new emails for the feeds')
            ->cron('30 6 * * *');

        $schedule->emailedCommand('assign:feed-schedules', 'Reasign feed schedules')->monthlyOn(13, '13:13');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
