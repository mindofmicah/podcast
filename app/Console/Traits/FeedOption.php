<?php
namespace App\Console\Traits;

use App\Domain\Common\Models\PodcastFeed;
use App\Domain\Common\QueryBuilders\FeedQueryBuilder;
use App\Feed;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use RuntimeException;
use Symfony\Component\Console\Input\InputOption;

trait FeedOption
{
    protected function addFeedOption()
    {
        $this->addOption('feed', 'f', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Feeds to limit the command');
        $this->addOption('feed-prompt', 'p', InputOption::VALUE_NONE, 'Show available feeds');
    }

    protected function beginFeedQuery(callable $if_no_feeds_passed = null): Builder
    {
        return Feed::when($this->normalizeFeedInput(), function (Builder $query, array $feeds) {
            return $query->withInactive()->withTrashed()->whereIn('path', $feeds);
        }, $if_no_feeds_passed);
    }

    /**
     * Prefer the feed option, but also prompt for a feed if necessary
     *
     * @return mixed|null
     */
    protected function normalizeFeedInput()
    {
        $passed_in_feeds = $this->option('feed');

        if (!empty($passed_in_feeds)) {
            return $passed_in_feeds;
        }

        if ($this->option('feed-prompt')) {
            $feeds = PodcastFeed::query()->orderBy('title')->when(property_exists($this, 'feed_option_scope'), function (FeedQueryBuilder $query) {
                if ($this->feed_option_scope === 'INACTIVE_ONLY') {
                    return $query->whereInactive();
                } elseif($this->feed_option_scope === 'DELETED_ONLY') {
                    return $query->whereNotNull('deleted_at')->withTrashed();
                }
                throw new RuntimeException('not yet implemented');
            })->pluck('path', 'title')->all();
            return Arr::wrap($feeds[$this->choice('Which feed would you like?', array_keys($feeds))]);
        }

        return null;
    }
}