<?php
namespace App\Console\Traits;

use InvalidArgumentException;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

trait SpiffyStyle
{
    protected $spiffy_styles = [
        'highlight' => [
            'yellow', null, ['bold'],
        ],
    ];

    public function addSpiffyStyle($style)
    {
        if (empty($this->spiffy_styles[$style])) {
            throw new InvalidArgumentException($style . ' is not a defined spiffy style. Options are: ' . implode(array_keys($this->spiffy_styles)));
        }

        $this->output->getFormatter()->setStyle($style, new OutputFormatterStyle(...$this->spiffy_styles[$style]));
    }
}