<?php
namespace App\Console\Traits;

use Illuminate\Contracts\Queue\ShouldQueue;

trait Dispatching
{
    protected function dispatch(ShouldQueue $job, callable $callback = null)
    {
        $is_verbose = $this->output->isVerbose();

        $result = call_user_func($is_verbose ? 'dispatch_now' : 'dispatch', $job);

        return !$is_verbose || !$callback
            ? $result
            : $callback($result) ?? $result;
    }
}