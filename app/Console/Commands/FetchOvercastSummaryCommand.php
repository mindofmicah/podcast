<?php
namespace App\Console\Commands;

use App\Jobs\ProcessEpisodeSummary;
use App\Jobs\SearchSummaryForNewlyPlayedEpisodes;
use Illuminate\Filesystem\Filesystem;
use MindOfMicah\Teletraan\Command;
use MindOfMicah\OvercastSDK\OvercastSDK;
use Illuminate\Config\Repository as Config;

class FetchOvercastSummaryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:overcast-summary {--save-summary}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab the latest summary from Overcast';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OvercastSDK $overcast, Config $config, Filesystem $fs)
    {
        $overcast_path = $config->get('services.overcast.summary_path') ;
        $this->info('Fetching summary file from Overcast.fm');

        $contents = $overcast->fetchSummary();
        if (!$fs->exists($overcast_path) && (md5_file($overcast_path) === md5($contents))) {
            $this->info('No changed detected. No updates performed');
            return;
        }

        $fs->put($overcast_path, $overcast->fetchSummary());
        $this->info('Successfully fetched summary file from Overcast.fm');

        if ($this->option('save-summary')) {
            ProcessEpisodeSummary::dispatch($overcast_path);
        }

        SearchSummaryForNewlyPlayedEpisodes::dispatch($overcast_path);
    }
}
