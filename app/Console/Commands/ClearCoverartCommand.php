<?php

namespace App\Console\Commands;

use App\Feed;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MindOfMicah\Teletraan\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ClearCoverartCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:coverart 
                            {feed : feed to clear out} 
                            {--d|delete-file : should the existing file be deleted?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear the coverart attribute for a feed';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $result = Feed::where('path', $this->argument('feed'))
                ->firstOrFail()
                ->clearCoverart($this->option('delete-file'));
        } catch (ModelNotFoundException $exception) {
            $this->error('No Feed with the path: ' . $this->argument('feed'));
            return;
        }

        if ($result) {
            $output = 'Successfully removed the coverart for ' . $this->argument('feed');
            if ($this->option('delete-file')) {
                $output .= ' and removed the local file';
            }
            $this->info($output);
            return;
        }
        $this->error('an unknown error occurred');
    }
}
