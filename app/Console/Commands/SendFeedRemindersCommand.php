<?php

namespace App\Console\Commands;

use MindOfMicah\Teletraan\Command;

class SendFeedRemindersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:feed-reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new \App\User;
        $user->email = env('TELETRAAN_EMAIL');
        \Mail::to($user)
            ->send(new \App\Mail\ReleaseSomeEpisodes);
    }
}
