<?php
namespace App\Console\Commands;

use App\Console\Traits\Dispatching;
use App\Console\Traits\FeedOption;
use App\Console\Traits\SpiffyStyle;
use App\Domain\Common\Models\PodcastFeed;
use App\FeedSources\FetchedInformation;
use Illuminate\Database\Eloquent\Builder;
use MindOfMicah\Teletraan\Command;
use App\Jobs\DiscoverNewEpisodes;

class DiscoverNewEpisodesCommand extends Command
{
    use Dispatching, SpiffyStyle, FeedOption;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:new-episodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for new episodes on all active feeds';

    protected function configure()
    {
        parent::configure();
        $this->addFeedOption();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->addSpiffyStyle('highlight');

        $this->beginFeedQuery(function (Builder $query) {
                return $query->shouldScanForDay(today())
                    ->where(function ($query) {
                        $query->whereDoesntHave('episodes')
                            ->orWhereHas('episodes', function (Builder $query) {
                                $query->where('created_at', '>', now()->subDays(90));
                            });
                    });
            })
            ->each(function (PodcastFeed $feed) {
                $this->dispatch(new DiscoverNewEpisodes($feed), function (FetchedInformation $response = null) use ($feed) {
                    if($response === null) {
                        return $this->line("<highlight>{$feed->title}</highlight> did not look for any results");
                    }

                    $new_episodes = $response->newEpisodes();

                    $this->line("<highlight>{$feed->title}</highlight> found <highlight>{$new_episodes->count()}</highlight> results");
                    $new_episodes->each(function ($episode) {
                        $this->line('- ' . $episode['title']);
                    });
                });
            });
    }
}
