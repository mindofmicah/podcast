<?php
namespace App\Console\Commands;

use App\Domain\Common\Models\Episode;
use App\Domain\EpisodeReleasing\Actions\MarkEpisodeAsPlayedAction;
use App\Events\EpisodePlayed;
use App\Feed;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Events\Dispatcher;
use MindOfMicah\OvercastSDK\OvercastSDK;
use MindOfMicah\Teletraan\Command;

class MarkPlayedEpisodesAsPlayed extends Command
{
    /** @var string The name and signature of the console command. */
    protected $signature = 'played-episodes:discover';

    /** @var string The console command description. */
    protected $description = 'Compare the database with the episodes currently in the overcast queue';

    private $episodes_marked_as_played = 0;

    /**
     * Execute the console command.
     *
     * @param OvercastSDK $overcastSDK
     * @param Dispatcher $events
     * @param MarkEpisodeAsPlayedAction $mark_as_played
     *
     * @return mixed
     */
    public function handle(OvercastSDK $overcastSDK, Dispatcher $events, MarkEpisodeAsPlayedAction $mark_as_played)
    {
        // Track anything we update along the way
        $events->listen(EpisodePlayed::class, function (EpisodePlayed $event) {
            $this->queueOutput('info', 'Episode Marked As Played: ' . $event->episode->title);
            $this->episodes_marked_as_played++;
        });

        [$stale, $active] = collect($overcastSDK->getFeeds())->mapToGroups(function (array $ary) {
            return [$ary['has_unplayed'] => $ary['id']];
        });

        if ($stale->isNotEmpty()) {
           Episode::query()
                ->whereUnplayed()
                ->whereReleased('-30 minutes')
                ->has('overcast_id')
                ->whereHas('parent_feeds', function ($q) use ($stale) {
                    // TODO can prolly just do parent_feeds.overcast_id?
                    return $q->whereHas('overcast_id', function ($q) use ($stale) {
                        $q->whereIn('overcast_id', $stale->all());
                    });
                })->each(function (Episode $episode) use ($mark_as_played) {
                    $mark_as_played->execute($episode);
                });
        }

        // Only if anything was marked as played, will we show any output
        if ($this->episodes_marked_as_played) {
            $this->printQueuedOutput();
        }

        return;
        // Revisit this once Rate Limit is over :)
        Feed::all()->filter(function (Feed $feed) use ($outstanding_feeds) {
            if (!in_array(optional($feed->overcast_id)->overcast_id, $outstanding_feeds)) {
                return false;
            }
            if ($feed->unplayedEpisodes()->count() === 0) {
                return false;
            }

            if ($feed->episodes()->released()->unplayed()->count() === 0) {
                return false;
            }

            return true;
        })->each(function (Feed $feed) {
            $this->info("Checking $feed->title");
            sleep(3);
            /** @var Collection|Episode[] $episodes */
            $episodes = $feed->episodes()->with('overcast_id')->unplayed()->released()->get()->keyBy('overcast_id.overcast_id')->toBase();

            if (!$feed->overcast_id) {
                return;
            }

            try {
                $overcast_episodes = $this->overcast->getFeedEpisodes($feed->overcast_id->overcast_id);
            } catch (RequestException $exception) {
                if ($exception->getCode() === 429) {
                    // We've hit a rate limit, chill
                    $this->error('Rate limit reached. Going to chill for a bit');
                    return false;
                }

                $this->error('An error occurred with ' . $feed->title);
                $this->error($exception->getMessage());
                return null;
            }

            $this->filterToOnlyPlayedIds($episodes, $overcast_episodes)
                ->each(function (Episode $episode) {
                    $this->markEpisodeAsPlayed($episode);
                });
        });
    }

    private function filterToOnlyPlayedIds(\Illuminate\Support\Collection $episodes, $overcast_episodes): \Illuminate\Support\Collection
    {
        $only_played_ids = array_keys(array_filter(array_column($overcast_episodes, 'is_played', 'id')));

        return $episodes->only($only_played_ids);
    }
}
