<?php
namespace App\Console\Commands;

use App\Console\Traits\Dispatching;
use App\Console\Traits\FeedOption;
use App\Console\Traits\SpiffyStyle;
use App\Episode;
use App\Feed;
use App\Jobs\ReleaseNewEpisodes;
use Illuminate\Database\Eloquent\Builder as Query;
use Illuminate\Support\Collection;
use MindOfMicah\Teletraan\Command;

class ReleaseNewEpisodesCommand extends Command
{
    use Dispatching, SpiffyStyle, FeedOption;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'episodes:release {--days-to-include=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'try to release new episodes for stale podcasts';

    protected function configure()
    {
        parent::configure();
        $this->addFeedOption();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->addSpiffyStyle('highlight');

        $this->beginFeedQuery(function (Query $query) {
                $query->when($this->option('days-to-include'), function (Query $query, $days) {
                    $query->where('updated_at', '>=', today()->subDays($days));
                })
                ->has('overcast_id')
                ->whereHas('episodes', function (Query $query) {
                    $query->where('is_played', 0);
                    $query->whereNotNull('released_at');
                }, 0)
                ->whereHas('episodes', function (Query $query) {
                    $query->where('is_played', 0);
                });
            })
            ->each(function (Feed $feed) {
                $this->dispatch(new ReleaseNewEpisodes($feed), function (Collection $released_episodes) use ($feed) {
                    /** @var Episode[]|Collection $released_episodes */
                    if ($released_episodes->isEmpty()) {
                        return $this->line("<highlight>{$feed->title}</highlight> did not have any episodes to release");
                    }

                    $this->line("<highlight>{$feed->title}</highlight> released <highlight>{$released_episodes->count()}</highlight> episodes");
                    $released_episodes->each(function ($episode) {
                        $this->line('- ' . $episode['title']);
                    });
                });
            });
    }
}
