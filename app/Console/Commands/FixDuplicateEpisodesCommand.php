<?php
namespace App\Console\Commands;

use MindOfMicah\Teletraan\Command;
use Illuminate\Support\Collection;
use App\Feed;

class FixDuplicateEpisodesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:duplicate-episodes {feed : Slug of the feed to fix}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Feed::where('path', $this->argument('feed'))->firstOrFail()
            ->episodes
            ->groupBy('title')
            ->filter(function (Collection $group) {
                return $group->count() > 1;
            })
            ->each(function (Collection $episodes) {
                list($orig, $new) = $episodes->sortBy('id');

                if (!$orig->is_played) {
                    $orig->url = $new->url;
                    $this->info("Updated $new->title to $new->url");
                }
                $new->delete();
                $orig->save();
            });
    }
}
