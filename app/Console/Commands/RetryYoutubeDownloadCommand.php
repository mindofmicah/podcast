<?php
namespace App\Console\Commands;

use App\Console\Traits\Dispatching;
use App\Domain\Common\Models\Episode;
use App\Domain\Services\ArtisanGUI\OutstandingYoutubeVideos;
use App\Jobs\DownloadYoutubeAsAudio;
use MindOfMicah\Teletraan\Command;

class RetryYoutubeDownloadCommand extends Command
{
    use Dispatching;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retry:youtube-download {--id=}';

    /** @var OutstandingYoutubeVideos */
    private $youtube_options;

    public function __construct(OutstandingYoutubeVideos $youtube_options)
    {
        parent::__construct();
        $this->youtube_options = $youtube_options;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($id = $this->option('id')) {
            $episode = Episode::findOrFail($id);
        } else {
            $episode = $this->getEpisodeFromConsole();
        }

        $this->info("Attempting to download $episode->title again from YouTube");
        $this->dispatch(new DownloadYoutubeAsAudio($episode->filename, $episode->feed->path, (bool)$this->verbosity));
    }

    private function getEpisodeFromConsole(): Episode
    {
        $possible_episodes = $this->youtube_options->values();

        $episode_index = $this->choice('Which episode would you like to retry downloading?', array_values($possible_episodes));
        return Episode::with('feed')->findOrFail(array_search($episode_index, $possible_episodes));
    }
}
