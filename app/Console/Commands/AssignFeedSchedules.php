<?php
namespace App\Console\Commands;

use App\Episode;
use App\Feed;
use App\RemoteFeed;
use Illuminate\Database\Eloquent\Collection;
use MindOfMicah\Teletraan\Command;

class AssignFeedSchedules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assign:feed-schedules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To limit lookups, use historical data to assign feed schedules';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Episode::groupBy(['feed_id', \DB::raw('strftime("%w", created_at)')])
            ->whereHas('feed', function ($query) {
                $query->ofType(RemoteFeed::class);
            })
            ->with('feed')
            ->get()
            ->groupBy(function (Episode $episode) {
                return $episode->feed_id;
            })
            ->each(function (Collection $episodes) {
                /** @var Episode[]|Collection $episodes */
                /** @var Feed $feed */
                $feed = $episodes->first()->feed;

                $schedule = ($episodes->map(function (Episode $episode) {
                    return $episode->created_at->dayOfWeek;
                })->reduce(function (int $carry, int $day) {
                    return $carry | pow(2, $day);
                }, 0));

                if ($feed->scan_schedule != $schedule) {
                    $feed->update(['scan_schedule' => $schedule]);
                    $this->info("Updating {$feed->title}'s feed schedule to be {$schedule}");
                }
            });
    }
}
