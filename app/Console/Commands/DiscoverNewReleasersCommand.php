<?php

namespace App\Console\Commands;

use MindOfMicah\Teletraan\Command;

class DiscoverNewReleasersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:releasers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(\Illuminate\Filesystem\Filesystem $file)
    {
        $releasers = \App\Domain\Common\Models\ReleaseScheduleDecorator::all()->keyBy('class_name');

        foreach($file->allFiles(app_path('Releasers')) as $file) {
            $class_name = 
                app()->getNamespace() . str_replace([app_path(),'/'], '', $file->getPath()) .'\\'. preg_replace('/\.php$/', '',$file->getFilename());
            $stuff = new \ReflectionClass($class_name);

            if (!$stuff->isSubclassOf(\App\Releasers\AbstractDecorator::class)) {
                continue;
            }

            $label = trim(preg_replace([
                    '/Decorator$/',
                    '/([A-Z][^A-Z]+)/'
                ], [
                    '',
                    ' $1'
                ],
                $stuff->getShortName()));


            $field_names = array_column($class_name::getAvailableFields(), 'label');

            if (!$releasers->has($class_name)) {
                $releaser = \App\Domain\Common\Models\ReleaseScheduleDecorator::create([
                    'label'      => $label,
                    'class_name' => $class_name,
                    'options'    => $field_names 
                ]);
                $this->info('Releaser Discovered: ' . $releaser->label);
                continue;
            }

            $existing_releaser = $releasers->get($class_name);

            if ($field_names == $existing_releaser->options) {
                continue;
            }

            $existing_releaser->update([
                'options'=>$field_names
            ]);
            $this->info('Releaser Updated:' . $existing_releaser->label);
        }

    }
}
