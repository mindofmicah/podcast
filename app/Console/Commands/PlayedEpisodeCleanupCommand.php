<?php

namespace App\Console\Commands;

use App\Episode;
use App\Feed;
use Carbon\Carbon;
use Illuminate\Console\Command;

class PlayedEpisodeCleanupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cleanup:episodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove old episodes to free space';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cutoff_date = Carbon::now()
            ->subDays(2)
            ->startOfDay();

        $played_episodes = Episode::where('is_played', true)
            ->where('updated_at', '<=', $cutoff_date)
            ->get();

        if ($played_episodes->isEmpty()) {
            $this->line('No episodes to delete');

            return;
        }

        $feeds = Feed::whereIn('id', $played_episodes->pluck('feed_id'))->withTrashed()->get()
            ->keyBy('id');
        $filenames = $played_episodes->groupBy('feed_id')
            ->map(function ($chunks, $feed_id) use ($feeds) {
                $path = $feeds[$feed_id]->path;

                return $chunks->map(function ($episode) use ($path) {
                    return "{$path}/{$episode->filename}";
                });
            });

        foreach ($feeds as $feed_id => $feed) {
            if ($feed->feedable_type === \App\DropboxFeed::class) {
                $current_files = \Storage::disk('dropbox')->allFiles($feed->path);

                $filenames[$feed_id]->intersect($current_files)->each(function ($filename) {
                    $this->line('Deleting ' . $filename);
                    \Storage::disk('dropbox')->delete($filename);
                });
            }
        }
    }
}
