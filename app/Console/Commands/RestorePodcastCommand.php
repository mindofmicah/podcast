<?php
namespace App\Console\Commands;

use App\Console\Traits\FeedOption;
use App\Domain\PodcastFeeds\Models\PodcastFeed;
use App\Feed;
use MindOfMicah\Teletraan\Command;

class RestorePodcastCommand extends Command
{
    use FeedOption;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restore:feed';

    protected $feed_option_scope = 'DELETED_ONLY';

    protected function configure()
    {
        parent::configure();
        $this->addFeedOption();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Feed::restored(function (Feed $feed) {
            $this->info("$feed->title has been restored!");
        });

        $this->beginFeedQuery()->each(function (PodcastFeed $feed) {
            $feed->restore();
        });
    }
}
