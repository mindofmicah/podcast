<?php
namespace App\Console\Commands;

use App\Domain\Common\Models\Episode;
use App\Domain\Common\Models\PodcastFeed as Feed;
use App\FunnelMapper;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Database\Eloquent\Collection;
use MindOfMicah\OvercastSDK\OvercastSDK;
use MindOfMicah\Teletraan\Command;

class AssignOvercastIds extends Command
{
    /** @var string The name and signature of the console command. */
    protected $signature = 'overcast:assign-ids';

    /** @var string The console command description. */
    protected $description = 'Assign overcast-generated ids to outstanding feeds/episodes';

    /** @var array Map funnel-dependent feeds to the main feed */
    protected $funnel_mapping = [];

    /**
     * Execute the console command.
     *
     * @param OvercastSDK $overcast
     * @param FunnelMapper $mapper
     *
     * @return mixed
     */
    public function handle(OvercastSDK $overcast, FunnelMapper $mapper)
    {
        $overcast_feeds = collect();

        $this->funnel_mapping = $mapper->buildMapping('overcast_id.overcast_id');
        $has_matches = false;

        Feed::whereDoesntHave('overcast_id')
            ->active()
            ->get()
            ->tap(function ($feeds) use ($overcast, &$overcast_feeds) {
                if ($feeds->count()) {
                    $overcast_feeds = collect($overcast->getFeeds())->pluck('id', 'title');
                }
            })
            ->each(function (Feed $feed) use ($overcast_feeds, &$has_matches) {
                if ($overcast_id = ($overcast_feeds[$feed->decorated_title] ?? null)) {
                    $feed->setOvercastId($overcast_id);
                    $this->queueOutput('line', $feed->title . ' is being assigned ' . $overcast_id);
                    $has_matches = true;
                }
            });

        /** @var Episode[]|Collection $episodes_to_check */
        $episodes_to_check = Episode::with('feed')
            ->has('feed')
            ->with('feed.overcast_id')
            ->whereDoesntHave('overcast_id')
//            ->unplayed()
            ->released()
            ->get()
            ->keyBy('options.title');

        if ($episodes_to_check->count()) {
            $episodes_to_check->groupBy('feed_id')->each(function (Collection $episodes) use ($overcast, &$has_matches) {
                if (($feed_id = $this->getFeedIdForEpisode($episodes->first())) === null) {
                    return;
                }
                try {
                    $feed_episodes = $overcast->getFeedEpisodes($feed_id);
                } catch (ClientException $e) {
                    $feed_name = $episodes->first()->feed->title;
                    if ($e->getCode() === 429) {
                        $this->queueOutput('error', "$feed_name has been rate-limited");
                        return;
                    }
                    if ($e->getCode() === 404) {
                        $this->queueOutput('error', "$feed_name does not exist on Overcast.fm");
                        return;
                    }
                }

                $feed_episodes = collect($feed_episodes)->keyBy('id');
                $existing_ids = \App\OvercastId::pluck('overcast_id');
                $feed_episodes = $feed_episodes->only($feed_episodes->keys()->diff($existing_ids));

                foreach ($feed_episodes as $overcast_episode) {
                    $episode = $this->findLocalVersionOfOvercastEpisode($overcast_episode, $episodes);
                    if ($episode) {
                        $episode->setOvercastId($overcast_episode['id']);
                        $episode->load('overcast_id');
                        $this->queueOutput('line', $episode->title . ' is being assigned ' . $overcast_episode['id']);
                        $has_matches = true;
                    }
                }
            });
        }

        if ($has_matches) {
            $this->printQueuedOutput();
        }
    }

    /**
     * @param array $overcast_episode
     * @param Collection|Episode[] $episodes_to_check
     *
     * @return Episode|null
     */
    private function findLocalVersionOfOvercastEpisode(array $overcast_episode, Collection $episodes_to_check)
    {
        return $episodes_to_check->first(function (Episode $episode) use ($overcast_episode) {
            if ($episode->overcast_id) {
                return null;
            }

            if (!empty($overcast_episode['author']) && ('Magic Playlist - ' . $episode->feed->title != $overcast_episode['author'])) {
                return null;
            }
            if (ends_with(preg_replace('/\W/', '', $episode->title), preg_replace('/\W/', '', $overcast_episode['title']))) {
                return $episode;
            }

            return null;
        });
    }

    /**
     * @param Episode $episode
     *
     * @return null|string
     */
    private function getFeedIdForEpisode(Episode $episode)
    {
        $feed = $episode->feed;
        return optional($feed->overcast_id)->overcast_id ?? $this->funnel_mapping[$feed->id] ?? null;
    }
}
