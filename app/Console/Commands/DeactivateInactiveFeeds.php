<?php
namespace App\Console\Commands;

use App\Domain\Common\Models\PodcastFeed;
use App\Domain\FeedManagement\Actions\DeactivateFeedAction;
use MindOfMicah\OvercastSDK\OvercastSDK;
use MindOfMicah\Teletraan\Command;

class DeactivateInactiveFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inactive-feeds:discover';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Look for any feeds that have had all of their episodes listened to and not subscribed to in Overcast';

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle(OvercastSDK $overcast, DeactivateFeedAction $action)
    {
        PodcastFeed::whereHas('episodes', function ($query) {
            $query->havingRaw('COUNT(*) = SUM(is_played)');
            $query->groupBy('episode_feed.feed_id');
        })
        ->ignoreFunnelledFeeds()
        ->get()
        ->whereNotIn('decorated_title', array_pluck($overcast->getFeeds(), 'title'))
        ->each(function (PodcastFeed $feed) use ($action) {
            $action->execute($feed);
            $this->info($feed->title . ' has been marked as "inactive"');
        });
    }
}
