<?php

namespace App\Console\Commands;

use MindOfMicah\Teletraan\Command;

class PrimeCacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:prime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(\Illuminate\Contracts\Cache\Repository $cache)
    {
        $cache->put(
            'inactive-feeds',
            \App\Feed::withInactive()
                ->where('is_active', false)
                ->pluck('path')
                ->toArray(),
            now()->addDay()
        );
    }
}
