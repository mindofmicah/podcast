<?php

use Faker\Generator as Faker;

$factory->define(\App\Episode::class, function (Faker $faker) {
    return [
        'filename'=>'filename.mp3',
        'options'=>[
            'title'=>'Episode Title', 
        ],
        'feed_id' => function () {
            return factory(\App\Feed::class)->create()->id;
        }
    ];
});
