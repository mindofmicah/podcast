<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('feed_id')->index();
            $table->string('filename');
            $table->json('options')->nullable();
            $table->integer('order', false, true)->nullable();
            $table->boolean('is_played')->default(false);
            $table->timestamps();

            $table->index('is_played');
            $table->unique(['feed_id', 'order',]);
            $table->unique(['feed_id', 'filename']);
            $table->foreign('feed_id')
                ->references('id')
                ->on('feeds')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episodes');
    }
}
