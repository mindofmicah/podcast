<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NormalizeOvercastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\OvercastId::where('overcastable_type', 'like', '%Episode%')->update(['overcastable_type' => 'episode']);
        \App\OvercastId::where('overcastable_type', 'like', '%Feed%')->update(['overcastable_type' => 'feed']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
