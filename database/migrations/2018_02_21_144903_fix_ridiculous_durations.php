<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixRidiculousDurations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        App\Episode::with('feed.details')
            ->whereNotNull('duration')
            ->get()
            ->filter(function (App\Episode $episode) {
                return gmdate('s', $episode->duration) == '0';
            })
            ->groupBy('feed_id')
            ->partition(function ($eps) {
                return $eps->first()->feed->feedable_type == App\DropboxFeed::class;
            })
            ->pipe(function ($a) {
                $a[0]->each(function ($b) {
                    $b->each(function ($d) {
                        $d->update([
                            'duration'=>dur2sec(gmdate('00:H:i', $d->duration))
                        ]);
                    });
                });

                return $a[1];
            })
            ->each(function ($episodes) {
                $feed = $episodes->first()->feed;
                $episodes = $episodes->keyBy(function ($a) {
                    return $a->options['title'];
                });
                $details = $feed->details;

                $xml = simplexml_load_file($details->path);
                foreach ($xml->channel->item as $item) {
                    $title = (string)$item->title;
                    if ($episodes->has($title)) {
                        $episodes[$title]->update([
                            'duration' => dur2sec('00:'.(string)$item->xpath('itunes:duration')[0])
                        ]);
                    }
                }
            });;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
