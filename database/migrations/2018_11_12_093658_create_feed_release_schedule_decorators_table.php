<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedReleaseScheduleDecoratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_release_schedule_decorators', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('feed_id');
            $table->unsignedInteger('release_schedule_decorator_id');
            $table->json('options')->default('{}');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feed_release_schedule_decorators');
    }
}
