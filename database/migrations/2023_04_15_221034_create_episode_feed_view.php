<?php
use Illuminate\Database\Migrations\Migration;

class CreateEpisodeFeedView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement($this->dropView());
        DB::statement($this->createView());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement($this->dropView());
    }

    private function dropView()
    {
        return <<<SQL
DROP VIEW IF EXISTS `episode_feed`
SQL;
    }

    private function createView()
    {
        return <<<SQL
CREATE VIEW `episode_feed` AS
Select feeds.id as feed_id, episodes.id as episode_id
from feeds
    left JOIN funnelled_feed_feeds on feeds.feedable_id = funnelled_feed_id AND feedable_type = "App\FunnelledFeed"
    JOIN episodes ON IFNULL(funnelled_feed_feeds.feed_id, feeds.id) = episodes.feed_id    
SQL;
    }
}
