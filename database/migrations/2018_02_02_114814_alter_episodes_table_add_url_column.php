<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Episode;

class AlterEpisodesTableAddUrlColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('episodes', function (Blueprint $table) {
            $table->text('url')->nullable();
        });

        Episode::whereNull('url')->get()->each(function (Episode $episode) {
            $episode->update([
                'url' => $episode->options['url']
            ]);
        });


        Schema::table('episodes', function (Blueprint $table) {
            $table->text('url')->change()->unique();
            //
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('episodes', function (Blueprint $table) {
            $table->dropColumn('url');
        });
    }
}
