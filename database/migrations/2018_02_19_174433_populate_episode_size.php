<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Episode;
use GuzzleHttp\Client;

class PopulateEpisodeSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->query()->get()->each(function ($episode) {
            $options = $episode->options;
            if (!empty($options['size'])) {
                $episode->update([
                    'size'=>$options['size']
                ]);
            }
        });

        $client = new Client;
        $this->query()->get()->each(function ($episode) use($client){
            $ch = curl_init($episode->url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $information = curl_exec($ch);
            $header_groups = array_filter(explode("\r\n\r\n", $information));
            if (preg_match('/Content-Length: ([\d]+)\D/', end($header_groups), $matches)) {
                $episode->update([
                    'size' => $matches[1]
                ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('episodes')->update([
            'size' => null,
        ]);
    }

    private function query()
    {
        return Episode::whereNull('size');
    }
}
