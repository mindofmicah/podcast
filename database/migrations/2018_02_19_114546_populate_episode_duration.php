<?php

use App\Episode;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateEpisodeDuration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->query()->get()->each(function ($episode) {
            $options = $episode->options;
            if ($options['duration'] ?? false) {
                $episode->update([
                    'duration' => $options['duration'],
                ]);
            }
        });

        $this->query()->with('feed')->whereHas('feed', function ($query) {
            $query->where('feedable_type', \App\RemoteFeed::class);
        })->get()->groupBy('feed_id')->each(function ($episodes) {
            $feed = $episodes->first()->feed;
            $details = $feed->details;

            if (strpos($details->path, 'archive.org') !== false) {
                return;
            }

            $episodes = $episodes->keyBy('options.title');

            $xml = simplexml_load_file($feed->details->path);
            foreach ($xml->channel->item as $item) {
                $title = (string)$item->title;
                if ($episodes->has($title)) {
                    $episodes[$title]->update([
                        'duration' => $this->durationToSeconds((string)$item->xpath('itunes:duration')[0])
                    ]);
                } else {
                    dump($title . ' was not found');
                }
            }
        });
        $this->query()->with('feed')->whereHas('feed', function ($query) {
           $query->where('feedable_type', \App\DropboxFeed::class);
        })->get()->groupBy('feed_id')->each(function ($episodes) {
            $feed = $episodes->first()->feed;
            $details = $feed->details;
            foreach($episodes as $episode) {
                $filename = 'podcasts/' . $details->path. '/' . $episode->filename;
                if (\Storage::disk('local')->exists($filename)) {
                    $id3 = new getID3;
                    $results = $id3->analyze(__DIR__ . '/../../storage/app/' . $filename)['playtime_string'];
                    $episode->update([
                        'duration' =>  $this->durationToSeconds($results)
                    ]);
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        DB::table('episodes')->update([
            'duration'=>null,
        ]);
    }

    private function query()
    {
        return Episode::whereNull('duration');
    }

    private function durationToSeconds($duration)
    {
        if (strpos($duration, ':') === false) {
            return null;
        };

        $duration_info = date_parse($duration);

        return $duration_info['hour']*60*60 + $duration_info['minute'] * 60 + $duration_info['second'];


    }
}
