<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulatePassedReleaseDatesForEpisodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Feed::with('episodes')->each(function (\App\Feed $feed) {
            // If there isn't a limit, just set it to their release date
            if ($feed->episode_limit == 0) {
                $feed->episodes()->update(['released_at'=>\DB::raw('updated_at')]);
                return;
            }

            $feed->episodes->each(function (\App\Episode $episode, $index) use($feed) {
                // First episodes are always ready to be released
                if ($index < $feed->episode_limit) {
                    $episode->released_at = $episode->created_at;
                    $episode->save();
                    return;
                }
                $last_played = $feed->episodes[max($index - $feed->episode_limit, 0)];

                if ($episode->is_played || $last_played->is_played) {
                    $episode->released_at = $last_played->updated_at;
                    $episode->save();
                    return;
                }
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Episode::query()->update(['released_at' => null]);
    }
}
