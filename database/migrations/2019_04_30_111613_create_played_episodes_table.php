<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayedEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('played_episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('overcast_id')->unique();
            $table->string('url');
            $table->string('podcast');
            $table->string('title');
            $table->timestamp('released_at');
            $table->timestamp('played_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('played_episodes');
    }
}
