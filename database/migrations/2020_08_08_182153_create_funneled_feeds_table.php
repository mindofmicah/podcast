<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunneledFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funnelled_feeds', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
        Schema::create('funnelled_feed_feeds', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('funnelled_feed_id');
            $table->unsignedInteger('feed_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funnelled_feed_feeds');
        Schema::dropIfExists('funnelled_feeds');
    }
}
