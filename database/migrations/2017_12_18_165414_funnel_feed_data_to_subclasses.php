<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FunnelFeedDataToSubclasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Make add the columns as nullable, since they don't yet have values
        Schema::table('feeds', function (Blueprint $table) {
            $table->unsignedInteger("feedable_id")->nullable();
            $table->string("feedable_type")->nullable();
        });

        // Populate the columns with the new values
            $feeds = \App\Feed::all()->each(function (\App\Feed $feed, int $index) {
                if (is_null($feed->remote_path)) {
                    $dropbox_feed = new \App\DropboxFeed;
                    $dropbox_feed->path = $feed->path;
                    $dropbox_feed->save();

                    $feed->feedable_type = get_class($dropbox_feed);
                    $feed->feedable_id = $dropbox_feed->id;
                    $feed->save();
                    return;
                }
                if (parse_url($feed->remote_path, PHP_URL_SCHEME)) {
                    $remote_feed = new \App\RemoteFeed;
                    $remote_feed->path = $feed->remote_path;
                    $remote_feed->save();
                    
                    $feed->feedable_type = get_class($remote_feed);
                    $feed->feedable_id = $remote_feed->id;
                    $feed->save();
                    return;
                }

                $dropbox_feed = new \App\DropboxFeed;
                $dropbox_feed->path = $feed->remote_path;
                $dropbox_feed->save();

                $feed->feedable_type = get_class($dropbox_feed);
                $feed->feedable_id = $dropbox_feed->id;
                $feed->save();
            });
        
        // Cleanup the feeds table
        Schema::table('feeds', function (Blueprint $table) {
            $table->unsignedInteger("feedable_id")->change();
            $table->string("feedable_type")->change();

            $table->index(["feedable_id", "feedable_type"]);

//            $table->dropColumn('remote_path');
  //          $table->dropColumn('feed_source');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feeds', function (Blueprint $table) {
            $table->dropColumn('feedable_type');
            $table->dropColumn('feedable_id');
        });
    }
}
