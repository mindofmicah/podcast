<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Routing Admin</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.css">
</head>
<body>
    
<style>
.page {
    display:grid;
/*    grid-template-rows:170px auto;*/
    grid-template-columns:170px auto;
}
nav li {
    text-transform:capitalize;
}
ul ul{
    margin-left: 20px;
}
</style>
<div class="container">
<div class="page">
<nav>
    <ul>
        @foreach($resources as $resource=>$nested)
        <li><a href="#{{$resource}}">{{strtr($resource,'-',' ')}}</a>
            @if($nested) <ul> @foreach($nested as $resource => $value)
                <li><a href="#{{$resource}}">{{strtr($resource,'-',' ')}}</a></li>
            @endforeach </ul> @endif
        </li>
        @endforeach
    </ul>
</nav>
<main>
<table class="table is-fullwidth">
<thead>
    <tr>
        <th>Methods</th>
        <th>Route Information</th>
        <th>Route Name</th>
    </tr>
</thead>
<tbody>
    @foreach($routes as $route)
    <tr>
        <td><a name="{{$route['resource']}}">{{implode('|', $route['methods'])}}</a></td>
        <td>
            <div><em>{{$route['url']}}</em></div>
            <div>{{$route['controller']}}</div>
            <div>Middleware: {{$route['middleware']}}</div>
<p>{{$route['description']}}</p>
            <div>{{$route['prefix']}}</div>
        </td>
        <td>{{$route['name']}}</td>
    </tr>
    @endforeach
</tbody>
</table>
</main>
</div>

</div>
</body>
</html>
