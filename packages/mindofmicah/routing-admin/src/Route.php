<?php
namespace MindOfMicah\RoutingAdmin;

use Illuminate\Routing\Route as LaravelRoute;
use phpDocumentor\Reflection\DocBlockFactory;
use Closure;

class Route
{
    private $route;

    public function __construct(LaravelRoute $route)
    {
        $this->route = $route;
    }

    public function toArray()
    {
        $route = $this->route;

        $action_info = $route->getAction();

        $description = '';

        if ($action_info['uses'] instanceof Closure === false && strpos($action_info['uses'], '@')) {
            $reflection_method = (new \ReflectionMethod(...explode('@', $action_info['uses'])))->getDocComment();
            if ($reflection_method) {
                $description = DocBlockFactory::createInstance()->create($reflection_method)->getSummary();
            }
        }

        return [
            'url'         => $route->uri(),
            'resource'    => $this->resourceFromURI($route->uri()),
            'methods'     => $route->methods(),
            'middleware'  => $action_info['middleware'] ?? '',
            'controller'  => $action_info['uses'] instanceof Closure  ? 'Closure' :$action_info['uses'],
            'name'        => $action_info['as'] ?? null,
            'description' => $description,
            'prefix'      => $action_info['prefix'] ?? '',

        ];
    }

    private function resourceFromURI(string $uri): string
    {
        return strtr(trim(preg_replace(['/\{[^\}]+\}\/?/', '/\/(edit|create)$/' ], [], $uri), '/'), '/', '.');
    }
}
