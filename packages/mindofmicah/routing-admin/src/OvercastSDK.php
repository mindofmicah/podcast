<?php
namespace MindOfMicah\RoutingAdmin;

use Symfony\Component\DomCrawler\Crawler;

class OvercastSDK
{
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
        $this->client = new Client([
            'cookies'=>true
        ]);
    }

    public function getEpisodes()
    {
        $url = 'podcasts';
        $listings = (string)$this->client->post('https://overcast.fm/login', [
            'form_params'=>[
                'then'     => $url,
                'email'    => $this->username,
                'password' => $this->password,
            ]
        ])->getBody();

        $listings = (new Crawler($listings))->filter('.episodecell')->each(function (Crawler $crawler) {
            $img_source = $this->getTextFromCrawler($crawler, 'img', 'src');
            $author = $this->getTextFromCrawler($crawler, '.caption2');
            $title = $this->getTextFromCrawler($crawler, '.title');
            $id = $crawler->extract('href')[0];

            return compact('img_source', 'author', 'title', 'id');
        });
        return $listings;
    }

    public function deleteEpisode()
    {

    }

    private function getTextFromCrawler(Crawler $crawler, string $selector, string $attribute = '_text'): string
    {
        return trim($crawler->filter($selector)->eq(0)->extract($attribute)[0]);
    }

}
