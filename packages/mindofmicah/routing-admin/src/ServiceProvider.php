<?php
namespace MindOfMicah\RoutingAdmin;

use Illuminate\Support\ServiceProvider as BaseProvider;

class ServiceProvider extends BaseProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadViewsFrom(__DIR__. '/../views', 'routing');
        $this->publishes([
            __DIR__.'/../config.php'=>config_path('routing.php'),
            __DIR__. '/../views' => resource_path('views/vendor/routing'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__. '/../config.php', 'routing');
    }
}
