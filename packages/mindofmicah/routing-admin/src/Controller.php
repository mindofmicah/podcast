<?php
namespace MindOfMicah\RoutingAdmin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Routing\Controller as BaseController;
use Route as LaravelRoute;

class Controller extends BaseController
{
    use DispatchesJobs;
    use ValidatesRequests;
    use AuthorizesRequests;

    public function index()
    {
        $routes = array_map(function ($route) {
            return (new Route($route))->toArray();
        }, LaravelRoute::getRoutes()->getRoutes());

        $resources = array_unique(array_column($routes, 'resource'));
        sort($resources);

        $resources = array_reduce($resources, function ($carry, string $current) {
            array_set($carry, $current, []);
            return $carry;
        }, []);


        return view('routing::index', get_defined_vars()); 
    }
}
