<?php
namespace MindOfMicah\Configgy;

use Illuminate\Config\Repository;
use RuntimeException;

abstract class Config
{
    protected string $config_heading = '';
    protected array $config_values = [];

    public function __construct(protected Repository $config) {}

    public function __get($key): mixed
    {
        $this->loadIfUnloaded();

        if (array_key_exists($key, $this->config_values)) {
            return $this->config_values[$key];
        }

        throw new RuntimeException('Invalid key for ' . get_called_class());
    }

    protected function loadIfUnloaded(): void
    {
        if (empty($this->config_values)) {
            $this->config_values = $this->config->get('domain.' . $this->config_heading);
        }
    }
}