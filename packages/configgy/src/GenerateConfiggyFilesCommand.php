<?php
namespace MindOfMicah\Configgy;

use Illuminate\Config\Repository;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use MindOfMicah\Teletraan\Command;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class GenerateConfiggyFilesCommand extends Command
{
    const STUB_FILE          = __DIR__ . '/template.stub';
    const GENERATED_FILENAME = 'Config.php';
    const DOMAIN_DIR         = 'Domain';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'configgy:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        private Filesystem $filesystem,
        private Finder $file_finder,
        private Repository $config
    ) {
        parent::__construct();
    }

    /**
     * Generate Config classes for each Domain that has a config file defined
     *
     * @return int
     * @throws FileNotFoundException
     */
    public function handle()
    {
        $this->forEachConfigFile(
            function (string $filename, string $generated_filename, string $namespace, array $values, bool $file_exists) {
                $this->filesystem->put(
                    $generated_filename,
                    $this->buildContents([
                        'NAMESPACE'  => $namespace,
                        'PROPERTIES' => implode(PHP_EOL . ' ', $this->buildProperties($values)),
                        'SLUG'       => $filename,
                    ])
                );
                $this->info(
                    sprintf(
                        '%s Configgy file for %s',
                        $file_exists ? 'Updating ' : 'Generating a ',
                        $namespace
                    )
                );
            });

        return 0;
    }

    private function forEachConfigFile(callable $callable)
    {
        $available_domains = $this->getAvailableDomains();

        foreach ($this->config->get('domain') as $filename => $values) {
            $domain = Str::studly($filename);
            if (!array_key_exists($domain, $available_domains)) {
                $this->warn($filename . ' does not have a correlating Domain', 'v');
                continue;
            }
            $new_filename = $available_domains[$domain] . DIRECTORY_SEPARATOR . self::GENERATED_FILENAME;
            $callable($filename, $new_filename, $domain, $values, $this->filesystem->exists($new_filename));
        }
    }

    private function getAvailableDomains(): array
    {
        $available_domains = [];

        /** @var SplFileInfo $directory */
        foreach ($this->file_finder->in(app_path(self::DOMAIN_DIR))->directories()->depth(0) as $path => $directory) {
            $available_domains[$directory->getFilename()] = $path;
        }

        return $available_domains;
    }

    /**
     * @throws FileNotFoundException
     */
    private function buildContents(array $replacements): string
    {
        $searches = array_map(
            function ($key) {
                return '{{' . $key . '}}';
            },
            array_keys($replacements)
        );

        return str_replace($searches, array_values($replacements), $this->filesystem->get(self::STUB_FILE));
    }

    private function buildProperties(mixed $values): array
    {
        $ret = [];
        foreach ($values as $key => $value) {
            $type = $this->getType($value);
            $ret[] = "* @property-read $type \$$key";
        }

        return $ret;
    }

    private function getType(mixed $value): string
    {
        return gettype($value);
    }
}
