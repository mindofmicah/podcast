<?php
namespace MindOfMicah\Configgy;

use Illuminate\Support\ServiceProvider as LaravelProvider;

class ServiceProvider extends LaravelProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateConfiggyFilesCommand::class,
            ]);
        }
    }
}