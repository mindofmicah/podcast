<?php
Route::get('/feed-releasers/{releaser}', function (App\ReleaseScheduleDecorator $releaser) {
    $feeds = $releaser->feeds->map(function (\App\Feed $feed) use ($releaser) {
        return [
            'title'=>$feed->title,
            'pickem_url'=>$releaser->getFeedUrl($feed)
        ];
    });

    return view('decorators.index', compact('feeds'));
    dd($feeds);
});

Route::get('/episodes', function () {
    return View::component('SearchEpisodes');
});

Route::get('/funnelled-feeds/create', 'FunnelledFeedsController@create');
Route::post('/funnelled-feeds', 'FunnelledFeedsController@store');

Route::get('/artisan/commands', 'Artisan\CommandsController@index');
Route::get('/artisan/commands/{command}', 'Artisan\CommandsController@show');

Route::redirect('/', '/feeds');

Route::post('/released-episodes', 'ReleasedEpisodesController@store');
Route::post('/played-episodes', [\App\Http\Controllers\Episodes\PlayedEpisodesController::class,'store']);

Route::get('/feeds/{feed}/unplayed-episodes', ['uses' => 'UnplayedEpisodesController@index', 'as' => 'feeds.unplayedepisodes.index']);
Route::get('/feeds/{feed}/episodes/create', ['uses' => 'EpisodesController@create']);
Route::post('/feeds/{feed}/episodes', ['uses' => 'EpisodesController@store']);
Route::post('/feeds/{feed}/episodes/{episode}', ['uses' => 'EpisodesController@update']);
Route::get('/episodes/{episode}/artwork', ['uses' => 'EpisodeArtworkController@show']);

Route::post('/feeds/{feed}/episode-order', 'EpisodeOrderController@store');

Route::get('/remote-feeds/create', 'RemoteFeedsController@create');
Route::post('/remote-feeds', 'RemoteFeedsController@store');

Route::get('/youtube-feeds/{slug}', 'YoutubeFeedsController@create');
Route::get('/youtube-feeds/create', 'YoutubeFeedsController@create');
Route::post('/youtube-feeds', 'YoutubeFeedsController@store');

Route::get('feeds/{feed}/unreleased.xml', ['uses'=>'UnreleasedEpisodesFeedController@show']);