<?php
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*
*/
Artisan::command('add:seasons-to-episodes {feed : Feed to update}', function () {
    DB::transaction(function () {
        $feed = $this->argument('feed');

        $feed = \App\Feed::where('path', $feed)->with('details')->firstOrFail(); 

        $episodes = $feed->episodes->keyBy('options.title');
        $urled = $feed->episodes->keyBy('url');

        $feed->scanFeedScource()
            ->newEpisodes()
            ->each(function ($episode, $index) use ($episodes, $urled, $feed) {
                $episode_obj = $episodes[$episode['title']] ?? $urled[$episode['url']] ?? null;
                if ($episode_obj) {
                    $options = $episode_obj['options'];
                    $options['season'] = $episode['options']['season'];
                    $episode_obj->options = $options;
                    $episode_obj->save();
                } else {
                    $feed->episodes()->create($episode);
                }
            });
    });
});


Artisan::command('fix:soundcloud-feeds {feed : Slug for the feed to fix}', function () {
    $feed = \App\Feed::where('path', $this->argument('feed'))
        ->with(['details'])
        ->firstOrFail();
    // Only remote feeds have these paths
    if ($feed->details instanceof \App\RemoteFeed === false) {
        return;
    }

    // The RSS feed tied to the feed needs to be secure
    $feed->details->path = str_replace('http://', 'https://', $feed->details->path);
    $feed->details->save();

    // update unreleased episodes
    $feed->episodes()->whereNull('released_at')->update([
        'url' => \DB::raw('REPLACE(url, "http://", "https://")')
    ]);
});

Artisan::command('fix:feed_updates', function () {
    $s = \DB::select('SELECT feeds.id, MAX(released_at) AS released_at FROM feeds JOIN episodes ON feeds.id = feed_id WHERE released_at > feeds.updated_at GROUP BY feed_id');
    foreach ($s as $r) {
        \App\Feed::where('id', $r->id)->update([
            'updated_at'=> $r->released_at
        ]);
    }
})->describe('update updated_ats for feeds to match latest episodes');
