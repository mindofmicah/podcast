<?php
use App\Domain\Common\Models\PodcastFeed;

Route::group(['prefix' => 'v1'], function () {
    Route::get('/played', function (\Illuminate\Http\Request $request) {
        $start = now()->startOfDay()->subDays(14);
        return response()->json(\DB::table('played_episodes')->where('played_at', '>=', $start)->get()
            
            ->mapToGroups(function ($row) {
            return [
                date('Y-m-d', strtotime($row->played_at)) => preg_replace('/^Magic Playlist - /', '', $row->podcast)
            ];
        })
            ->map(function ($collection) {
              return $collection->groupBy(function ($item) {
                  return $item;
              })->map->count();

            })->pipe(function ($collection) {
                $available_feeds = $collection->map->keys()->flatten()->unique()->values()->all();
//                $collection
                
                $gram = (array_fill_keys($available_feeds, []));
                return [
                    'labels'=> $collection->keys(),
                        'datasets' => collect($collection->reduce(function ($carry, $a) use($available_feeds){
                            foreach (arraY_keys($carry) as $feed) {
                                $carry[$feed][] =  $a->get($feed, 0);
                            }
                            return $carry;
                        }, $gram))
                            ->map(function ($data, $label) {
                                $backgroundColor = sprintf('#%06X', mt_rand(0,0xFFFFFF));
                                return get_defined_vars();
                        })->values()
                ];
            }))
                
                    ->header('Access-Control-Allow-Origin','*')
                    ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
                                ->header('Access-Control-Allow-Headers','X-Requested-With, Content-Type, X-Token-Auth, Authorization');
                ;

    });


    Route::post('/feeds/{feed}/released-episodes', function (PodcastFeed $feed, \App\Domain\EpisodeReleasing\Actions\ReleaseNextEpisodesForFeedAction $action) {
        $data = \DB::transaction(function ($connection) use ($feed, $action) {
            return $action->execute($feed)->pluck('title');
        });

        return response()->json(compact('data'));
    });

    Route::get('available-releasers', 'Api\V1\AvailableReleasersController@index');
    Route::get('feeds/{feed}/release-schedule', function (App\Feed $feed) {
        return response()
            ->json([
                'data'=>
                    $feed->releasables->map(function ($r) {
                        return [
                            'id'=>$r->id,
                            'options'=>$r->pivot->options
                        ];
                    })->all()
            ]);
    });
    Route::post('feeds/{feed}/release-schedule', function (\Illuminate\Http\Request $request, App\Feed $feed) {
        $releasers =collect($request->get('releasers'))->mapWithKeys(function (array $record) {
            return [
                $record['id']=> [
                    'options'=>$record['options'],
                ]
            ];
        })->all();
        $feed->releasables()->sync($releasers);
        return response()->json();
    });

    Route::post('feeds/{feed}/episode-naming-pattern', 'EpisodeNamingPatternsController@store');
    Route::get('active-feeds', 'ActiveFeedsController@index');
    Route::delete('active-feeds/{feed}/', 'ActiveFeedsController@destroy');

    Route::get('dropbox/unclaimed-feeds', 'Dropbox\UnclaimedFeedsController@index');
    Route::any('dropbox/active-feeds', 'Dropbox\ActiveFeedsController@store');

    Route::post('paused-podcasts', 'Api\V1\PausedPodcastsController@store');

    Route::get('audiobooks', function () {
        return response()->json([
            'data'=>
                \App\Feed::with('details')
                    ->whereDoesntHave('episodes', function ($query) {
                        return $query->where('is_played', 0);
                    })
                    ->latest()
                    ->get()
                    ->map(function (App\Feed $feed) {
                        return [
                            'title'=>$feed->title,
                            'coverart'=>$feed->image_path,
                            'date_finished'=> [
                                'raw'=>$feed->updated_at->timestamp,
                                'formatted'=>$feed->updated_at->diffForHumans()
                            ]
                        ];
                    })
            ]);
    });

    Route::get('audiobooks', 'Api\V1\AudiobooksController@index');
});
