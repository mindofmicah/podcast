<?php
use App\Http\Controllers\Feeds\EditFeedController;
use App\Http\Controllers\Feeds\EditPickemReleasableController;
use App\Http\Controllers\Feeds\ListFeedEpisodesController;
use App\Http\Controllers\Feeds\ListFeedsController;
use App\Http\Controllers\Feeds\ShowFeedController;
use App\Http\Controllers\Feeds\ShowFeedCoverart;
use App\Http\Controllers\Feeds\UpdateFeedController;
use App\Http\Controllers\AudioFiles\ShowAudioFilesController;
use App\Http\Controllers\Feeds\UpdateFeedCoverart;
use App\Http\Controllers\Feeds\UpdatePickemReleasableController;
use App\Http\Controllers\Overcast\ListOvercastFeedPlayedEpisodesController;
use App\Http\Controllers\Overcast\ListOvercastFeedsController;
use App\Http\Controllers\Overcast\ShowOvercastFeedRSSUrlController;
use App\Http\Controllers\Youtube\ShowVideoAsMp3;

Route::get('/audio-files/{feed}/{filename}', ShowAudioFilesController::class)->name('audio-files.show');

Route::group(['prefix' => '/feeds/'], function () {
    Route::get('/', ListFeedsController::class)->name('feeds.index');
    Route::get('{feed}', ShowFeedController::class)->name('feeds.show');
    Route::get('{feed}/edit', EditFeedController::class)->name('feeds.edit');
    Route::post('{feed}', UpdateFeedController::class);
    Route::get('{feed}/episodes', ListFeedEpisodesController::class)->name('feeds.episodes.index');
    Route::get('{feed}/releasables/pickem/edit', EditPickemReleasableController::class);
    Route::post('{feed}/releasables/pickem', UpdatePickemReleasableController::class);

    Route::get('{feed}/coverart', ShowFeedCoverart::class);
    Route::post('{feed}/coverart', UpdateFeedCoverart::class);
});

Route::group([], function () {
    Route::get('overcast/feeds', ListOvercastFeedsController::class);
    Route::get('/overcast-feeds/{overcast_feed}/rss-url', ShowOvercastFeedRSSUrlController::class);
    Route::get('/overcast-feeds/{overcast_feed}/played-episodes', ListOvercastFeedPlayedEpisodesController::class);
});

Route::group([], function () {
    Route::get('/youtube/{youtube_id}.mp3', ShowVideoAsMp3::class);
});