<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\NamingPattern;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NamingPatternTest extends TestCase
{
//    use RefreshDatabase;

    /** @test */
    function a_pattern_needs_at_least_one_variable()
    {
        try {
            $a = new NamingPattern('INVALID');
        } catch (\Exception $e) {
            $this->assertTrue(true);
            return;
        }
        $this->fail('An Exception should have been thrown');

    }

    /** @test */
    function it_can_create_a_new_name_for_different_models()
    {
        $pattern = new NamingPattern('New Name {PROP}');

        $model1 = new BaseModel;
        $model1->prop = 'value1';

        $model2 = new BaseModel;
        $model2->prop = 'value2';

        $this->assertEquals('New Name value1', $pattern->rename($model1));
        $this->assertEquals('New Name value2', $pattern->rename($model2));
    }

}

class BaseModel extends \Illuminate\Database\Eloquent\Model{}
