<?php
namespace Tests\Unit;

use App\Episode;
use App\Feed;
use Exception;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FeedTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_can_rename_episodes_to_fit_a_pattern_with_variables()
    {
        $feed = factory(Feed::class)->create();
        factory(Episode::class)->create([
            'order'   => 3,
            'feed_id' => $feed->id
        ]);

        $feed->renameEpisodesByPattern('New Name #{ORDER}');
        $this->assertEquals('New Name #3', $feed->episodes()->first()['options']['title']);
    }

    /** @test */
    function renaming_episodes_to_fit_a_pattern_need_at_least_one_variable()
    {
        $feed = factory(Feed::class)->create();
        factory(Episode::class)->create([
            'feed_id'=>$feed->id,
            'options'=>[
                'title'=>'Old Name'
            ],
        ]);

        try {
            $feed->renameEpisodesByPattern('New Name');
        } catch (Exception $e) {
            $this->assertEquals('Old Name', $feed->episodes()->first()['options']['title']);

            return;
        }
        $this->fail('An Exception should have been thrown');
    }

}
