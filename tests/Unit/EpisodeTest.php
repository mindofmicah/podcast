<?php

namespace Tests\Unit;

use App\Episode;
use App\NamingPattern;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EpisodeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_can_be_renamed_via_naming_pattern()
    {
        $episode = factory(Episode::class)->create([
            'order'=>3
        ]);
        $pattern = new NamingPattern('New Name #{ORDER}');
        $episode->renameByPattern($pattern);

        $this->assertEquals('New Name #3', $episode['options']['title']);
    }
}
