<?php

namespace Tests\Feature;

use App\Feed;
use App\Episode;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EpisodeNamingConventionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function can_rename_all_episodes_of_a_feed_based_on_a_pattern()
    {
        $feed = factory(Feed::class)->create(['path'=>'feed-name']);
        
        $feed->episodes()->saveMany([
            factory(Episode::class)->make([
                'order' => 4
            ])
        ]);

        $response = $this->post('/api/v1/feeds/feed-name/episode-naming-pattern', ['pattern'=>'New Name{ORDER}']);

        $response->assertStatus(201);
        $this->assertEquals('New Name4', $feed->fresh()->episodes[0]->fresh()['options']['title']);
    }
}
