<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default deployment strategy
    |--------------------------------------------------------------------------
    |
    | This option defines which deployment strategy to use by default on all
    | of your hosts. Laravel Deployer provides some strategies out-of-box
    | for you to choose from explained in detail in the documentation.
    |
    | Supported: 'basic', 'firstdeploy', 'local', 'pull'.
    |
    */

    'default' => 'upload',

    /*
    |--------------------------------------------------------------------------
    | Custom deployment strategies
    |--------------------------------------------------------------------------
    |
    | Here, you can easily set up new custom strategies as a list of tasks.
    | Any key of this array are supported in the `default` option above.
    | Any key matching Laravel Deployer's strategies overrides them.
    |
    */

    'strategies' => [
        //
    ],

    /*
    |--------------------------------------------------------------------------
    | Hooks
    |--------------------------------------------------------------------------
    |
    | Hooks let you customize your deployments conveniently by pushing tasks 
    | into strategic places of your deployment flow. Each of the official
    | strategies invoke hooks in different ways to implement their logic.
    |
    */

    'hooks' => [
        // Right before we start deploying.
        'start' => [
            //
        ],
        
        // Code and composer vendors are ready but nothing is built.
        'build' => [
//            'npm:install',
//            'npm:production',
        ],
        
        // Deployment is done but not live yet (before symlink)
        'ready' => [
//            'artisan:storage:link',
            'artisan:view:clear',
            'artisan:cache:clear',
            'artisan:config:cache',
            'artisan:migrate',
            'artisan:custom',
        ],

        // Deployment is done and live
        'done' => [
            //
        ],

        // Deployment succeeded.
        'success' => [
//            'deploy:cloudflare',
        //    'deploy:sentry',
//            'artisan:queue:restart',
        ],
        
        // Deployment failed.
        'fail' => [
            'deploy:unlock',
        ],
        
        // After a deployment has been rolled back.
        'rollback' => [
            //
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Deployment options
    |--------------------------------------------------------------------------
    |
    | Options follow a simple key/value structure and are used within tasks
    | to make them more configurable and reusable. You can use options to
    | configure existing tasks or to use within your own custom tasks.
    |
    */

    'options' => [
        'bin/composer'=>'composer',
        'shared_files'=>[
            '.env',
            'database/database.sqlite',
            'database/telescope.sqlite',
            'database/queue.sqlite',
        ],
        'application' => env('APP_NAME', 'Laravel'),
        'repository'=>'https://mindofmicah:4pEGfyrCSw65Sqw5LXWq@bitbucket.org/mindofmicah/podcast.git',
//        'writable_mode'=>'chown',
  //      'http_user'=>'mo:www-data',
    //    'writable_recursive'=>true,
//        'repository' => 'git@bitbucket.org:mindofmicah/stitching.git',
        'cloudflare'=> [
        //    'service_key'=>'d141ec7f1d0684484d756874f6b7943f',
    'api_key'=>'5b9432ad37954477c1c8f092ca695d0f48677',
    'email'=>'michael.eschbacher+cloudflare@gmail.com',
    'domain'=>'whamdonk.com',

        ],
        'sentry'=>[
            'organization'=>'michael-eschbacher',
            'projects'=>['podcasts'],
            'token'=>'92c8d6536aef43419d6e4b94cf632c92fdd1897e8cb24b94aef1a84e89a67dd9',
            'commits'=>null,
            #   'version' => 'date | md5sum | cut -c-32',
            'git_version_command' => 'date | md5sum | cut -c-32',
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Hosts
    |--------------------------------------------------------------------------
    |
    | Here, you can define any domain or subdomain you want to deploy to.
    | You can provide them with roles and stages to filter them during
    | deployment. Read more about how to configure them in the docs.
    |
    */

    'hosts' => [
        '143.198.149.90' => [
            'deploy_path' => '/var/www/podcasts',
            'user' => 'mo',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Localhost
    |--------------------------------------------------------------------------
    |
    | This localhost option give you the ability to deploy directly on your
    | local machine, without needing any SSH connection. You can use the
    | same configurations used by hosts to configure your localhost.
    |
    */

    'localhost' => [
        //
    ],

    /*
    |--------------------------------------------------------------------------
    | Include additional Deployer recipes
    |--------------------------------------------------------------------------
    |
    | Here, you can add any third party recipes to provide additional tasks, 
    | options and strategies. Therefore, it also allows you to create and
    | include your own recipes to define more complex deployment flows.
    |
    */

    'include' => [
        'recipe/cloudflare.php',
        'recipe/deployment.php',
        'recipe/sentry.php',
    ],

    /*
    |--------------------------------------------------------------------------
    | Use a custom Deployer file
    |--------------------------------------------------------------------------
    |
    | If you know what you are doing and want to take complete control over
    | Deployer's file, you can provide its path here. Note that, without
    | this configuration file, the root's deployer file will be used.
    |
    */

    'custom_deployer_file' => false,

];
