<?php
return [
    'recipes'=>[
        'laravel',
        'common',
        'cloudflare',
        'rsync',
        'npm',
    ],
    'hosts' => [
        '160.153.93.197' => [
            'user' => 'mindofmicah',
            'identity_file'=>'~/.ssh/godaddy.rsa',
            'stage'=>'staging',
            'settings'=> [
                'deploy_path'=>'/home/mindofmicah/public_html/podcasts',
            ]
        ]
    ],
    'settings'=>[
        'default_stage'=>'staging',
        'repository'=>'git@bitbucket.org:mindofmicah/podcast.git',
        'cloudflare' => [
           'api_key'=>'5b9432ad37954477c1c8f092ca695d0f48677',
            'email'=>'michael.eschbacher+cloudflare@gmail.com',
            'domain'=>'whamdonk.com',
        ],
        'writable_mode'=> 'chmod',
        'local_release_path' => '/tmp/deployer',
        'keep_releases' => 3,
        'shared_dirs'=> [
            'storage'
        ],
        'shared_files'=>[
            '.env',
            'database/database.sqlite'
        ]
    ],

    'before'=>[
        'deploy:staging'=>'bbuild',
    ],
    'after'=>[
        'deploy:staging' => 'cleeean',
        'deploy:failed'  => 'deploy:unlock',
    ],
];
