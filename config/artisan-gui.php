<?php
return [
    'option-values' => [
        '*'=>[
            'feed' => \App\Domain\Services\ArtisanGUI\FeedOptionValues::class,
        ],
       'retry:youtube-download'=>[
            'id'=> \App\Domain\Services\ArtisanGUI\OutstandingYoutubeVideos::class,
       ],
    ],
    'favorite'=> [
        'commands'=> [
        ],
        'namespaces'=>[
            'favorites',
            'discover',
        ],  
    ],
    'excluded'=> [
        'list',
        'clear-compiled',
        'auth:clear-resets',
        'db:wipe',
        'env',
        'key:generate',
        'optimize',
        'optimize:clear',
        'package:discover',
        'preset',
        'serve',
        'tinker',
        'inspire',
    ],
    'field_options' => [
        'discover:releasers' => [
        ] 
    ],
    'route'=> [
        'prefix' => 'commands'

    ],
];
