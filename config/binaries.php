<?php
return [
    'youtube-dl' => env('BIN_YOUTUBEDL', '/usr/local/bin/youtube-dl'),
];