<?php
namespace Deployer;
require 'recipe/laravel.php';
require 'recipe/common.php';
require 'recipe/cloudflare.php';
require 'vendor/deployer/recipes/recipe/rsync.php';
require 'vendor/deployer/recipes/recipe/npm.php';
require 'vendor/deployer/recipes/recipe/sentry.php';


set('default_stage', 'staging');

set('repository', 'git@bitbucket.org:mindofmicah/podcast.git');

set('cloudflare', [
//    'service_key'=>'d141ec7f1d0684484d756874f6b7943f',
    'api_key'=>'5b9432ad37954477c1c8f092ca695d0f48677',
    'email'=>'michael.eschbacher+cloudflare@gmail.com',
    'domain'=>'whamdonk.com',
]);

set('sentry', [
    'organization'=>'michael-eschbacher',
    'projects'=>['podcasts'],
    'token'=>'92c8d6536aef43419d6e4b94cf632c92fdd1897e8cb24b94aef1a84e89a67dd9',
    'commits'=>null,
 #   'version' => 'date | md5sum | cut -c-32',
    'git_version_command' => 'date | md5sum | cut -c-32',

]);

host('143.198.149.90')
    ->user('mo')
    ->identityFile('~/.ssh/id_rsa')
    ->stage('staging')
    ->set('deploy_path', '/var/www/podcasts');

add('rsync', [
    'exclude'=>[
        '.git', 
        'deploy.php',
        'node_modules',
    ]
]);

task('npm:build', function (){
    runLocally("cd {{release_path}} && {{bin/npm}} run production");
});

set('rsync_src', function () {
    $local_src = get('build_path');
    if (is_callable($local_src)) {
        $local_src = $local_src();
    }
    return $local_src;
});

set('shared_dirs', [
    'storage'
]);

set('shared_files', [
    '.env',
    'database/database.sqlite',
    'database/telescope.sqlite',
    'database/queue.sqlite',
]);

task('bbuild', function () {
    set('deploy_path', '/tmp/deployer');

    invoke('deploy:prepare');

    invoke('deploy:prepare');
    invoke('deploy:release');
    invoke('deploy:update_code');
    invoke('deploy:vendors');

    invoke('npm:install');
    invoke('npm:build');

    invoke('deploy:symlink');

    if (get('install')) {
        $output = preg_replace_callback('/^(DB_DATABASE=)'.preg_quote(getcwd(), '/').'\/(.+)$/m', function ($matches) {
            run('touch {{release_path}}/' . $matches[2]);
            return $matches[1] . host('64.227.15.176')->get('deploy_path') . '/shared/' . $matches[2];
        }, file_get_contents('.env'));
        file_put_contents('.envv', $output);
        upload('.envv', '{{release_path}}/.env');
        unlink('.envv');
    }

    $release_path = get('release_path');
    on(Deployer::get()->hosts, function () use ($release_path){
        set('build_path', "$release_path/");
    });

})->local();

task('cleeean', function () {
    invoke('cleanup');
})->local();

desc('Run custom artisan stuff');
task('artisan:custom', function () {
    run('{{bin/php}} {{release_path}}/artisan discover:releasers');
});


task('deploy:staging', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:writable',
    'deploy:shared',
    'artisan:view:clear',
    'artisan:cache:clear',
    'artisan:config:cache',
//    'artisan:optimize',
//    'artisan:migrate',  // this isn't set up yet
    'artisan:custom',
    'artisan:queue:restart',
    'deploy:symlink',
    'deploy:cloudflare',
    'deploy:unlock',
    'cleanup',
])->desc('Deploy your project');


before('deploy:staging', 'bbuild');
after('deploy:staging', 'deploy:sentry');
after('deploy:staging', 'cleeean');
after('deploy:failed', 'deploy:unlock');

