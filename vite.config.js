import autoPreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';
import laravel from 'laravel-vite-plugin';
import { defineConfig } from 'vite';
import { svelte } from '@sveltejs/vite-plugin-svelte';
import postcss from './postcss.config.js';

/**
 * @type {import('vite').UserConfig}
 */
export default defineConfig(({command, mode}) => {
    return {
        server: {
            https: true,
            // host:'localhost',
            host: 'podcasts.test'
        },

        rollupOptions: {},
        plugins: [
            svelte({
                preprocess: autoPreprocess()
            }),
            typescript({sourceMap: true}),
            laravel.default({
                    input: ['resources/assets/js/svelte.js'],
                    valetTls: 'podcasts.test'
                }
            )
        ], css: {postcss}
    }
});